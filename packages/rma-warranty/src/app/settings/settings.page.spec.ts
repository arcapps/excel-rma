import { Location } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { PopoverController, ToastController } from '@ionic/angular';
import { EMPTY } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { MaterialModule } from '../material/material.module';
import { SettingsPage } from './settings.page';
import { SettingsService } from './settings.service';

describe('SettingsPage', () => {
  let component: SettingsPage;
  let fixture: ComponentFixture<SettingsPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          FormsModule,
          ReactiveFormsModule,
          MaterialModule,
          BrowserAnimationsModule,
          RouterTestingModule,
        ],
        declarations: [SettingsPage],
        providers: [
          { provide: Location, useValue: {} },
          {
            provide: SettingsService,
            useValue: {
              relayCompaniesOperation: (...args) => switchMap(res => EMPTY),
              relaySellingPriceListsOperation: (...args) =>
                switchMap(res => EMPTY),
              getSettings: (...args) => EMPTY,
              findTerritories: (...args) => EMPTY,
              relayTimeZoneOperation: (...args) => switchMap(res => EMPTY),
              relayAccountsOperation: (...args) => switchMap(res => EMPTY),
              relayWarehousesOperation: (...args) => switchMap(res => EMPTY),
            },
          },
          { provide: ToastController, useValue: {} },
          { provide: PopoverController, useValue: {} },
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
