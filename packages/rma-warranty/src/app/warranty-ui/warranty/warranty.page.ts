import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { WarrantyClaimsDataSource } from './warranty-claims-datasource';
import { Location } from '@angular/common';
import { WarrantyService } from '../warranty-tabs/warranty.service';
import { WarrantyClaims } from '../../common/interfaces/warranty.interface';
import { FormControl, FormGroup } from '@angular/forms';

import {
  DateAdapter,
  MAT_DATE_LOCALE,
  MAT_DATE_FORMATS,
} from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { MY_FORMATS } from '../../constants/date-format';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { map, filter, startWith, switchMap } from 'rxjs/operators';
import { PERMISSION_STATE } from '../../constants/permission-roles';
import {
  CATEGORY,
  CLAIM_STATUS,
  CURRENT_STATUS_VERDICT,
  DATE_TYPE,
  WARRANTY_CLAIMS_CSV_FILE,
  WARRANTY_CLAIMS_DOWNLOAD_HEADERS,
  WARRANTY_TYPE,
} from '../../constants/app-string';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CsvJsonService } from '../../api/csv-json/csv-json.service';
import { ValidateInputSelected } from '../../common/pipes/validators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-warranty',
  templateUrl: './warranty.page.html',
  styleUrls: ['./warranty.page.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class WarrantyPage implements OnInit {
  warrantyClaimsList: Array<WarrantyClaims>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  dataSource: WarrantyClaimsDataSource;
  displayedColumns = [
    'sr_no',
    'claim_no',
    'claim_type',
    'received_on',
    'customer_name',
    'third_party_name',
    'item_code',
    'claimed_serial',
    'claim_status',
    'receiving_branch',
    'delivery_branch',
    'received_by',
    'delivered_by',
    'product_brand',
    'replace_serial',
    'problem',
    'verdict',
    'delivery_date',
    'billed_amount',
    'outstanding_amount',
    'remarks',
  ];
  filteredCustomerList: Observable<any[]>;
  filteredProductList: Observable<any[]>;
  filteredTerritoryList: Observable<any[]>;
  filteredBrandList: Observable<any[]>;
  bulkFlag: boolean = false;
  multipleClaimFlag: boolean = false;
  dateTooltip: string;
  dateValue: any;
  sortQuery: any = {};
  territoryList;
  claim_status: string = CLAIM_STATUS.ALL;
  claimList: string[] = [
    WARRANTY_TYPE.WARRANTY,
    WARRANTY_TYPE.NON_WARRANTY,
    WARRANTY_TYPE.NON_SERIAL,
    WARRANTY_TYPE.THIRD_PARTY,
    WARRANTY_TYPE.PHYSICAL_SERIAL,
  ];
  claimStatusList: string[] = [
    CLAIM_STATUS.IN_PROGRESS,
    CLAIM_STATUS.TO_DELIVER,
    CLAIM_STATUS.DELIVERED,
    CLAIM_STATUS.REJECTED,
    CLAIM_STATUS.ALL,
  ];
  dateTypeList: string[] = [DATE_TYPE.RECEIVED_DATE, DATE_TYPE.DELIVERED_DATE];

  permissionState = PERMISSION_STATE;
  validateInput: any = ValidateInputSelected;
  warrantyForm: FormGroup;
  currentStatus: string[];

  get f() {
    return this.warrantyForm.controls;
  }

  constructor(
    private readonly location: Location,
    private readonly warrantyService: WarrantyService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly csvService: CsvJsonService,
  ) {}

  ngOnInit() {
    this.createFormGroup();
    this.currentStatus = Object.values(CURRENT_STATUS_VERDICT);
    this.route.params.subscribe(() => {
      this.paginator.firstPage();
    });
    this.dataSource = new WarrantyClaimsDataSource(this.warrantyService);
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map((event: any) => {
          if (event.url === '/warranty') {
            this.dataSource.loadItems(
              undefined,
              undefined,
              undefined,
              {},
              {
                set: [CATEGORY.BULK, CATEGORY.SINGLE, CATEGORY.PART],
              },
              this.multipleClaimFlag,
            );
          }
          return event;
        }),
      )
      .subscribe({
        next: () => {},
        error: () => {},
      });

    this.filteredCustomerList = this.warrantyForm
      .get('customer_name')
      .valueChanges.pipe(
        startWith(''),
        switchMap(value => {
          return this.warrantyService.getCustomerList(value);
        }),
      );

    this.filteredBrandList = this.warrantyForm.get('brand').valueChanges.pipe(
      startWith(''),
      switchMap(value => {
        return this.warrantyService.getBrandList(value);
      }),
    );

    this.filteredProductList = this.warrantyForm
      .get('product')
      .valueChanges.pipe(
        startWith(''),
        switchMap(value => {
          return this.warrantyService.getItemList({ item_name: value });
        }),
      );

    this.filteredTerritoryList = this.warrantyForm
      .get('territory')
      .valueChanges.pipe(
        startWith(''),
        switchMap(value => {
          return this.warrantyService
            .getStorage()
            .getItemAsync('territory', value);
        }),
      );
  }

  createFormGroup() {
    this.warrantyForm = new FormGroup({
      customer_name: new FormControl(''),
      claim_no: new FormControl(''),
      third_party_name: new FormControl(''),
      third_party_contact: new FormControl(''),
      product: new FormControl(''),
      brand: new FormControl(''),
      claim_status: new FormControl(CLAIM_STATUS.ALL),
      claim_type: new FormControl(''),
      territory: new FormControl(''),
      serial_no: new FormControl(''),
      replace_serial: new FormControl(''),
      received_by: new FormControl(''),
      delivered_by: new FormControl(''),
      date_type: new FormControl(DATE_TYPE.RECEIVED_DATE),
      from_date: new FormControl(''),
      to_date: new FormControl(''),
      verdict: new FormControl(''),
      created_by: new FormControl(''),
      voucher_no: new FormControl(''),
    });
  }

  //
  getShortenedString(largeString: string): string {
    const maxLength = 300;
    return largeString.length > maxLength
      ? largeString.substring(0, maxLength) + '...'
      : largeString;
  }

  getUpdate(event?: any) {
    const query: any = {};
    if (this.f.customer_name.value)
      query.customer = this.f.customer_name.value.customer_name;
    if (this.f.claim_no.value) query.claim_no = this.f.claim_no.value;
    if (this.f.third_party_name.value)
      query.third_party_name = this.f.third_party_name.value;
    if (this.f.third_party_contact.value)
      query.third_party_contact = this.f.third_party_contact.value;
    if (this.f.product.value) query.item_name = this.f.product.value.item_name;
    if (this.f.brand.value) query.product_brand = this.f.brand.value;
    if (this.f.claim_status.value)
      query.claim_status = this.f.claim_status.value;
    if (this.f.claim_type.value) query.claim_type = this.f.claim_type.value;
    if (this.f.territory.value) query.receiving_branch = this.f.territory.value;
    if (this.f.serial_no.value) query.serial_no = this.f.serial_no.value;
    if (this.f.replace_serial.value)
      query.replace_serial = this.f.replace_serial.value;
    if (this.f.received_by.value) query.received_by = this.f.received_by.value;
    if (this.f.delivered_by.value)
      query.delivered_by = this.f.delivered_by.value;
    if (this.f.from_date.value && this.f.to_date.value) {
      query.date_type = this.f.date_type.value;
      query.from_date = new Date(this.f.from_date.value).setHours(0, 0, 0, 0);
      query.to_date = new Date(this.f.to_date.value).setHours(23, 59, 59, 59);
    }

    if (this.f.verdict.value) {
      query.verdict = this.f.verdict.value;
    }

    if (this.f.created_by.value) {
      query.created_by = this.f.created_by.value;
    }

    if (this.f.voucher_no.value) {
      query.voucher_no = this.f.voucher_no.value;
    }

    this.paginator.pageIndex = event?.pageIndex || 0;
    this.paginator.pageSize = event?.pageSize || 30;
    if (event) {
      for (const key of Object.keys(event)) {
        if (key === 'active' && event.direction !== '') {
          this.sortQuery[event[key]] = event.direction;
        }
      }
    }
    this.sortQuery =
      Object.keys(this.sortQuery).length === 0
        ? { createdOn: 'desc' }
        : this.sortQuery;
    this.dataSource.loadItems(
      this.sortQuery,
      this.paginator.pageIndex,
      this.paginator.pageSize,
      query,
      this.bulkFlag
        ? {
            territory: this.territoryList,
            set: [CATEGORY.BULK],
          }
        : {
            territory: this.territoryList,
            set: [CATEGORY.SINGLE, CATEGORY.PART],
          },
      this.multipleClaimFlag,
    );
  }

  setFilter() {
    const query: any = {};
    if (this.f.customer_name.value)
      query.customer = this.f.customer_name.value.customer_name;
    if (this.f.claim_no.value) query.claim_no = this.f.claim_no.value;
    if (this.f.third_party_name.value)
      query.third_party_name = this.f.third_party_name.value;
    if (this.f.third_party_contact.value)
      query.third_party_contact = this.f.third_party_contact.value;
    if (this.f.product.value) query.item_name = this.f.product.value.item_name;
    if (this.f.brand.value) query.product_brand = this.f.brand.value;
    if (this.f.claim_type.value) query.claim_type = this.f.claim_type.value;
    if (this.f.territory.value) query.receiving_branch = this.f.territory.value;
    if (this.f.serial_no.value) query.serial_no = this.f.serial_no.value;
    if (this.f.replace_serial.value)
      query.replace_serial = this.f.replace_serial.value;
    if (this.f.received_by.value) query.received_by = this.f.received_by.value;
    if (this.f.delivered_by.value)
      query.delivered_by = this.f.delivered_by.value;

    if (this.f.from_date.value && this.f.to_date.value) {
      this.dateTooltip = `${this.formatDate(
        this.f.from_date.value,
      )} to ${this.formatDate(this.f.to_date.value)}`;
      query.date_type = this.f.date_type.value;
      query.from_date = new Date(this.f.from_date.value).setHours(0, 0, 0, 0);
      query.to_date = new Date(this.f.to_date.value).setHours(23, 59, 59, 59);
    }
    if (this.f.claim_status.value)
      query.claim_status = this.f.claim_status.value;

    if (this.f.verdict.value) {
      query.verdict = this.f.verdict.value;
    }

    if (this.f.created_by.value) {
      query.created_by = this.f.created_by.value;
    }

    if (this.f.voucher_no.value) {
      query.voucher_no = this.f.voucher_no.value;
    }

    this.paginator.firstPage();
    this.dataSource.loadItems(
      this.sortQuery,
      this.paginator.pageIndex,
      this.paginator.pageSize,
      query,
      this.bulkFlag
        ? {
            territory: this.territoryList,
            set: [CATEGORY.BULK],
          }
        : {
            territory: this.territoryList,
            set: [CATEGORY.SINGLE, CATEGORY.PART, CATEGORY.BULK],
          },
      this.multipleClaimFlag,
    );
  }

  getBulkClaims() {
    this.bulkFlag = true;
    this.dataSource.loadItems(
      undefined,
      undefined,
      undefined,
      undefined,
      {
        territory: this.territoryList,
        set: [CATEGORY.BULK],
      },
      this.multipleClaimFlag,
    );
  }

  // multiple claim flag
  getMultipleClaim() {
    this.multipleClaimFlag = true;
    this.setFilter();
  }

  formatDate(timestamp: number): string {
    const dateObject = new Date(timestamp);
    const day = dateObject.getDate();
    const month = dateObject.getMonth() + 1; // Months are zero-based, so add 1
    const year = dateObject.getFullYear();
    // Ensure two digits for day and month
    const formattedDay = day < 10 ? `0${day}` : day.toString();
    const formattedMonth = month < 10 ? `0${month}` : month.toString();
    return `${formattedDay}-${formattedMonth}-${year}`;
  }

  // statusChange(status: string) {
  //   if (status === 'All') {
  //     this.dataSource.loadItems(
  //       undefined,
  //       undefined,
  //       undefined,
  //       undefined,
  //       this.bulkFlag
  //         ? {
  //             territory: this.territoryList,
  //             set: [CATEGORY.BULK],
  //           }
  //         : {
  //             territory: this.territoryList,
  //             set: [CATEGORY.SINGLE, CATEGORY.PART, CATEGORY.BULK],
  //           },
  //     );
  //   } else {
  //     // this.claim_status = status;
  //     this.setFilter();
  //   }
  // }

  clearFilters() {
    this.f.customer_name.setValue('');
    this.f.claim_no.setValue('');
    this.f.third_party_name.setValue('');
    this.f.third_party_contact.setValue('');
    this.f.product.setValue('');
    this.f.brand.setValue('');
    this.f.claim_status.setValue(CLAIM_STATUS.ALL);
    this.f.claim_type.setValue('');
    this.f.territory.setValue('');
    this.f.serial_no.setValue('');
    this.f.received_by.setValue('');
    this.f.delivered_by.setValue('');
    this.f.from_date.setValue('');
    this.f.to_date.setValue('');
    this.f.replace_serial.setValue('');
    this.f.verdict.setValue('');
    this.f.created_by.setValue('');
    this.f.voucher_no.setValue('');
    this.f.date_type.setValue(DATE_TYPE.RECEIVED_DATE);
    this.paginator.pageSize = 30;
    this.sortQuery = {};
    this.paginator.firstPage();
    this.bulkFlag = false;
    this.multipleClaimFlag = false;
    this.dataSource.loadItems(
      undefined,
      undefined,
      undefined,
      undefined,
      {
        territory: this.territoryList,
        set: [CATEGORY.SINGLE, CATEGORY.PART, CATEGORY.BULK],
      },
      this.multipleClaimFlag,
    );
  }

  navigateBack() {
    this.location.back();
  }

  getCustomerOption(option) {
    return option.customer_name;
  }

  getProductOption(option) {
    return option.item_name;
  }

  getOption(option) {
    return option;
  }

  warrantyRoute(row: any) {
    this.dataSource.loadItems(
      undefined,
      undefined,
      undefined,
      { parent: row.uuid },
      {
        territory: this.territoryList,
        set: [CATEGORY.PART],
      },
      this.multipleClaimFlag,
    );
  }

  downloadSerials() {
    this.csvService.downloadAsCSV(
      this.dataSource.data,
      WARRANTY_CLAIMS_DOWNLOAD_HEADERS,
      `${WARRANTY_CLAIMS_CSV_FILE}`,
    );
  }

  getDate(date: string) {
    return new Date(date);
  }

  getTime(time: string) {
    // eslint-disable-next-line no-var
    const date = new Date(time);
    // eslint-disable-next-line no-var
    const formattedTime = date.toLocaleTimeString([], {
      hour: 'numeric',
      minute: 'numeric',
    });

    return formattedTime;
  }
  timeFunc(timeString: string): string {
    const time = new Date('1970-01-01T' + timeString);
    return time.toLocaleTimeString([], {
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
    });
  }
  // Developer Achem
  isVisible = false;
  warrantyClaims() {
    const filterShowHideID = document.getElementById('warranty_claims');
    const WarrantyFilter = document.getElementById('WarrantyFilter');
    const WarrantyClose = document.getElementById('WarrantyClose');
    if (filterShowHideID.classList.contains('active')) {
      this.isVisible = false;
      filterShowHideID.classList.remove('active');
      WarrantyFilter.style.display = 'block';
      WarrantyClose.style.display = 'none';
    } else {
      this.isVisible = true;
      filterShowHideID.classList.add('active');
      WarrantyFilter.style.display = 'none';
      WarrantyClose.style.display = 'block';
    }
  }

  openSerialSearch() {
    const url = '/serial-info-search'; // Update with the correct route URL
    const newTab = window.open();
    newTab.location.href = this.location.prepareExternalUrl(url);
  }
}

@Component({
  selector: 'assign-serials-dialog',
  templateUrl: 'assign-serials-dialog.html',
})
export class AssignSerialsDialog {
  constructor(
    public dialogRef: MatDialogRef<AssignSerialsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}
  onNoClick(): void {
    this.dialogRef.close();
  }
}
