import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { STORAGE_TOKEN } from '../../../api/storage/storage.service';
import { MaterialModule } from '../../../material/material.module';
import { WarrantyService } from '../../warranty-tabs/warranty.service';
import { ClaimDetailsComponent } from './claim-details.component';

describe('ClaimDetailsComponent', () => {
  let component: ClaimDetailsComponent;
  let fixture: ComponentFixture<ClaimDetailsComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ClaimDetailsComponent],
        imports: [
          IonicModule.forRoot(),
          HttpClientTestingModule,
          MaterialModule,
          RouterTestingModule,
        ],
        providers: [
          {
            provide: WarrantyService,
            useValue: {
              getWarrantyClaim: () => of({}),
              getStorage: () => ({
                getItem: (...args) => Promise.resolve('Item'),
                getItems: (...args) => Promise.resolve({}),
              }),
            },
          },
          {
            provide: STORAGE_TOKEN,
            useValue: {},
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(ClaimDetailsComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
