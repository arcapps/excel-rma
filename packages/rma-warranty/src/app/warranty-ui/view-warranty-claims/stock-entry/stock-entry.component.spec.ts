import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { STORAGE_TOKEN } from '../../../api/storage/storage.service';
import { AppService } from '../../../app.service';
import { MaterialModule } from '../../../material/material.module';
import { AddServiceInvoiceService } from '../../shared-warranty-modules/service-invoices/add-service-invoice/add-service-invoice.service';
import { StockEntryService } from './services/stock-entry/stock-entry.service';
import { StockEntryComponent } from './stock-entry.component';

describe('StockEntryComponent', () => {
  let component: StockEntryComponent;
  let fixture: ComponentFixture<StockEntryComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [StockEntryComponent],
        imports: [
          IonicModule.forRoot(),
          BrowserAnimationsModule,
          HttpClientTestingModule,
          MaterialModule,
          FormsModule,
          ReactiveFormsModule,
          NoopAnimationsModule,
          RouterTestingModule.withRoutes([]),
        ],
        providers: [
          {
            provide: AppService,
            useValue: {
              getStorage: (...args) => of({}),
            },
          },
          {
            provide: StockEntryService,
            useValue: {
              getStockEntryList: (...args) => of([{}]),
              finalizeEntry: (...args) => of([{}]),
            },
          },
          {
            provide: AddServiceInvoiceService,
            useValue: {
              getWarrantyDetail: (...args) => of([{}]),
            },
          },
          {
            provide: STORAGE_TOKEN,
            useValue: {},
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(StockEntryComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
