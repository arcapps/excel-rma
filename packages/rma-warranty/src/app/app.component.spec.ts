import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, waitForAsync } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';
import { Platform } from '@ionic/angular';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Subscription, of } from 'rxjs';
import { STORAGE_TOKEN } from './api/storage/storage.service';
import { AppComponent } from './app.component';
import { AppService } from './app.service';

describe('AppComponent', () => {
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AppComponent],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [
          {
            provide: AppService,
            useValue: {
              getMessage: (...args) => of({}),
              setInfoLocalStorage: (...args) => null,
              checkUserProfile: () => of({ roles: [] }),
            },
          },
          {
            provide: Platform,
            useValue: {
              ready: () => Promise.resolve(),
              backButton: {
                subscribeWithPriority: (...args) => new Subscription(),
              },
            },
          },
          { provide: STORAGE_TOKEN, useValue: {} },
        ],
        imports: [RouterTestingModule.withRoutes([]), HttpClientTestingModule],
      }).compileComponents();
    }),
  );

  it('should create the app', async () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
