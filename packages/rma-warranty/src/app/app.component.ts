import { Component, OnInit } from '@angular/core';
import { LOGGED_IN, ALL_TERRITORIES, APP_URL } from './constants/storage';
import { AppService } from './app.service';
import { USER_ROLE, TERRITORY, WAREHOUSES } from './constants/app-string';
import { SettingsService } from './settings/settings.service';
import { PermissionManager } from './api/permission/permission.service';
import { PERMISSION_STATE } from './constants/permission-roles';
import { TokenService } from './auth/token/token.service';
import { SET_ITEM, StorageService } from './api/storage/storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  loggedIn: boolean = false;
  permissionState: any = PERMISSION_STATE;

  isSettingMenuVisible: boolean = false;
  isSalesMenuVisible: boolean = false;
  isRnDMenuVisible: boolean = false;
  isStockMenuVisible: boolean = false;
  isRelayMenuVisible: boolean = false;

  name: string = '';
  imageURL: string = '';

  constructor(
    private readonly appService: AppService,
    private readonly settingService: SettingsService,
    private readonly permissionManager: PermissionManager,
    private readonly storageService: StorageService,
    private readonly tokenService: TokenService,
    private readonly router: Router,
  ) {}

  ngOnInit() {
    this.storageService.getItem(LOGGED_IN).then(loggedIn => {
      loggedIn === 'true' ? (this.loggedIn = true) : (this.loggedIn = false);
      if (this.loggedIn) {
        this.loadProfile();
        this.appService.getGlobalDefault();
        this.getRoles();
      }
    });

    this.storageService.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          res.value?.value === 'true'
            ? (this.loggedIn = true)
            : (this.loggedIn = false);
          if (!this.loggedIn) {
            this.router.navigate(['/home']).then(() => {});
          }
          if (this.loggedIn) {
            this.loadProfile();
            this.appService.getGlobalDefault();
            this.getRoles();
          }
        }
      },
      error: () => {
        this.logout();
      },
    });
  }

  login() {
    this.tokenService.logIn();
  }

  logout() {
    this.tokenService.logOut();
  }

  loadProfile() {
    this.tokenService.loadProfile().subscribe({
      next: profile => {
        this.loggedIn = true;
        this.name = profile.name;
        this.imageURL = profile.picture;
      },
      error: () => {},
    });
  }

  getRoles() {
    this.settingService.getRoles().subscribe({
      next: async (res: {
        roles: string[];
        warehouse: string[];
        territory: string[];
      }) => {
        this.loggedIn = true;
        if (res) {
          await this.storageService.setItem(
            USER_ROLE,
            res.roles?.toString() || '',
          );
          await this.storageService.setItem(
            WAREHOUSES,
            res?.warehouse?.toString() || '',
          );
          const filtered_territory = res?.territory?.filter(
            territory => territory !== ALL_TERRITORIES,
          );
          this.storageService.setItem(
            TERRITORY,
            filtered_territory?.toString() || '',
          );
          this.permissionManager.setupPermissions();
        }
      },
      error: () => {},
    });
  }
  sidebarHandler() {
    const sidebar = document.getElementById('sidebar_menu');
    if (sidebar.classList.contains('menu_active')) {
      sidebar.classList.remove('menu_active');
    } else {
      sidebar.classList.add('menu_active');
    }
  }

  openSalesPortal() {
    this.storageService.getItem(APP_URL).then(salesPortalUrl => {
      window.open(salesPortalUrl, '_blank');
    });
  }
}
