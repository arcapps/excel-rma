import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { Observable, from, of } from 'rxjs';
import {
  ACCESS_TOKEN,
  AUTHORIZATION,
  BEARER_TOKEN_PREFIX,
} from '../../../constants/storage';
import { catchError, map, switchMap } from 'rxjs/operators';
import { APIResponse } from '../../../common/interfaces/sales.interface';
import {
  LIST_ITEMS_ENDPOINT,
  RELAY_DOCTYPE_ENDPOINT_PREFIX,
  STOCK_BALANCE_SUMMARY_ENDPOINT,
} from '../../../constants/url-strings';
import { StorageService } from '../../../api/storage/storage.service';
@Injectable({
  providedIn: 'root',
})
export class StockBalanceSummaryService {
  constructor(private http: HttpClient, private storage: StorageService) {}

  getProblemList(search = '', sort = 'ASC', pageNumber = 0, pageSize = 30) {
    const url = STOCK_BALANCE_SUMMARY_ENDPOINT;
    const params = new HttpParams()
      .set('limit', pageSize.toString())
      .set('offset', (pageNumber * pageSize).toString())
      .set('filter_query', search)
      .set('sort', sort);

    return this.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get<APIResponse>(url, {
          params,
          headers,
        });
      }),
    );
  }
  relayDocTypeOperation(
    docType: string,
    value: string = '',
  ): Observable<string[]> {
    const params = new HttpParams({
      fromObject: {
        fields: '["*"]',
        filters: `[["name","like","%${value}%"]]`,
      },
    });

    return this.getHeaders().pipe(
      switchMap(headers => {
        return this.http
          .get<{ data: string[] }>(RELAY_DOCTYPE_ENDPOINT_PREFIX + docType, {
            headers,
            params,
          })
          .pipe(map(res => res.data));
      }),
    );
  }

  getItemList(
    filter: any = {},
    sortOrder: any = { item_name: 'asc' },
    pageIndex = 0,
    pageSize = 30,
    query?: { [key: string]: any },
  ) {
    try {
      sortOrder = JSON.stringify(sortOrder);
    } catch {
      sortOrder = JSON.stringify({ item_name: 'asc' });
    }
    const url = LIST_ITEMS_ENDPOINT;
    query = query ? query : {};
    query.item_name = filter?.item_name ? filter.item_name : filter;
    query.has_serial_no = 1;
    query.disabled = 0;

    const params = new HttpParams()
      .set('limit', pageSize.toString())
      .set('offset', (pageIndex * pageSize).toString())
      .set('search', encodeURIComponent(JSON.stringify(query)))
      .set('sort', sortOrder);
    return this.getHeaders().pipe(
      switchMap(headers => {
        return this.http
          .get<APIResponse>(url, {
            params,
            headers,
          })
          .pipe(
            switchMap(response => {
              return of(response.docs);
            }),
            catchError(err => {
              return of([]);
            }),
          );
      }),
    );
  }

  getHeaders() {
    return from(this.storage.getItem(ACCESS_TOKEN)).pipe(
      map(token => {
        return {
          [AUTHORIZATION]: BEARER_TOKEN_PREFIX + token,
        };
      }),
    );
  }
}
