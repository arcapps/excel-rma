import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Location } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { MaterialModule } from '../material/material.module';
import { StockBalanceSummaryService } from './services/stock-balance-summary/stock-balance-summary.service';
import { StockBalanceSummaryPage } from './stock-balance-summary.page';

describe('StockBalanceSummaryPage', () => {
  let component: StockBalanceSummaryPage;
  let fixture: ComponentFixture<StockBalanceSummaryPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [StockBalanceSummaryPage],
        imports: [
          IonicModule.forRoot(),
          MaterialModule,
          ReactiveFormsModule,
          FormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [
          {
            provide: StockBalanceSummaryService,
            useValue: {
              relayDocTypeOperation: (docType: string, value: string) => of([]), // Provide a suitable response
              getItemList: (
                filter: any,
                sortOrder: any,
                pageIndex: number,
                pageSize: number,
                query?: { [key: string]: any },
              ) => of([]), // Provide a suitable response
              getProblemList: (...args) =>
                of({ docs: [], length: 0, offset: 0 }),
            },
          },
          { provide: Location, useValue: {} },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(StockBalanceSummaryPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
