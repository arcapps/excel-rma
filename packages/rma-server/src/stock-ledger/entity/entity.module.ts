import { Module } from '@nestjs/common';
// import { TypeOrmModule } from '@nestjs/typeorm';
// import { StockLedger } from './stock-ledger/stock-ledger.entity';
import { StockLedgerService } from './stock-ledger/stock-ledger.service';
import { MongooseModule } from '@nestjs/mongoose';
import { StockLedgerSchema } from '../schema/stock-ledger.schema';

@Module({
  // imports: [TypeOrmModule.forFeature([StockLedger])],
  imports: [
    MongooseModule.forFeature([
      { name: 'StockLedger', schema: StockLedgerSchema },
    ]),
  ],
  providers: [StockLedgerService],
  exports: [StockLedgerService],
})
export class StockLedgerEntitiesModule {}
