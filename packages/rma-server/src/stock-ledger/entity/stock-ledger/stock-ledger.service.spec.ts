import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { StockLedgerService } from './stock-ledger.service';

describe('StockLedgerService', () => {
  let service: StockLedgerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StockLedgerService,
        {
          provide: getModelToken('StockLedger'),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<StockLedgerService>(StockLedgerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
