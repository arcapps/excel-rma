import { Module } from '@nestjs/common';
// import { TypeOrmModule } from '@nestjs/typeorm';
import { ItemService } from './item/item.service';
// import { Item } from '../schema/item.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { ItemSchema } from '../schema/item.schema';

@Module({
  imports: [
    // TypeOrmModule.forFeature([Item]),
    MongooseModule.forFeature([{ name: 'Item', schema: ItemSchema }]),
  ],
  providers: [ItemService],
  exports: [ItemService],
})
export class ItemEntitiesModule {}
