import { IQuery } from '@nestjs/cqrs';

export class RetrieveItemByItemBarcodeQuery implements IQuery {
  constructor(public readonly barcode: string, public readonly req: any) {}
}
