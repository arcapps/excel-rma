import { RetrieveItemHandler } from './get-item/retrieve-item-query.handler';
import { RetrieveItemListHandler } from './list-item/retrieve-item-list-query.handler';
import { RetrieveItemByCodeHandler } from './get-item-by-code/retrieve-item-by-code-query.handler';
import { RetrieveItemByNamesHandler } from './get-item-by-names/retrieve-item-by-names-query.handler';
import { RetrieveItemByItemGroupHandler } from './get-item-by-item-group/retrieve-item-by-item-group-query.handler';
import { RetrieveItemByBarcodeHandler } from './get-item-by-barcode/retrieve-item-by-barcode-query.handler';

export const ItemQueryManager = [
  RetrieveItemHandler,
  RetrieveItemListHandler,
  RetrieveItemByCodeHandler,
  RetrieveItemByNamesHandler,
  RetrieveItemByItemGroupHandler,
  RetrieveItemByBarcodeHandler,
];
