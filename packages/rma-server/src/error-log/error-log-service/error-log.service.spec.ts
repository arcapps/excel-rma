import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { ErrorLogService } from './error-log.service';

describe('ErrorLogService', () => {
  let service: ErrorLogService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ErrorLogService,
        {
          provide: getModelToken('ErrorLog'),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ErrorLogService>(ErrorLogService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
