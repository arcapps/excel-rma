import { Module, Global } from '@nestjs/common';
import { ErrorLogController } from './controller/error-logs.controller';
import { ErrorLogService } from './error-log-service/error-log.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ErrorLogSchema } from './schema/error-log.schema';

@Global()
@Module({
  // imports: [TypeOrmModule.forFeature([ErrorLog])],
  imports: [
    MongooseModule.forFeature([{ name: 'ErrorLog', schema: ErrorLogSchema }]),
  ],
  controllers: [ErrorLogController],
  providers: [ErrorLogService],
  exports: [ErrorLogService],
})
export class ErrorLogModule {}
