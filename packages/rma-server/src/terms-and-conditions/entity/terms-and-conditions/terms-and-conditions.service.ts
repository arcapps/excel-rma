import { Injectable } from '@nestjs/common';
import { TermsAndConditions } from './terms-and-conditions.entity';
// import { MongoFindOneOptions } from 'typeorm/find-options/mongodb/MongoFindOneOptions';
import { InjectModel } from '@nestjs/mongoose';
import { TermsAndConditionsDocument } from '../../../terms-and-conditions/schema/terms-and-conditions.schema';
import { Model } from 'mongoose';

@Injectable()
export class TermsAndConditionsService {
  constructor(
    // @InjectRepository(TermsAndConditions)
    // private termsAndConditionsRepository: MongoRepository<TermsAndConditions>,
    @InjectModel('TermsAndConditions')
    private termsAndConditionsModel: Model<TermsAndConditionsDocument>,
  ) {}

  async create(termsAndConditionsPayload: TermsAndConditions) {
    return await new this.termsAndConditionsModel(
      termsAndConditionsPayload,
    ).save();
  }

  async findOne(options) {
    return await this.termsAndConditionsModel.findOne(options.where);
  }

  async deleteOne(query, options?): Promise<any> {
    return await this.termsAndConditionsModel.deleteOne(query, options);
  }

  async updateOne(query, param) {
    return await this.termsAndConditionsModel.updateOne(query, param);
  }

  async list(skip, take, search, sort) {
    const sortQuery = { terms_and_conditions: sort };

    const nameExp = new RegExp(search, 'i');

    const $or = [{ terms_and_conditions: nameExp }];

    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.termsAndConditionsModel
      .find(where)
      .skip(skip)
      .limit(take)
      .sort(sortQuery);

    return {
      docs: results || [],
      length: await this.termsAndConditionsModel
        .count(where)
        .skip(skip)
        .limit(take)
        .sort(sortQuery),
      offset: skip,
    };
  }
}
