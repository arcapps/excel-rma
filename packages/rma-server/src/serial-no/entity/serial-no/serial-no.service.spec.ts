import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { SerialNoService } from './serial-no.service';

describe('SerialNoService', () => {
  let service: SerialNoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SerialNoService,
        {
          provide: getModelToken('SerialNo'),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<SerialNoService>(SerialNoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
