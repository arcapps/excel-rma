import { Injectable } from '@nestjs/common';
import { Observable, firstValueFrom } from 'rxjs';
import { SERIAL_FILTER_KEYS } from '../../../constants/app-strings';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SalesNoDocument, SerialNo } from '../../schema/serial-no.schema';

@Injectable()
export class SerialNoService {
  constructor(
    // @InjectRepository(SerialNo)
    // private readonly serialNoRepository: MongoRepository<SerialNo>,
    @InjectModel('SerialNo')
    private serialNoModel: Model<SalesNoDocument>,
  ) {}

  async find(query?, take?) {
    return await this.serialNoModel.find(query).limit(take);
  }

  async findAll(query) {
    return await this.serialNoModel.find(query);
  }

  async create(serialNoPayload: SerialNo) {
    // const serialNo = new SerialNo();
    // Object.assign(serialNo, serialNoPayload);
    return await new this.serialNoModel(serialNoPayload).save();
  }

  async findOne(options) {
    return await this.serialNoModel.findOne(options.where);
  }

  // async list(skip, take, sort, filterQuery) {
  //   let order: any;

  //   try {
  //     order = JSON.parse(sort);
  //   } catch (error) {
  //     order = { _id: -1 };
  //   }

  //   if (Object.keys(order).length === 0) {
  //     order = { _id: -1 };
  //   }

  //   try {
  //     filterQuery = JSON.parse(filterQuery);
  //   } catch (error) {
  //     filterQuery = {};
  //   }

  //   const where: { [key: string]: any } = filterQuery
  //     ? this.parseSerialFilterQuery(filterQuery)
  //     : {};
  //   const results = await this.serialNoModel.find()
  //   .where(where)
  //   .skip(skip)
  //   .limit(take)
  //   .sort(order)

  //   return {
  //     docs: results || [],
  //     length: await this.serialNoModel.count(where).skip(skip).limit(take),
  //     offset: skip,
  //   };
  // }
  async list(skip, take, sort, filterQuery) {
    let order: any;
    let showBoth = false;
    try {
      order = JSON.parse(sort);
    } catch (error) {
      order = { _id: -1 };
    }

    if (Object.keys(order).length === 0) {
      order = { _id: -1 };
    }

    try {
      filterQuery = JSON.parse(filterQuery);
      showBoth = filterQuery?.bothChecked;
    } catch (error) {
      filterQuery = {};
    }

    let where: { [key: string]: any } = filterQuery
      ? this.parseSerialFilterQuery(filterQuery)
      : {};

    if (where.purchase_invoice_name && !where.purchase_invoice_name.$exists) {
      delete where.purchase_invoice_name;
    }

    if (showBoth) {
      delete where.purchase_invoice_name;
      delete where.sales_invoice_name;
      where = {
        $and: [
          where,
          {
            $or: [
              { purchase_invoice_name: { $exists: true } },
              { sales_invoice_name: { $exists: true } },
              // { delivery_note: { $exists: true } },
              { sales_return_name: { $exists: true } },
            ],
          },
        ],
      };
      delete where.purchase_invoice_name;
      delete where.sales_invoice_name;
      delete where.sales_return_name;
      delete where.delivery_note;
    }

    // const results = await this.serialNoModel
    //   .find(where)
    //   .skip(skip)
    //   .limit(take)
    //   .sort(order);

    // const count = await this.serialNoModel.countDocuments(where);

    const [results, count] = await Promise.all([
      this.serialNoModel.find(where).skip(skip).limit(take).sort(order),
      this.serialNoModel.countDocuments(where),
    ]);

    return {
      docs: results || [],
      length: count,
      offset: skip,
    };
  }

  parseSerialFilterQuery(filterQuery: { [key: string]: any }) {
    Object.keys(filterQuery).forEach(key => {
      if (key !== '$or' && !SERIAL_FILTER_KEYS.includes(key)) {
        delete filterQuery[key];
      }
      if (['item_code', 'warehouse'].includes(key) && !filterQuery[key]) {
        delete filterQuery[key];
      }
    });
    return filterQuery;
  }

  // rseSerialFilterQuery(filterQuery: { [key: string]: any }) {
  //   Object.keys(filterQuery).forEach(key => {
  //     if (key !== '$or' && !SERIAL_FILTER_KEYS.includes(key)) {
  //       delete filterQuery[key];
  //     }
  //     if (['item_code', 'warehouse'].includes(key) && !filterQuery[key]) {
  //       delete filterQuery[key];
  //     }
  //   });

  //   return filterQuery;
  // }

  async listPurchasedSerial(purchase_invoice_name, skip, take, search = '') {
    const searchQuery: any = { purchase_invoice_name };

    if (search && search !== '') {
      searchQuery.serial_no = { $regex: search.toUpperCase() };
    }

    return {
      docs: await this.aggregateList(skip, take, searchQuery),
      length: await this.serialNoModel.count(searchQuery),
      offset: skip,
    };
  }

  async aggregateList(skip = 0, limit = 10, query, sort?) {
    return firstValueFrom(
      this.asyncAggregate([
        { $match: query },
        { $sort: { serial_no: -1 } },
        { $skip: skip },
        { $limit: limit },
      ]),
    );
  }

  async listDeliveredSerial(sales_invoice_name, search, skip = 0, take = 10) {
    const serialNoQuery: any = { sales_invoice_name };

    if (search && search !== '') {
      serialNoQuery.serial_no = { $regex: search.toUpperCase() };
    }
    const res = await this.aggregateList(skip, take, serialNoQuery);
    // res.sort((a, b) => {
    //   // Extract the numeric part from the "serial_no" using regular expressions
    //   const serialANumeric = parseInt(a.serial_no.match(/\d+$/)[0], 10);
    //   const serialBNumeric = parseInt(b.serial_no.match(/\d+$/)[0], 10);
    //   // Compare the numeric parts
    //   if (serialANumeric < serialBNumeric) {
    //     return 1;
    //   } else if (serialANumeric > serialBNumeric) {
    //     return -1;
    //   } else {
    //     return 0;
    //   }
    // });

    return {
      docs: res,
      length: await this.serialNoModel.count(serialNoQuery),
      offset: skip,
    };
  }

  async listReturnInvoicesSerials(
    serial_numbers: string[],
    sales_return_name: string[],
    offset: number,
    limit: number,
  ) {
    return {
      data: await this.aggregateList(offset, limit, {
        serial_no: { $in: serial_numbers },
        sales_return_name: { $in: sales_return_name },
      }),
      length: await this.serialNoModel.count({
        serial_no: { $in: serial_numbers },
        sales_return_name: { $in: sales_return_name },
      }),
      offset,
    };
  }

  async deleteOne(query, options?): Promise<any> {
    return await this.serialNoModel.deleteOne(query, options);
  }

  async deleteMany(query, options?): Promise<any> {
    return await this.serialNoModel.deleteMany(query, options);
  }

  async updateOne(query, options?) {
    const res = await this.serialNoModel.updateOne(query, options);
    return res;
  }

  async updateMany(query, options?) {
    return await this.serialNoModel.updateMany(query, options);
  }

  async insertMany(query, options?) {
    return await this.serialNoModel.insertMany(query, options);
  }

  asyncAggregate(query): Observable<any> {
    const promise = this.serialNoModel.aggregate(query);
    return new Observable(observer => {
      promise.then(
        settings => {
          observer.next(settings);
          observer.complete();
        },
        error => {
          observer.error(error);
        },
      );
    });
  }

  async count(query) {
    const res = await this.serialNoModel.count(query);
    return res;
  }

  async listSerialQuantity(
    skip: number = 0,
    take: number = 10,
    sort: any,
    filter_query: any,
  ) {
    let sortQuery = {};
    try {
      sortQuery = JSON.parse(sort);
    } catch (error) {
      sortQuery = { item_name: 'asc' };
    }
    sortQuery =
      Object.keys(sortQuery).length === 0 ? { item_name: 'asc' } : sortQuery;

    for (const key of Object.keys(sortQuery)) {
      sortQuery[key] = sortQuery[key].toUpperCase();
      if (sortQuery[key] === 'ASC') {
        sortQuery[key] = 1;
      }
      if (sortQuery[key] === 'DESC') {
        sortQuery[key] = -1;
      }
      if (!sortQuery[key]) {
        delete sortQuery[key];
      }
    }
    const purchaseInvoiceQuery = { purchase_invoice_name: { $exists: true } };
    const salesInvoiceQuery = { sales_invoice_name: { $exists: false } };

    const $and: any[] = [
      filter_query ? this.getFilterQuery(filter_query) : {},
      purchaseInvoiceQuery,
      salesInvoiceQuery,
    ];
    const results: any = await firstValueFrom(
      this.asyncAggregate([
        { $match: { $and } },
        {
          $group: {
            _id: {
              warehouse: '$warehouse',
              item_code: '$item_code',
              item_name: '$item_name',
            },
            total: { $sum: 1 },
          },
        },
        {
          $project: {
            _id: 0,
            warehouse: '$_id.warehouse',
            item_code: '$_id.item_code',
            item_name: '$_id.item_name',
            total: 1,
          },
        },
        { $sort: sortQuery },
      ]),
    );

    const length = results.length;
    return {
      docs: results.splice(skip, take) || [],
      length: length || 0,
      offset: skip,
    };
  }

  getFilterQuery(query: any) {
    const keys = Object.keys(query);
    keys.forEach(key => {
      if (typeof query[key] === 'string') {
        query[key] = {
          $regex: query[key],
          $options: 'i',
        };
      } else {
        delete query[key];
      }
    });
    return query;
  }
}

export class AggregatePaginationResponse {
  length: { total: string }[];
  docs: any[];
}
