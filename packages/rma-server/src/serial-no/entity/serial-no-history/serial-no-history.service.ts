// import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
// import { MongoRepository } from 'typeorm';
// import {
//   SerialNoHistory,
//   SerialNoHistoryInterface,
// } from './serial-no-history.entity';
import { of, from, Observable } from 'rxjs';
import { switchMap, concatMap, bufferCount } from 'rxjs/operators';
// import { MONGO_INSERT_MANY_BATCH_NUMBER } from '../../../constants/app-strings';
// import { MongoFindOneOptions } from 'typeorm/find-options/mongodb/MongoFindOneOptions';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import {
  SalesNoHistoryDocument,
  SerialNoHistory,
  SerialNoHistoryInterface,
} from '../../schema/serial-no-history.schema';

@Injectable()
export class SerialNoHistoryService {
  constructor(
    // @InjectRepository(SerialNoHistory)
    // private readonly serialNoRepository: MongoRepository<SerialNoHistory>,
    @InjectModel('SerialNoHistory')
    private serialNoModel: Model<SalesNoHistoryDocument>,
  ) {}

  async find(query?) {
    const res = await this.serialNoModel
      .find(query.where)
      .sort(query.order)
      .limit(query.limit);
    return res;
  }

  async create(serialNoPayload: SerialNoHistory) {
    // const serialNo = new SerialNoHistory();
    // Object.assign(serialNo, serialNoPayload);
    return await new this.serialNoModel(serialNoPayload).save();
  }

  async findOne(options) {
    return await this.serialNoModel.findOne(options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const $or = [{ serial_no: nameExp }];

    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.serialNoModel
      .find()
      .where(where)
      .skip(skip)
      .limit(take);
    return {
      docs: results || [],
      length: await this.serialNoModel.count(where).skip(skip).limit(take),
      offset: skip,
    };
  }

  async aggregateListForMaterialReceiptValidation(serialArray) {
    return await this.serialNoModel.aggregate([
      {
        $match: {
          serial_no: { $in: serialArray },
        },
      },
      {
        $group: {
          _id: '$serial_no',
          count: { $sum: 1 },
        },
      },
      {
        $match: {
          count: { $gt: 1 },
        },
      },
    ]);
  }

  async aggregateListForMaterialTransferValidation(serialArray) {
    return await this.serialNoModel.aggregate([
      {
        $match: {
          serial_no: { $in: serialArray },
        },
      },
      {
        $group: {
          _id: '$serial_no',
          lastEvent: { $last: '$eventType' },
        },
      },
      {
        $match: {
          lastEvent: {
            $nin: ['Serial Transfer Created', 'Serial Transfer Accepted'],
          },
        },
      },
    ]);
  }

  async aggregateListForMaterialTransferAndRND(serialArray) {
    return await this.serialNoModel.aggregate([
      {
        $match: {
          serial_no: { $in: serialArray },
        },
      },
      {
        $group: {
          _id: '$serial_no',
          lastEvent: { $last: '$eventType' },
        },
      },
      {
        $match: {
          lastEvent: {
            $nin: ['Material Issue', 'R&D Product'],
          },
        },
      },
    ]);
  }

  aggregateList(skip = 0, limit = 10, query, sort) {
    return this.asyncAggregate([
      { $match: query },
      { $skip: skip },
      { $limit: limit },
      { $sort: sort },
    ]);
  }

  async deleteOne(query, options?): Promise<any> {
    return await this.serialNoModel.deleteOne(query, options);
  }

  async deleteMany(query, options?): Promise<any> {
    return await this.serialNoModel.deleteMany(query, options);
  }

  async updateOne(query, options?) {
    return await this.serialNoModel.updateOne(query, options);
  }

  async updateMany(query, options?) {
    return await this.serialNoModel.updateMany(query, options);
  }

  async insertMany(query, options?) {
    const res = await this.serialNoModel.insertMany(query, options);
    return res;
  }

  asyncAggregate(query): Observable<any> {
    const promise = this.serialNoModel.aggregate(query);
    return new Observable(observer => {
      promise.then(
        settings => {
          observer.next(settings);
          observer.complete();
        },
        error => {
          observer.error(error);
        },
      );
    });
  }

  // addSerialHistory(serials: string[], update: SerialNoHistoryInterface) {
  //   return from(serials).pipe(
  //     map(serial => {
  //       // const serial_no_history = new SerialNoHistory();
  //       // Object.assign(serial_no_history, update);
  //       update.serial_no = serial;
  //       return update;
  //     }),
  //     bufferCount(MONGO_INSERT_MANY_BATCH_NUMBER),
  //     concatMap(data => {
  //       return from(this.insertMany(data, { ordered: false })).pipe(
  //         switchMap(() => of(true)),
  //       );
  //     }),
  //     switchMap(() => {
  //       return of(true);
  //     }),
  //   );
  // }
  addSerialHistory(serials: string[], update: SerialNoHistoryInterface) {
    return from(serials).pipe(
      bufferCount(1000),
      concatMap(batch => {
        const updates = batch.map(data => {
          const clonedUpdate = { ...update };
          clonedUpdate.serial_no = data;
          return clonedUpdate;
        });
        return from(this.insertMany(updates, { ordered: false })).pipe(
          switchMap(() => of(true)),
        );
      }),
    );
  }

  async count(query) {
    return await this.serialNoModel.count(query);
  }
}
