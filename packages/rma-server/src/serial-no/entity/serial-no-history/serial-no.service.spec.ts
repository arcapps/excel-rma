import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { SerialNoHistoryService } from './serial-no-history.service';

describe('SerialNoHistoryService', () => {
  let service: SerialNoHistoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SerialNoHistoryService,
        {
          provide: getModelToken('SerialNoHistory'),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<SerialNoHistoryService>(SerialNoHistoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
