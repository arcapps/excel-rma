import { IQuery } from '@nestjs/cqrs';

export class RetrieveSerialNoDocumentQuery implements IQuery {
  constructor(public readonly serial_no: string) {}
}
