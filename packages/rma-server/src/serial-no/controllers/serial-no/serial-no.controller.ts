import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { FileInterceptor } from '@nestjs/platform-express';
import { firstValueFrom } from 'rxjs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import {
  DELIVERY_NOTE,
  PURCHASE_RECEIPT,
} from '../../../constants/app-strings';
import { SerialQuantityListQueryDto } from '../../../constants/listing-dto/serial-quantity-list-query';
import { RetrieveSalesInvoiceReturnedSerialNoQuery } from '../../../serial-no/query/retrieve-sales-invoice-return-serial-no/retrieve-sales-invoice-return-serial-no.query'; // eslint-disable-line
import { SerialNoAggregateService } from '../../aggregates/serial-no-aggregate/serial-no-aggregate.service';
import { AssignSerialNoCommand } from '../../command/assign-serial-no/assign-serial-no.command';
import { AssignSerialDto } from '../../entity/serial-no/assign-serial-dto';
import {
  ValidateReturnSerialsDto,
  ValidateSerialsDto,
} from '../../entity/serial-no/serial-no-dto';
import { RetrieveDirectSerialNoQuery } from '../../query/get-direct-serial-no/retrieve-direct-serial-no.query';
import { RetrieveSerialNoHistoryQuery } from '../../query/get-serial-no-history/get-serial-no-history.query';
import { RetrieveSerialNoQuery } from '../../query/get-serial-no/retrieve-serial-no.query';
import { RetrieveSerialNoListQuery } from '../../query/list-serial-no/retrieve-serial-no-list.query';
import { RetrieveSalesInvoiceDeliveredSerialNoQuery } from '../../query/retrieve-sales-invoice-delivered-serial-no/retrieve-sales-invoice-delivered-serial-no.query'; // eslint-disable-line
import { ValidateSerialsQuery } from '../../query/validate-serial/validate-serial.query';
import { RetrieveSerialNoDocumentQuery } from '../../query/get-serial-document/retrieve-serial-no-document.query';

@Controller('serial_no')
export class SerialNoController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
    private readonly serialAggregateService: SerialNoAggregateService,
  ) {}

  @Get('v1/get/:serial_no')
  @UseGuards(TokenGuard)
  async getSerialNo(@Param('serial_no') serial_no) {
    return await this.queryBus.execute(new RetrieveSerialNoQuery(serial_no));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getSerialNoList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('sort') sort,
    @Query('query') query,
  ) {
    query = decodeURIComponent(query);
    return this.queryBus.execute(
      new RetrieveSerialNoListQuery(offset, limit, sort, query),
    );
  }

  @Get('v1/get_sales_invoice_delivered_serials')
  @UseGuards(TokenGuard)
  getSalesInvoiceDeliveredSerials(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('find') find,
    @Req() clientHttpRequest,
  ) {
    return this.queryBus.execute(
      new RetrieveSalesInvoiceDeliveredSerialNoQuery(
        offset,
        limit,
        search,
        find,
        clientHttpRequest,
      ),
    );
  }

  @Get('v1/get_sales_invoice_returned_serials')
  @UseGuards(TokenGuard)
  getSalesInvoiceReturnedSerials(
    @Query('offset') offset = '0',
    @Query('limit') limit = '15',
    @Query('invoice_name') salesInvoiceName = '',
  ) {
    return this.queryBus.execute(
      new RetrieveSalesInvoiceReturnedSerialNoQuery(
        salesInvoiceName,
        Number(offset),
        Number(limit),
      ),
    );
  }

  @Post('v1/get_purchase_invoice_delivered_serials')
  @UseGuards(TokenGuard)
  @UseInterceptors(FileInterceptor('file'))
  async getPurchaseInvoiceDeliveredSerials(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('purchase_invoice_name') purchase_invoice_name,
    @Req() clientHttpRequest,
  ) {
    if (!purchase_invoice_name) {
      throw new BadRequestException('Purchase Invoice Name is mandatory.');
    }

    return await this.serialAggregateService.getPurchaseInvoiceDeliveredSerials(
      purchase_invoice_name,
      search,
      +offset,
      +limit,
      clientHttpRequest,
    );
  }

  @Post('v1/assign')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  assignSerialNo(
    @Body() assignSerialPayload: AssignSerialDto,
    @Req() clientHttpRequest,
  ) {
    return this.commandBus.execute(
      new AssignSerialNoCommand(assignSerialPayload, clientHttpRequest),
    );
  }

  @Post('v1/validate')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseInterceptors(FileInterceptor('file'))
  validateSerialNo(
    @Req() clientHttpRequest,
    @Body() body: ValidateSerialsDto,
    @UploadedFile('file') file,
  ) {
    body.validateFor =
      body.validateFor === PURCHASE_RECEIPT ? body.validateFor : DELIVERY_NOTE;
    return this.queryBus.execute(
      new ValidateSerialsQuery(body, clientHttpRequest, file),
    );
  }

  @Post('v1/validate_return_serials')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseInterceptors(FileInterceptor('file'))
  validateReturnSerialNo(
    @Body() body: ValidateReturnSerialsDto,
    @UploadedFile('file') file,
  ) {
    if (file) {
      return firstValueFrom(
        this.serialAggregateService.validateBulkReturnSerialFile(file),
      );
    }

    return this.serialAggregateService.validateReturnSerials(body);
  }

  @Get('v1/get_history/:serial_no')
  @UseGuards(TokenGuard)
  async getSerialHistory(@Param('serial_no') serial_no) {
    return await this.queryBus.execute(
      new RetrieveSerialNoHistoryQuery(serial_no),
    );
  }

  @Get('v1/get_direct_serial/:serial_no')
  @UseGuards(TokenGuard)
  async getDirectSerialNo(@Param('serial_no') serial_no) {
    return await this.queryBus.execute(
      new RetrieveDirectSerialNoQuery(serial_no),
    );
  }

  @Get('v1/get_serial_no_document/:serial_no')
  @UseGuards(TokenGuard)
  async getSerialNoDocument(@Param('serial_no') serial_no) {
    return await this.queryBus.execute(
      new RetrieveSerialNoDocumentQuery(serial_no),
    );
  }

  @Post('v1/items-by-serials/:warehouse')
  @UseGuards(TokenGuard)
  getItemBySerial(@Param('warehouse') warehouse, @Body() serial_arr: string[]) {
    return this.serialAggregateService.retrieveItemsBySerialOrItemCode(
      warehouse,
      serial_arr,
    );
  }

  @Get('v1/list_serial_quantity')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async getSerialQuantity(@Query() query: SerialQuantityListQueryDto) {
    const { offset, limit, sort, filter_query } = query;
    let filter = {};
    try {
      filter = JSON.parse(filter_query);
    } catch {
      filter;
    }

    return await this.serialAggregateService.listSerialQuantity(
      Number(offset),
      Number(limit),
      sort,
      filter,
    );
  }
}
