import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';

export type StockEntryDocument = HydratedDocument<StockEntry>;

@Schema({ collection: 'stock_entry' })
export class StockEntry {
  @Prop()
  uuid: string;

  @Prop()
  docstatus?: 1;

  @Prop()
  names: string[];

  @Prop()
  createdOn: string;

  @Prop()
  created_by_email: string;

  @Prop()
  createdByEmail: string;

  @Prop()
  customer: string;

  @Prop()
  createdBy: string;

  @Prop()
  stock_entry_type: string;

  @Prop()
  set_posting_time: number;

  @Prop()
  status: string;

  @Prop()
  createdAt: Date;

  @Prop()
  company: string;

  @Prop()
  posting_date: string;

  @Prop()
  posting_time: string;

  @Prop()
  doctype: string;

  @Prop()
  inQueue: boolean;

  @Prop()
  isSynced: boolean;

  @Prop()
  description: string;

  @Prop()
  type: string;

  @Prop()
  remarks: string;

  @Prop()
  territory: string;

  @Prop()
  warrantyClaimUuid: string;

  @Prop()
  stock_voucher_number: string;

  @Prop({ required: false })
  default_s_warehouse: string;

  @Prop({ required: false })
  default_t_warehouse: string;

  @Prop()
  items: StockEntryItem[];

  @Prop({ type: mongoose.Schema.Types.Mixed })
  item_data: any;

  @Prop({ unique: true, type: mongoose.Schema.Types.Mixed })
  stock_id: any;

  @Prop()
  trin_stock_id: string;

  @Prop({ required: false })
  project: string;

  naming_series?: string;
}

export const StockEntrySchema = SchemaFactory.createForClass(StockEntry);

export class StockEntryItem {
  s_warehouse: string;
  t_warehouse: string;
  warehouse?: string;
  item_code: string;
  item_name: string;
  excel_serials?: string;
  qty: number;
  basic_rate?: number;
  has_serial_no: number;
  transfer_qty: number;
  warranty_date?: string;
  transferWarehouse: string;
  serial_no: string[];
  warranty?: stockEntryWarranty;
  delivery_note?: string;
  sales_invoice_name?: string;
}

export class stockEntryWarranty {
  salesWarrantyDate?: string;
  soldOn?: string;
  purchaseWarrantyDate?: string;
  purchasedOn?: string;
}
