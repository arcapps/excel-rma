// eslint-disable-line no-console
import { Injectable, BadRequestException } from '@nestjs/common';
import { StockEntryService } from '../../entities/stock-entry.service';
import { from, throwError, of, forkJoin } from 'rxjs';
import {
  ACCEPT,
  APPLICATION_JSON_CONTENT_TYPE,
  APP_JSON,
  AUTHORIZATION,
  BEARER_HEADER_VALUE_PREFIX,
  CONTENT_TYPE,
  DELIVERY_STATUS,
  // DOC_NAMES,
  INVALID_PROGRESS_STATE,
  INVALID_STOCK_ENTRY_STATUS,
  NON_SERIAL_ITEM,
  STOCK_ENTRY,
  STOCK_ENTRY_STATUS,
  VERDICT,
  WARRANTY_CLAIM_DOCTYPE,
} from '../../../constants/app-strings';
import { v4 as uuidv4 } from 'uuid';
import { DateTime } from 'luxon';

import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ServerSettings } from '../../../system-settings/schemas/server-settings.schema';
import { WarrantyStockEntryDto } from '../../entities/warranty-stock-entry-dto';
import { SerialNoService } from '../../../serial-no/entity/serial-no/serial-no.service';
import { StockEntry } from '../../schema/stock-entry.schema';
import { switchMap, map, toArray, concatMap, catchError } from 'rxjs/operators';
import { WarrantyClaimService } from '../../../warranty-claim/entity/warranty-claim/warranty-claim.service';
import { SerialNoHistoryService } from '../../../serial-no/entity/serial-no-history/serial-no-history.service';
import {
  EventType,
  SerialNoHistory,
  SerialNoHistoryInterface,
} from '../../../serial-no/schema/serial-no-history.schema';
import { StockEntryPoliciesService } from '../../../stock-entry/policies/stock-entry-policies/stock-entry-policies.service';
import { WarrantyClaimAggregateService } from '../../../warranty-claim/aggregates/warranty-claim-aggregate/warranty-claim-aggregate.service';
import { PROGRESS_STATUS } from '../../../constants/app-strings';
import { StockLedger } from '../../../stock-ledger/schema/stock-ledger.schema';
import { StockLedgerService } from '../../../stock-ledger/entity/stock-ledger/stock-ledger.service';
import { HttpService } from '@nestjs/axios';
import {
  ACCOUNT_API_ENDPOINT,
  FRAPPE_CLIENT_CANCEL,
  STOCK_ENTRY_API_ENDPOINT,
} from '../../../constants/routes';
import { TerritoryAggregateService } from '../../../customer/aggregates/territory-aggregate/territory-aggregate.service';

@Injectable()
export class WarrantyStockEntryAggregateService {
  constructor(
    private readonly stockEntryService: StockEntryService,
    private readonly settingService: SettingsService,
    private readonly serialService: SerialNoService,
    private readonly warrantyService: WarrantyClaimService,
    private readonly serialNoHistoryService: SerialNoHistoryService,
    private readonly stockEntryPoliciesService: StockEntryPoliciesService,
    private readonly warrantyAggregateService: WarrantyClaimAggregateService,
    private readonly stockLedgerService: StockLedgerService,
    private readonly http: HttpService,
    private readonly territory: TerritoryAggregateService,
  ) {}

  createDeliveryNote(deliveryNotes: WarrantyStockEntryDto[], req: any) {
    const deliveryNotesTemp = [...deliveryNotes];

    if (deliveryNotes.length > 1) {
      deliveryNotesTemp[0] = deliveryNotes.find(
        x => x.stock_entry_type === 'Delivered',
      );
      deliveryNotesTemp[1] = deliveryNotes.find(
        x => x.stock_entry_type === 'Returned',
      );
    }
    deliveryNotes = deliveryNotesTemp;

    const warrantyPayload: any = {};
    let deliveryNotesList: WarrantyStockEntryDto[] = [];
    let settingState = {} as ServerSettings;
    return from(deliveryNotes).pipe(
      concatMap(singleDeliveryNote => {
        Object.assign(warrantyPayload, singleDeliveryNote);
        return forkJoin({
          deliveryNote: of(singleDeliveryNote),
          validStockEntry:
            this.stockEntryPoliciesService.validateWarrantyStockEntry(
              warrantyPayload,
            ),
          validSerials:
            this.stockEntryPoliciesService.validateWarrantyStockSerials(
              warrantyPayload,
            ),
          warrantyPayload: of(warrantyPayload),
          settings: this.settingService.find(),
        });
      }),
      toArray(),
      switchMap(settings => {
        const COST_ACCOUNT_QUERY = ['account_type', '=', 'Cost of Goods Sold'];
        const params = {
          filters: JSON.stringify([COST_ACCOUNT_QUERY]),
        };
        return this.http
          .get(
            settings.find(x => x.settings).settings.authServerURL +
              ACCOUNT_API_ENDPOINT,
            {
              params,
              headers: {
                [AUTHORIZATION]:
                  BEARER_HEADER_VALUE_PREFIX + req.token.accessToken,
                [CONTENT_TYPE]: APP_JSON,
                [ACCEPT]: APPLICATION_JSON_CONTENT_TYPE,
              },
            },
          )
          .pipe(
            switchMap(account => {
              if (
                !account?.data?.data?.length ||
                !account?.data?.data[0].name
              ) {
                throwError(
                  () => new BadRequestException('Expense account not found'),
                );
              }
              const expense_account = account.data.data[0].name;
              let prevDeliveyNote = { name: '' };
              return from(deliveryNotes).pipe(
                concatMap((deliveryNote: WarrantyStockEntryDto) => {
                  deliveryNote.status = undefined;
                  deliveryNote.stock_voucher_number = uuidv4();
                  return this.makeStockEntry(
                    deliveryNote,
                    prevDeliveyNote,
                    req,
                    settings.find(x => x.settings).settings,
                    expense_account,
                  ).pipe(
                    map((res: any) => {
                      prevDeliveyNote = res;
                      deliveryNote.stock_id = res.stock_id;
                      return res;
                    }),
                    switchMap(() => {
                      return this.updateProgressState(deliveryNote);
                    }),
                  );
                }),
                toArray(),
              );
            }),
          );
      }),
      switchMap(() => {
        return forkJoin({
          warranty: from(
            this.warrantyService.findOne({
              where: { uuid: warrantyPayload.warrantyClaimUuid },
            }),
          ),
          settings: this.settingService.find(),
        }).pipe(
          switchMap(({ warranty, settings }) => {
            if (warranty.serial_no === undefined) {
              warranty.serial_no = '';
            }
            deliveryNotesList = warranty.progress_state;
            settingState = settings;
            return from(deliveryNotes).pipe(
              concatMap((deliveryNote: WarrantyStockEntryDto) => {
                return this.updateSerials(
                  deliveryNote,
                  warranty.serial_no,
                  settings,
                  warranty.claim_type === 'Third Party Non Warranty',
                );
              }),
              toArray(),
            );
          }),
        );
      }),
      switchMap(() => {
        return this.territory.getDefaultTerritory(req).pipe(
          switchMap(res => {
            const defaultTerritory = res?.length ? res[0]?.for_value : '';
            return from(deliveryNotesList).pipe(
              concatMap((deliveryNote: WarrantyStockEntryDto) => {
                return this.createSerialNoHistory(
                  deliveryNote,
                  settingState,
                  req,
                  defaultTerritory,
                );
              }),
              toArray(),
            );
          }),
        );
      }),
      switchMap(() => {
        return from(deliveryNotesList).pipe(
          concatMap((deliveryNote: WarrantyStockEntryDto) => {
            return this.syncProgressState(
              warrantyPayload.warrantyClaimUuid,
              deliveryNote,
            );
          }),
          toArray(),
        );
      }),
      switchMap(() => {
        return from(deliveryNotes).pipe(
          concatMap((deliveryNote: WarrantyStockEntryDto) => {
            return this.createStockLedger(
              deliveryNote,
              req.token,
              settingState,
            );
          }),
        );
      }),
      catchError(err => {
        return throwError(() => new BadRequestException(err));
      }),
    );
  }

  makeStatusHistory(uuid: string, req: any) {
    let defaultBranch = null;
    return forkJoin({
      warranty: this.warrantyService.findOne({ where: uuid }),
      settingState: this.settingService.find(),
      defaultTerritory: this.territory.getDefaultTerritory(req),
    }).pipe(
      switchMap(claim => {
        const defaultTerr = claim?.defaultTerritory?.length
          ? claim.defaultTerritory[0].for_value
          : null;
        defaultBranch = defaultTerr;
        if (
          claim.warranty.status_history[
            claim.warranty.status_history.length - 1
          ].verdict === VERDICT.DELIVER_TO_CUSTOMER
        ) {
          return throwError(
            new BadRequestException('Stock Entries Already Finalized'),
          );
        }
        this.serialNoHistoryService.updateMany(
          {
            document_no: { $eq: claim.warranty.claim_no },
          },
          {
            $set: {
              eventType: VERDICT.DELIVER_TO_CUSTOMER,
            },
          },
        );
        const statusHistoryDetails = {} as any;
        statusHistoryDetails.uuid = claim.warranty.uuid;
        statusHistoryDetails.time = claim.warranty.posting_time;
        statusHistoryDetails.posting_date = new DateTime(
          claim.settingState.timeZone,
        ).toFormat('yyyy-MM-dd');
        statusHistoryDetails.status_from =
          defaultTerr || req.token.territory[0];
        statusHistoryDetails.verdict = VERDICT.DELIVER_TO_CUSTOMER;
        statusHistoryDetails.description =
          claim.warranty.progress_state[0].description;
        statusHistoryDetails.created_by_email = req.token.email;
        statusHistoryDetails.created_by = req.token.name;
        switch (claim.warranty.progress_state[0].type) {
          case PROGRESS_STATUS.REPLACE:
            statusHistoryDetails.delivery_status = DELIVERY_STATUS.REPLACED;
            break;
          case PROGRESS_STATUS.UPGRADE:
            statusHistoryDetails.delivery_status = DELIVERY_STATUS.UPGRADED;
            break;
          case PROGRESS_STATUS.SPARE_PARTS:
            statusHistoryDetails.delivery_status = DELIVERY_STATUS.REPAIRED;
            break;
          default:
            return throwError(
              () => new BadRequestException(INVALID_PROGRESS_STATE),
            );
        }
        return this.warrantyAggregateService.addStatusHistory(
          statusHistoryDetails,
          req,
        );
      }),
      switchMap(() => {
        return from(
          this.warrantyService.updateOne(uuid, {
            $set: {
              delivery_branch: defaultBranch || req.token.territory[0],
            },
          }),
        );
      }),
      catchError(err => {
        return throwError(() => new BadRequestException(err));
      }),
    );
  }

  createStockLedger(
    payload: WarrantyStockEntryDto,
    token: any,
    settings: ServerSettings,
  ) {
    return this.createStockLedgerPayload(payload, token, settings).pipe(
      switchMap((stockLedgers: StockLedger[]) => {
        return from(stockLedgers).pipe(
          switchMap(stockLedger => {
            const res = from(this.stockLedgerService.create(stockLedger));
            return res;
          }),
        );
      }),
    );
  }

  createStockLedgerPayload(
    res: WarrantyStockEntryDto,
    token: any,
    settings: ServerSettings,
  ) {
    return this.settingService.getFiscalYear(settings).pipe(
      switchMap((fiscalYear: string) => {
        const date = new DateTime(settings.timeZone).toJSDate();
        return from(res.items).pipe(
          concatMap((item: any) => {
            // fetch total qty in warehouse
            return this.stockLedgerService
              .asyncAggregate([
                {
                  $group: {
                    _id: {
                      warehouse: '$warehouse',
                      item_code: '$item_code',
                    },
                    stockAvailability: {
                      $sum: '$actual_qty',
                    },
                  },
                },
                {
                  $lookup: {
                    from: 'item',
                    localField: '_id.item_code',
                    foreignField: 'item_code',
                    as: 'item',
                  },
                },
                {
                  $match: {
                    'item.item_code': `${item.item_code}`,
                    '_id.warehouse': `${item.s_warehouse}`,
                    stockAvailability: { $gt: 0 },
                  },
                },
                {
                  $unwind: '$item',
                },
              ])
              .pipe(
                switchMap(data => {
                  const available_stock = data[0]?.stockAvailability
                    ? data[0].stockAvailability
                    : 0;

                  // Returned
                  if (res.stock_entry_type === STOCK_ENTRY_STATUS.returned) {
                    let current_valuation_rate;
                    let new_quantity;
                    let pre_incoming_rate;
                    let incoming_rate;

                    // fetch created invoice
                    return this.stockLedgerService
                      .asyncAggregate([
                        {
                          $match: {
                            voucher_no: `${item.sales_invoice_name}`,
                          },
                        },
                      ])
                      .pipe(
                        switchMap((created_sales_invoice: StockLedger) => {
                          // fetch current valuation of warehouse
                          return this.stockLedgerService
                            .asyncAggregate([
                              {
                                $match: {
                                  item_code: `${item.item_code}`,
                                  warehouse: `${item.s_warehouse}`,
                                  actual_qty: { $gt: 0 },
                                },
                              },
                              {
                                $sort: {
                                  modified: -1,
                                },
                              },
                            ])
                            .pipe(
                              switchMap(latest_stock_ledger => {
                                const stockPayload = new StockLedger();
                                // treated as sold from our warehouse
                                if (
                                  Object.keys(created_sales_invoice).length > 0
                                ) {
                                  incoming_rate = created_sales_invoice[0]
                                    ?.valuation_rate
                                    ? created_sales_invoice[0]?.valuation_rate
                                    : latest_stock_ledger[0]?.valuation_rate;
                                  stockPayload.incoming_rate = incoming_rate
                                    ? incoming_rate
                                    : 0;
                                  current_valuation_rate =
                                    latest_stock_ledger[0]?.valuation_rate > 0
                                      ? latest_stock_ledger[0]?.valuation_rate
                                      : incoming_rate;
                                  pre_incoming_rate = latest_stock_ledger[0]
                                    ?.incoming_rate
                                    ? latest_stock_ledger[0]?.incoming_rate
                                    : 0;
                                }
                                // treated as third party
                                else {
                                  current_valuation_rate =
                                    latest_stock_ledger[0]?.valuation_rate > 0
                                      ? latest_stock_ledger[0]?.valuation_rate
                                      : incoming_rate;
                                  pre_incoming_rate = latest_stock_ledger[0]
                                    ?.incoming_rate
                                    ? latest_stock_ledger[0]?.incoming_rate
                                    : incoming_rate;
                                  stockPayload.incoming_rate =
                                    current_valuation_rate
                                      ? current_valuation_rate
                                      : 0;
                                }
                                new_quantity = available_stock + item.qty;
                                if (!available_stock) {
                                  new_quantity = item.qty * -1;
                                }

                                stockPayload.name = uuidv4();
                                stockPayload.modified = date;
                                stockPayload.modified_by = token.email;
                                stockPayload.actual_qty = 1;
                                if (
                                  pre_incoming_rate !==
                                  stockPayload.incoming_rate
                                ) {
                                  stockPayload.valuation_rate =
                                    parseFloat(
                                      this.calculateValuationRate(
                                        available_stock,
                                        stockPayload.actual_qty,
                                        stockPayload.incoming_rate,
                                        current_valuation_rate,
                                        new_quantity,
                                      ),
                                    ) || 0;
                                  if (!stockPayload.valuation_rate) {
                                    stockPayload.valuation_rate = 0;
                                  }
                                } else {
                                  stockPayload.valuation_rate =
                                    current_valuation_rate;
                                }
                                stockPayload.actual_qty =
                                  item.qty < 0 ? item.qty * -1 : item.qty;
                                stockPayload.voucher_no = res.stock_id;
                                stockPayload.batch_no = '';
                                stockPayload.warehouse = item?.s_warehouse
                                  ? item.s_warehouse
                                  : item.warehouse;
                                stockPayload.balance_qty = new_quantity;
                                if (stockPayload.valuation_rate) {
                                  stockPayload.balance_value =
                                    parseFloat(
                                      (
                                        stockPayload.balance_qty *
                                        stockPayload.valuation_rate
                                      ).toFixed(2),
                                    ) || 0;
                                } else {
                                  stockPayload.balance_value =
                                    parseFloat(
                                      (stockPayload.balance_qty * 0).toFixed(2),
                                    ) || 0;
                                }
                                stockPayload.item_code = item.item_code;
                                stockPayload.posting_date = date;
                                stockPayload.posting_time = date;
                                stockPayload.voucher_type = STOCK_ENTRY;
                                stockPayload.voucher_detail_no = '';
                                stockPayload.outgoing_rate = 0;
                                stockPayload.company = settings.defaultCompany;
                                stockPayload.fiscal_year = fiscalYear;
                                return of(stockPayload);
                              }),
                            );
                        }),
                      );
                  }
                  // Delivered
                  if (res.stock_entry_type === STOCK_ENTRY_STATUS.delivered) {
                    // fetch current valuation of warehouse
                    return this.stockLedgerService
                      .asyncAggregate([
                        {
                          $match: {
                            item_code: `${item.item_code}`,
                            warehouse: `${item.s_warehouse}`,
                            actual_qty: { $gt: 0 },
                          },
                        },
                        {
                          $sort: {
                            modified: -1,
                          },
                        },
                      ])
                      .pipe(
                        switchMap(latest_stock_ledger => {
                          const stockPayload = new StockLedger();
                          stockPayload.name = uuidv4();
                          stockPayload.modified = date;
                          stockPayload.modified_by = token.email;
                          stockPayload.actual_qty = -item.qty;
                          stockPayload.voucher_no = res.stock_id;
                          stockPayload.batch_no = '';
                          stockPayload.incoming_rate = 0;
                          stockPayload.warehouse = item?.s_warehouse
                            ? item.s_warehouse
                            : item.warehouse;
                          stockPayload.item_code = item.item_code;
                          if (
                            latest_stock_ledger &&
                            latest_stock_ledger[0]?.valuation_rate > 0
                          ) {
                            stockPayload.valuation_rate =
                              latest_stock_ledger[0].valuation_rate;
                          } else {
                            stockPayload.valuation_rate = 0;
                          }
                          stockPayload.balance_qty = available_stock - item.qty;

                          if (stockPayload.valuation_rate) {
                            stockPayload.balance_value =
                              parseFloat(
                                (
                                  stockPayload.balance_qty *
                                  stockPayload.valuation_rate
                                ).toFixed(2),
                              ) || 0;
                          } else {
                            stockPayload.balance_value =
                              parseFloat(
                                (stockPayload.balance_qty * 0).toFixed(2),
                              ) || 0;
                          }
                          stockPayload.posting_date = date;
                          stockPayload.posting_time = date;
                          stockPayload.voucher_type = STOCK_ENTRY;
                          stockPayload.voucher_detail_no = '';

                          stockPayload.outgoing_rate = 0;
                          stockPayload.company = settings.defaultCompany;
                          stockPayload.fiscal_year = fiscalYear;
                          return of(stockPayload);
                        }),
                      );
                  }
                }),
              );
          }),
          toArray(),
          switchMap(data => {
            return of(data);
          }),
        );
      }),
    );
  }

  calculateValuationRate(
    preQty,
    incomingQty,
    incomingRate,
    preValuation,
    totalQty,
  ) {
    const result =
      (preQty * preValuation + incomingQty * incomingRate) / totalQty;
    return result.toFixed(2);
  }

  makeStockEntry(
    deliveryNote: WarrantyStockEntryDto,
    prevDeliveyNote: any,
    req: any,
    settings: ServerSettings,
    expense_account: string,
  ) {
    const stockEntry: StockEntry = this.setStockEntryDefaults(
      deliveryNote,
      req,
      settings,
    );
    stockEntry.items[0].serial_no = deliveryNote.items[0].serial_no;
    return this.getAssignStockId(stockEntry, settings).pipe(
      switchMap((stockEntryPayload: StockEntry) => {
        return this.createFrappeStockEntry(
          stockEntryPayload,
          prevDeliveyNote.stock_id,
          settings,
          req,
          expense_account,
        ).pipe(
          switchMap(res => {
            stockEntryPayload.stock_id = res.data.data.name;
            return from(this.stockEntryService.create(stockEntryPayload));
          }),
        );
      }),
    );
  }

  createFrappeStockEntry(
    stockEntryPayload,
    deliveryNoteName,
    settings,
    req,
    expense_account,
  ) {
    const stockEntryTemp = { ...stockEntryPayload };
    stockEntryTemp.items = stockEntryTemp.items.map(item => {
      return {
        ...item,
        // warehouse: undefined,
        name: undefined,
        serial_no: '',
        t_warehouse:
          stockEntryTemp.stock_entry_type === 'Returned'
            ? item.s_warehouse
            : '',
        s_warehouse:
          stockEntryTemp.stock_entry_type === 'Delivered'
            ? item.s_warehouse
            : '',
        rate: 0,
        price_list_rate: 0,
        expense_account,
      };
    });
    if (stockEntryTemp.stock_entry_type === 'Returned') {
      stockEntryTemp.stock_entry_type = 'Material Receipt';
      stockEntryTemp.naming_series = 'WSDR-.YYYY.-';
      // stockEntryTemp.return_against = deliveryNoteName;
    }
    if (stockEntryTemp.stock_entry_type === 'Delivered') {
      stockEntryTemp.stock_entry_type = 'Material Issue';
      stockEntryTemp.naming_series = 'WSD-.YYYY.-';
    }

    stockEntryTemp.status = undefined;
    stockEntryTemp.price_list_rate = 0.0;
    return this.http
      .post(settings.authServerURL + STOCK_ENTRY_API_ENDPOINT, stockEntryTemp, {
        headers: {
          [AUTHORIZATION]: BEARER_HEADER_VALUE_PREFIX + req.token.accessToken,
          [CONTENT_TYPE]: APP_JSON,
          [ACCEPT]: APPLICATION_JSON_CONTENT_TYPE,
        },
      })
      .pipe(
        catchError(error => {
          if (error.response.status === 417) {
            return throwError(
              new BadRequestException(error.response.data.exception),
            );
          } else {
            return throwError(new BadRequestException(error));
          }
          // Handle the error and return a custom error message as an observable
        }),
      );
  }

  updateProgressState(deliveryNote: any) {
    deliveryNote.isSync = false;
    let serialData = {} as any;
    switch (deliveryNote.stock_entry_type) {
      case STOCK_ENTRY_STATUS.returned:
        serialData = {
          damaged_serial: deliveryNote.items[0].serial_no,
          damage_warehouse: deliveryNote.items[0].s_warehouse,
          damage_product: deliveryNote.items[0].item_name,
        };
        break;
      case STOCK_ENTRY_STATUS.delivered:
        serialData = {
          replace_serial: deliveryNote.items[0].serial_no,
          replace_warehouse: deliveryNote.items[0].s_warehouse,
          replace_product: deliveryNote.items[0].item_name,
        };
        break;
      default:
        return throwError(
          () => new BadRequestException(INVALID_STOCK_ENTRY_STATUS),
        );
    }
    return from(
      this.warrantyService.updateOne(
        { uuid: deliveryNote.warrantyClaimUuid },
        {
          $push: {
            progress_state: deliveryNote,
          },
          $set: serialData,
        },
      ),
    );
  }

  createSerialNoHistory(
    deliveryNote: any,
    settings: ServerSettings,
    req: any,
    defaultTerritory?,
  ) {
    if (deliveryNote.isSync) {
      return of({});
    }
    if (deliveryNote.items[0].serial_no[0] === '') {
      return of({});
    }
    const serialHistory: SerialNoHistoryInterface = {};
    serialHistory.naming_series = deliveryNote.naming_series;
    serialHistory.serial_no = deliveryNote.items[0].serial_no[0];
    serialHistory.created_by = req.token.fullName;
    serialHistory.created_on = new DateTime(settings.timeZone).toJSDate();
    serialHistory.document_no = deliveryNote.stock_voucher_number;

    serialHistory.readable_document_no = deliveryNote.stock_id;

    serialHistory.document_type = WARRANTY_CLAIM_DOCTYPE;
    serialHistory.eventDate = new DateTime(settings.timeZone);
    serialHistory.parent_document = deliveryNote.warrantyClaimUuid;
    switch (deliveryNote.stock_entry_type) {
      case STOCK_ENTRY_STATUS.returned:
        const verdict = Object.keys(VERDICT).find(
          key => VERDICT[key] === VERDICT.RECEIVED_FROM_CUSTOMER,
        );
        const event = EventType[verdict];
        serialHistory.eventType = event;
        serialHistory.transaction_from = deliveryNote.customer;
        serialHistory.transaction_to = !deliveryNote.items[0].warehouse
          ? deliveryNote.customer
          : deliveryNote.items[0].warehouse;
        break;
      case STOCK_ENTRY_STATUS.delivered:
        const verdict_key = Object.keys(VERDICT).find(
          key => VERDICT[key] === VERDICT.DELIVER_TO_CUSTOMER,
        );
        const eventType = EventType[verdict_key];
        serialHistory.eventType = eventType;
        serialHistory.transaction_from =
          defaultTerritory || req.token.territory[0];
        serialHistory.transaction_to = !deliveryNote.customer
          ? defaultTerritory || req.token.territory[0]
          : deliveryNote.customer;
        break;
      default:
        break;
    }
    return this.serialNoHistoryService.addSerialHistory(
      [deliveryNote.items[0].serial_no],
      serialHistory,
    );
  }

  syncProgressState(uuid: string, deliveryNote: any) {
    return from(
      this.warrantyService.updateOne(
        {
          uuid,
          'progress_state.stock_voucher_number':
            deliveryNote.stock_voucher_number,
        },
        {
          $set: {
            'progress_state.$.isSync': true,
          },
        },
      ),
    );
  }

  updateSerials(
    deliveryNote: any,
    serial_no: string,
    settings: ServerSettings,
    isThirdParty?: boolean,
  ) {
    if (serial_no === '') {
      return of(true);
    }
    // if (deliveryNote.isSync) {
    //   return of(true);
    // }
    if (
      deliveryNote.items[0].serial_no &&
      deliveryNote.stock_entry_type === STOCK_ENTRY_STATUS.delivered
    ) {
      deliveryNote.delivery_note = deliveryNote.stock_id;
      return this.updateSerialItem(deliveryNote, serial_no, settings);
    }
    const purchase_invoice_name = isThirdParty
      ? deliveryNote.stock_id
      : undefined;
    const update = {
      delivery_note: deliveryNote.stock_id,
      warehouse: deliveryNote.set_warehouse,
      purchase_invoice_name,
    };
    if (!purchase_invoice_name) delete update.purchase_invoice_name;
    deliveryNote.delivery_note = deliveryNote.stock_voucher_number;
    return from(
      this.serialService.updateOne(
        { serial_no: deliveryNote.items[0].serial_no },
        {
          $set: update,
          $unset: {
            customer: '',
            'warranty.salesWarrantyDate': '',
            'warranty.soldOn': '',
            sales_invoice_name: '',
            claim_no: '',
          },
        },
      ),
    );
  }

  updateSerialItem(payload: any, serial_no: string, settings: ServerSettings) {
    return from(
      this.warrantyService.findOne({
        where: {
          uuid: payload.warrantyClaimUuid,
          // progress_state: {
          //   $elemMatch: {
          //     items: {
          //       $elemMatch: {
          //         serial_no,
          //       },
          //     },
          //   },
          // },
        },
      }),
    ).pipe(
      switchMap(state => {
        // eslint-disable-next-line
        // console.log('During Claim update Here', state);
        return from(
          this.serialService.updateOne(
            { serial_no: payload.items[0].serial_no },
            {
              $set: {
                customer: state.customer_code,
                'warranty.salesWarrantyDate': state.warranty_end_date
                  ? state.warranty_end_date
                  : '',
                'warranty.soldOn': new DateTime(settings.timeZone).toJSDate(),
                sales_invoice_name: state.invoice_no,
                delivery_note: payload.delivery_note,
              },
              // $unset: { warehouse: 1 },
            },
          ),
        );
      }),
    );
  }

  setStockEntryDefaults(
    payload: WarrantyStockEntryDto,
    clientHttpRequest,
    settings: ServerSettings,
  ): StockEntry {
    const stockEntry = new StockEntry();
    Object.assign(stockEntry, payload);
    stockEntry.uuid = uuidv4();
    stockEntry.doctype = STOCK_ENTRY;
    stockEntry.createdOn = payload.posting_date;
    stockEntry.createdAt = new DateTime(settings.timeZone).toJSDate();
    stockEntry.createdByEmail = clientHttpRequest.token.email;
    stockEntry.createdBy = clientHttpRequest.token.fullName;
    stockEntry.stock_voucher_number = payload.stock_voucher_number;
    stockEntry.status = STOCK_ENTRY_STATUS.in_transit;
    stockEntry.isSynced = false;
    stockEntry.inQueue = true;
    stockEntry.docstatus = 1;
    return stockEntry;
  }

  removeStockEntry(uuid: string, req) {
    return from(this.stockEntryService.findOne({ where: uuid })).pipe(
      switchMap(stockEntry => {
        return this.stockEntryPoliciesService
          .validateCancelWarrantyStockEntry(
            stockEntry.warrantyClaimUuid,
            this.getSerialString(stockEntry.items.find(x => x).serial_no),
          )
          .pipe(
            switchMap(() => {
              return this.settingService.find().pipe(
                switchMap(settings => {
                  return this.cancelDoc(
                    'Stock Entry',
                    stockEntry.stock_id,
                    settings,
                    req,
                  ).pipe(
                    catchError(error => {
                      return throwError(
                        new BadRequestException(error.response.data.exception),
                      );
                    }),
                  );
                }),
              );
            }),
            switchMap(() => {
              if (
                this.getSerialString(
                  stockEntry.items.find(x => x).serial_no,
                ) === NON_SERIAL_ITEM
              ) {
                // for NON SERIAL ITEM
                this.stockEntryService.deleteOne({
                  uuid: stockEntry.uuid,
                });
                this.warrantyService.updateOne(
                  { uuid: stockEntry.warrantyClaimUuid },
                  {
                    $unset: {
                      damage_product: '',
                      damage_warehouse: '',
                      damaged_serial: '',
                    },
                  },
                );
              } else {
                // for SERIAL ITEM
                if (
                  stockEntry.stock_entry_type === STOCK_ENTRY_STATUS.delivered
                ) {
                  return from(
                    this.serialNoHistoryService.find({
                      where: {
                        serial_no: stockEntry.items.find(x => x)?.serial_no,
                      },
                      order: { created_on: -1 },
                      limit: 2,
                    }),
                  ).pipe(
                    switchMap(history => {
                      // const historyTemp = [history[1]];
                      return this.serialService.updateOne(
                        {
                          serial_no: stockEntry.items.find(x => x)?.serial_no,
                        },
                        {
                          $unset: {
                            customer: '',
                            'warranty.salesWarrantyDate': '',
                            'warranty.soldOn': '',
                            delivery_note: '',
                            sales_invoice_name: '',
                            sales_return_name: '',
                          },
                          // $set: {
                          //   warehouse: historyTemp.find(x => x)?.transaction_to,
                          // },
                        },
                      );
                    }),
                  );
                }
                if (
                  stockEntry.stock_entry_type === STOCK_ENTRY_STATUS.returned
                ) {
                  return from(
                    this.warrantyService.findOne({
                      where: {
                        uuid: stockEntry.warrantyClaimUuid,
                      },
                    }),
                  ).pipe(
                    switchMap(claim => {
                      return this.resetCancelledSerialItem(
                        stockEntry,
                        claim.claim_type === 'Third Party Non Warranty',
                      );
                    }),
                  );
                }
                return of(true);
              }
              return of(true);
            }),
            switchMap(() => {
              return from(
                this.warrantyService.findOne({
                  where: { uuid: stockEntry.warrantyClaimUuid },
                }),
              );
            }),
            switchMap(warranty => {
              // DELETE SERIAL HISTORY OF WSD
              if (
                stockEntry.stock_entry_type === STOCK_ENTRY_STATUS.delivered
              ) {
                warranty.progress_state.forEach(state => {
                  state.items.forEach(element => {
                    if (
                      element.stock_entry_type === STOCK_ENTRY_STATUS.delivered
                    ) {
                      this.serialNoHistoryService.deleteOne({
                        document_no: state.stock_voucher_number,
                      });
                    }
                  });
                });
                return from(
                  this.warrantyService.updateOne(
                    { uuid: stockEntry.warrantyClaimUuid },
                    {
                      $unset: {
                        replace_warehouse: '',
                        replace_product: '',
                        replace_serial: '',
                      },
                    },
                  ),
                );
              }
              // DELETE SERIAL HISTORY OF WSDR
              else if (
                stockEntry.stock_entry_type === STOCK_ENTRY_STATUS.returned
              ) {
                warranty.progress_state.forEach(state => {
                  state.items.forEach(element => {
                    if (
                      element.stock_entry_type === STOCK_ENTRY_STATUS.returned
                    ) {
                      this.serialNoHistoryService.deleteOne({
                        document_no: state.stock_voucher_number,
                      });
                    }
                  });
                });
              }
              return of([]);
            }),
            switchMap(() => {
              return from(
                this.stockEntryService.deleteOne({
                  uuid: stockEntry.uuid,
                }),
              );
            }),
            switchMap(() => {
              return this.revertStatusHistory(stockEntry.warrantyClaimUuid);
            }),
            switchMap(() => {
              return from(
                this.warrantyService.updateOne(
                  { uuid: stockEntry.warrantyClaimUuid },
                  {
                    $pull: {
                      progress_state: {
                        stock_voucher_number: stockEntry.stock_voucher_number,
                      },
                    },
                  },
                ),
              );
            }),
            switchMap(() => {
              if (
                stockEntry.items.some(
                  item =>
                    this.getSerialString(item.serial_no) !== NON_SERIAL_ITEM,
                )
              ) {
                return from(
                  this.serialNoHistoryService.deleteOne({
                    document_no: stockEntry.stock_voucher_number,
                  }),
                ).pipe();
              }
              return of(true);
            }),
            switchMap(() => {
              return from(
                this.stockLedgerService.deleteMany({
                  voucher_no: stockEntry.stock_id,
                }),
              );
            }),
          )
          .pipe(
            catchError(error => {
              return throwError(
                new BadRequestException(error.response.message),
              );
            }),
          );
      }),
    );
  }

  cancelDoc(doctype, docName, settings: ServerSettings, req) {
    const doc = {
      doctype,
      name: docName,
    };
    return this.http.post(settings.authServerURL + FRAPPE_CLIENT_CANCEL, doc, {
      headers: {
        [AUTHORIZATION]: BEARER_HEADER_VALUE_PREFIX + req.token.accessToken,
      },
    });
  }

  revertStatusHistory(uuid: string) {
    return from(
      this.warrantyService.findOne({
        where: {
          uuid,
          status_history: {
            $elemMatch: {
              verdict: VERDICT.DELIVER_TO_CUSTOMER,
            },
          },
        },
      }),
    ).pipe(
      switchMap(res => {
        if (!res) {
          return of({});
        }
        return this.warrantyAggregateService.removeStatusHistory({ uuid });
      }),
    );
  }

  resetCancelledSerialItem(stockEntry: StockEntry, isThirdParty: boolean) {
    return from(
      this.warrantyService.findOne({
        where: { uuid: stockEntry.warrantyClaimUuid },
      }),
    ).pipe(
      switchMap(warranty => {
        if (!warranty) {
          return from(
            this.serialService.updateOne(
              {
                serial_no: stockEntry.items.find(x => x).serial_no,
              },
              {
                $set: {
                  customer: stockEntry.customer,
                  warehouse: stockEntry.items[0].warehouse,
                  'warranty.salesWarrantyDate':
                    stockEntry.items[0].warranty?.salesWarrantyDate,
                  'warranty.soldOn': stockEntry.items[0].warranty?.soldOn,
                  sales_invoice_name: stockEntry.items[0].sales_invoice_name,
                  delivery_note: stockEntry.items[0].delivery_note,
                },
              },
            ),
          );
        }
        return from(
          this.serialNoHistoryService.find({
            where: { serial_no: stockEntry.items.find(x => x)?.serial_no },
            order: { created_on: 1 },
          }),
        ).pipe(
          switchMap((serialHistory: SerialNoHistory[]) => {
            const deliveredSerial = serialHistory.find(
              event => event.eventType === 'Serial Delivered',
            );
            return from(
              this.serialService.updateOne(
                {
                  serial_no: stockEntry.items.find(x => x)?.serial_no,
                },
                {
                  $set: {
                    'warranty.salesWarrantyDate': warranty.received_on,
                    'warranty.soldOn': warranty.received_on,
                    warehouse: deliveredSerial?.transaction_from,
                    customer: deliveredSerial?.transaction_to,
                    sales_invoice_name: deliveredSerial?.parent_document,
                    // last entry is the one which is to be deleted so we get the warehouse of the second last entry
                    delivery_note: isThirdParty
                      ? null
                      : deliveredSerial?.document_no,
                  },
                },
              ),
            );
          }),
        );
      }),
    );
  }

  // assigning stock id function
  getAssignStockId(stockPayload: StockEntry, serverSettings: ServerSettings) {
    const date = new DateTime(serverSettings.timeZone).year;
    let $match: any;
    if (stockPayload.stock_entry_type === STOCK_ENTRY_STATUS.returned) {
      $match = {
        stock_entry_type: STOCK_ENTRY_STATUS.returned,
      };
    } else if (stockPayload.stock_entry_type === STOCK_ENTRY_STATUS.delivered) {
      $match = {
        stock_entry_type: STOCK_ENTRY_STATUS.delivered,
      };
    }
    const $sort: any = {
      createdAt: -1,
    };
    const $limit: any = 1;
    return this.stockEntryService
      .asyncAggregate([{ $match }, { $sort }, { $limit }])
      .pipe(
        map((result: any) => {
          const incrementor = 1;
          if (stockPayload.stock_entry_type === STOCK_ENTRY_STATUS.returned) {
            stockPayload.stock_id = `WSDR-${date}-${incrementor}`;
          } else if (
            stockPayload.stock_entry_type === STOCK_ENTRY_STATUS.delivered
          ) {
            stockPayload.stock_id = `WSD-${date}-${incrementor}`;
          }
          return stockPayload;
        }),
      );
  }

  getSerialString(serials): string {
    if (typeof serials === 'object') {
      return serials[0];
    }
    return serials;
  }
}
