import { HttpService } from '@nestjs/axios';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
  NotImplementedException,
} from '@nestjs/common';
import { DateTime } from 'luxon';
import { firstValueFrom, forkJoin, from, of, throwError } from 'rxjs';
import {
  catchError,
  concatMap,
  map,
  mergeMap,
  switchMap,
  toArray,
} from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import {
  ACCEPT,
  ACCEPT_STOCK_ENTRY_JOB,
  APPLICATION_JSON_CONTENT_TYPE,
  APP_JSON,
  AUTHORIZATION,
  BEARER_HEADER_VALUE_PREFIX,
  CONTENT_TYPE,
  CREATE_STOCK_ENTRY_JOB,
  INVALID_FILE,
  INVALID_STOCK_ENTRY_TYPE,
  REJECT_STOCK_ENTRY_JOB,
  STOCK_ENTRY,
  STOCK_ENTRY_NOT_FOUND,
  STOCK_ENTRY_STATUS,
  STOCK_ENTRY_TYPE,
  STOCK_MATERIAL_TRANSFER,
  STOCK_OPERATION,
} from '../../../constants/app-strings';
import { SerialNoService } from '../../../serial-no/entity/serial-no/serial-no.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ServerSettings } from '../../../system-settings/schemas/server-settings.schema';
import { StockEntryDto } from '../../entities/stock-entry-dto';
import { StockEntry, StockEntryItem } from '../../schema/stock-entry.schema';
import { StockEntryService } from '../../entities/stock-entry.service';
import { StockEntryPoliciesService } from '../../policies/stock-entry-policies/stock-entry-policies.service';

import { getUserPermissions } from '../../../constants/agenda-job';
import {
  FRAPPE_CLIENT_CANCEL,
  POST_STOCK_PRINT_ENDPOINT,
  STOCK_ENTRY_API_ENDPOINT,
} from '../../../constants/routes';
import { SerialNoHistoryService } from '../../../serial-no/entity/serial-no-history/serial-no-history.service';
import { StockEntrySyncService } from '../../../stock-entry/schedular/stock-entry-sync/stock-entry-sync.service';
import { StockLedger } from '../../../stock-ledger/schema/stock-ledger.schema';
import { StockLedgerService } from '../../../stock-ledger/entity/stock-ledger/stock-ledger.service';
import * as moment from 'moment';
@Injectable()
export class StockEntryAggregateService {
  constructor(
    private readonly stockEntryService: StockEntryService,
    private readonly stockEntryPolicies: StockEntryPoliciesService,
    private readonly http: HttpService,
    private readonly settingService: SettingsService,
    private readonly serialHistoryService: SerialNoHistoryService,
    private readonly serialNoService: SerialNoService,
    private readonly stockEntrySyncService: StockEntrySyncService,
    private readonly stockLedgerService: StockLedgerService,
  ) {}

  createStockEntry(payload: StockEntryDto, req) {
    return this.settingService.find().pipe(
      switchMap(settings => {
        if (payload.status === STOCK_ENTRY_STATUS.draft || !payload.uuid) {
          return this.stockEntryPolicies
            .validateStockPermission(
              payload.stock_entry_type,
              STOCK_OPERATION.create,
              req,
            )
            .pipe(
              switchMap(res => {
                return this.saveDraft(payload, req);
              }),
              catchError(err => {
                return err;
              }),
            );
        }
        return this.stockEntryPolicies.validateStockEntry(payload).pipe(
          switchMap(() => {
            return this.createFrappeStockEntry(
              payload,
              settings,
              req.token,
            ).pipe(
              switchMap(frappeResponse => {
                return this.stockEntryPolicies
                  .validateStockPermission(
                    payload.stock_entry_type,
                    STOCK_OPERATION.submit,
                    req,
                  )
                  .pipe(
                    switchMap(() => {
                      return from(
                        this.stockEntryService.findOne({
                          where: { uuid: payload.uuid },
                        }),
                      );
                    }),
                    switchMap(stockEntry => {
                      stockEntry.stock_id = payload.stock_id;
                      if (!stockEntry) {
                        return throwError(
                          new BadRequestException(STOCK_ENTRY_NOT_FOUND),
                        );
                      }
                      const mongoSerials: SerialHash =
                        this.getStockEntryMongoSerials(payload);
                      if (
                        stockEntry.stock_entry_type ===
                        STOCK_ENTRY_TYPE.MATERIAL_RECEIPT
                      ) {
                        if (mongoSerials && mongoSerials.length) {
                          stockEntry.status = undefined;
                          return this.createMongoSerials(
                            stockEntry,
                            mongoSerials,
                            req,
                            settings,
                            payload,
                            frappeResponse.data.data.name,
                          );
                        }
                      }

                      if (!mongoSerials) {
                        return this.stockEntrySyncService
                          .createStockEntry(
                            {
                              payload: stockEntry,
                              token: req.token,
                              type: CREATE_STOCK_ENTRY_JOB,
                              parent: stockEntry.uuid,
                              settings,
                            },
                            frappeResponse.data.data.name,
                          )
                          .pipe(
                            switchMap(() => {
                              return this.stockEntryService.updateOne(
                                { uuid: stockEntry.uuid },
                                {
                                  $set: {
                                    status:
                                      payload.stock_entry_type ===
                                      STOCK_ENTRY_TYPE.MATERIAL_TRANSFER
                                        ? STOCK_ENTRY_STATUS.in_transit
                                        : STOCK_ENTRY_STATUS.delivered,
                                  },
                                },
                              );
                            }),
                          );
                      }
                      return from(Object.keys(mongoSerials)).pipe(
                        mergeMap(key => {
                          return from(
                            this.serialNoService.updateOne(
                              {
                                serial_no: {
                                  $in: mongoSerials[key].serial_no,
                                },
                              },
                              {
                                $set: {
                                  queue_state: {
                                    stock_entry: {
                                      parent: stockEntry.uuid,
                                      warehouse:
                                        stockEntry.stock_entry_type ===
                                        'Material Transfer'
                                          ? 'Work In Progress - ETL'
                                          : mongoSerials[key].t_warehouse,
                                    },
                                  },
                                },
                              },
                            ),
                          );
                        }),
                        toArray(),
                        switchMap(() => {
                          stockEntry.items = stockEntry.items.map(item => {
                            if (
                              stockEntry.stock_entry_type ===
                              'Material Transfer'
                            ) {
                              return {
                                ...item,
                                t_warehouse: 'Work In Progress - ETL',
                              };
                            }
                            return item;
                          });
                          if (payload?.items?.length)
                            stockEntry.items = payload.items;
                          let stockEntryItems: any = JSON.stringify(
                            stockEntry.items,
                          );
                          return this.stockEntrySyncService
                            .createStockEntry(
                              {
                                payload: stockEntry,
                                token: req.token,
                                type: CREATE_STOCK_ENTRY_JOB,
                                parent: stockEntry.uuid,
                                settings,
                              },
                              frappeResponse.data.data.name,
                            )
                            .pipe(
                              switchMap(() => {
                                stockEntry.items = stockEntry?.items?.map(
                                  item => {
                                    if (item.qty < 0) item.qty = item.qty * -1;
                                    return item;
                                  },
                                );
                                try {
                                  stockEntryItems = JSON.parse(stockEntryItems);
                                } catch (error) {
                                  stockEntryItems = stockEntry.items;
                                }
                                return this.stockEntryService.updateOne(
                                  { uuid: stockEntry.uuid },
                                  {
                                    $set: {
                                      status:
                                        payload.stock_entry_type ===
                                        STOCK_ENTRY_TYPE.MATERIAL_TRANSFER
                                          ? STOCK_ENTRY_STATUS.in_transit
                                          : STOCK_ENTRY_STATUS.delivered,
                                      items: stockEntryItems,
                                    },
                                  },
                                );
                              }),
                            );
                        }),
                      );
                    }),
                  );
              }),
            );
          }),
        );
      }),
    );
  }

  createFrappeStockEntry(payload, settings, token) {
    if (payload.status !== STOCK_ENTRY_STATUS.draft && payload.uuid) {
      const stockEntryTemp: any = { ...payload };
      if (stockEntryTemp.stock_entry_type === 'Material Issue') {
        stockEntryTemp.naming_series = 'PCM-.YYYY.-';
      }
      if (stockEntryTemp.stock_entry_type === 'Material Receipt') {
        stockEntryTemp.naming_series = 'PAQ-.YYYY.-';
      }
      if (stockEntryTemp.stock_entry_type === 'Material Transfer') {
        stockEntryTemp.naming_series = 'TROUT-.YYYY.-';
        // stockEntryTemp.add_to_transit = 1;
      }
      if (stockEntryTemp.stock_entry_type === 'R&D Products') {
        stockEntryTemp.naming_series = 'RND-.YYYY.-';
      }
      stockEntryTemp.docstatus = 1;
      stockEntryTemp.set_posting_time = 1;
      stockEntryTemp.excel_territory = stockEntryTemp.territory;
      stockEntryTemp.items = stockEntryTemp.items.map(item => {
        delete item.name;
        let payloadToReturn = {
          ...item,
          serial_no: '',
          idx: undefined,
        };
        if (stockEntryTemp.stock_entry_type === 'Material Transfer') {
          payloadToReturn = {
            ...payloadToReturn,
            t_warehouse: 'Work In Progress - ETL',
          };
        }
        return payloadToReturn;
      });

      return this.http
        .post(
          settings.authServerURL + STOCK_ENTRY_API_ENDPOINT,
          stockEntryTemp,
          {
            headers: {
              [AUTHORIZATION]: BEARER_HEADER_VALUE_PREFIX + token.accessToken,
              [CONTENT_TYPE]: APP_JSON,
              [ACCEPT]: APPLICATION_JSON_CONTENT_TYPE,
            },
          },
        )
        .pipe(
          catchError(error => {
            if (error.response.status === 417) {
              return throwError(
                new BadRequestException(error.response.data.exception),
              );
            } else {
              return throwError(new BadRequestException(error));
            }
          }),
        );
    }
  }

  createMongoSerials(
    stockEntry: StockEntry,
    mongoSerials: any,
    req: any,
    settings: any,
    payload: any,
    invoiceName,
  ) {
    return of({}).pipe(
      switchMap(() => {
        return from(
          this.serialNoService.insertMany(mongoSerials, { ordered: false }),
        ).pipe(
          switchMap(response => {
            return this.stockEntrySyncService
              .createStockEntry(
                {
                  payload: stockEntry,
                  token: req.token,
                  type: CREATE_STOCK_ENTRY_JOB,
                  parent: stockEntry.uuid,
                  settings,
                },
                invoiceName,
              )
              .pipe(
                switchMap(() => {
                  return this.stockEntryService.updateOne(
                    { uuid: stockEntry.uuid },
                    {
                      $set: {
                        status:
                          payload.stock_entry_type ===
                          STOCK_ENTRY_TYPE.MATERIAL_TRANSFER
                            ? STOCK_ENTRY_STATUS.in_transit
                            : STOCK_ENTRY_STATUS.delivered,
                      },
                    },
                  );
                }),
              );
          }),
          catchError(() => {
            return this.stockEntryPolicies
              .validateStockEntryQueue(stockEntry)
              .pipe(
                switchMap(() => {
                  const serials = [];
                  mongoSerials.forEach(entry => serials.push(entry.serial_no));
                  this.serialNoService.deleteMany({
                    serial_no: { $in: serials },
                    'queue_state.stock_entry.parent': stockEntry.uuid,
                  });
                  return throwError(
                    new BadRequestException(
                      'Error occurred while adding serials to mongo, please try again.',
                    ),
                  );
                }),
              );
          }),
        );
      }),
    );
  }

  parseStockEntryPayload(payload: StockEntryDto) {
    return this.settingService.find().pipe(
      switchMap(serverSettings => {
        const date = new DateTime(serverSettings.timeZone).year;
        const $match = { stock_entry_type: payload.stock_entry_type };
        return this.stockEntryService.asyncAggregate([{ $match }]).pipe(
          map((result: any) => {
            const maxArray = [];
            for (const data of result) {
              const myArray = data.stock_id.split('-');
              if (myArray.length === 3) {
                maxArray.push(Number(myArray[2]));
              }
            }

            const incrementor = Number(Math.max(...maxArray)) + 1;
            switch (payload.stock_entry_type) {
              case STOCK_ENTRY_TYPE.MATERIAL_RECEIPT:
                payload.stock_id = `PAQ-${date}-${incrementor}`;
                return payload;

              case STOCK_ENTRY_TYPE.MATERIAL_ISSUE:
                payload.stock_id = `PCM-${date}-${incrementor}`;
                payload.items.filter(item => {
                  delete item.basic_rate;
                  return item;
                });
                return payload;

              case STOCK_ENTRY_TYPE.MATERIAL_TRANSFER:
                payload.stock_id = `TROUT-${date}-${incrementor}`;
                payload.items.filter(item => {
                  delete item.basic_rate;
                  return item;
                });
                return payload;

              case STOCK_ENTRY_TYPE.RnD_PRODUCTS:
                payload.stock_id = `RND-${date}-${incrementor}`;
                return payload;

              default:
                return payload;
            }
          }),
        );
      }),
    );
  }

  getStockEntryMongoSerials(stockEntry) {
    let mongoSerials: any = [];
    stockEntry.items.forEach((item: any) => {
      if (!item.has_serial_no) return;
      item.serial_no.forEach(serial_no => {
        if (stockEntry.stock_entry_type === STOCK_ENTRY_TYPE.MATERIAL_RECEIPT) {
          // if (!mongoSerials) mongoSerials = [];
          mongoSerials.push({
            serial_no,
            item_code: item.item_code,
            item_name: item.item_name,
            company: stockEntry.company,
            queue_state: {
              stock_entry: {
                parent: stockEntry.uuid,
                warehouse: item.t_warehouse,
              },
            },
          });
          return;
        }
        if (!mongoSerials) mongoSerials = {};
        if (mongoSerials[item.item_code]) {
          mongoSerials[item.item_code].serial_no.push(serial_no);
        } else {
          mongoSerials[item.item_code] = { serial_no: [serial_no] };
          mongoSerials[item.item_code].t_warehouse = item.t_warehouse;
        }
      });
    });
    return mongoSerials;
  }

  getStockBalance(payload: { item_code: string; warehouse: string }) {
    return this.stockLedgerService
      .asyncAggregate([
        {
          $match: {
            item_code: JSON.parse(decodeURIComponent(payload.item_code)),
            warehouse: payload.warehouse,
          },
        },
        {
          $group: { _id: null, sum: { $sum: '$actual_qty' } },
        },
        { $project: { sum: 1 } },
      ])
      .pipe(
        switchMap((stockCount: [{ sum: number }]) => {
          if (stockCount.length) {
            return of(stockCount.find(data => data).sum);
          }
          return of(0);
        }),
      );
  }

  async deleteDraft(uuid: string, req: any) {
    const stockEntry = await this.stockEntryService.findOne({
      where: { uuid },
    });
    if (!stockEntry) {
      throw new BadRequestException(STOCK_ENTRY_NOT_FOUND);
    }
    if (stockEntry.status !== STOCK_ENTRY_STATUS.draft) {
      throw new BadRequestException(
        `Stock Entry with status ${stockEntry.status}, cannot be deleted.`,
      );
    }
    await firstValueFrom(
      this.stockEntryPolicies.validateStockPermission(
        stockEntry.stock_entry_type,
        STOCK_OPERATION.delete,
        req,
      ),
    );
    await this.stockEntryService.deleteOne({ uuid });
    return true;
  }

  resetStockEntry(uuid: string, req) {
    let serverSettings: ServerSettings;
    return from(this.stockEntryService.findOne({ where: { uuid } })).pipe(
      switchMap(stockEntry => {
        return this.stockEntryPolicies
          .validateStockPermission(
            stockEntry.stock_entry_type,
            STOCK_OPERATION.delete,
            req,
          )
          .pipe(switchMap(() => of(stockEntry)));
      }),
      switchMap(stockEntry => {
        if (!stockEntry) {
          return throwError(() => new NotFoundException(STOCK_ENTRY_NOT_FOUND));
        }
        return forkJoin({
          validateSerials: this.stockEntryPolicies.validateSerials(stockEntry),
          stockEntry:
            this.stockEntryPolicies.validateStockEntryCancel(stockEntry),
          settings: this.settingService.find(),
        });
      }),
      switchMap(({ stockEntry, settings }) => {
        serverSettings = settings;
        return of(stockEntry);
      }),
      switchMap(stockEntry => {
        return forkJoin({
          serialReset: this.resetStockEntrySerial(stockEntry),
          serialHistoryReset: this.resetStockEntrySerialHistory(stockEntry),
        }).pipe(
          switchMap(() => {
            if (
              stockEntry.stock_entry_type !== STOCK_ENTRY_TYPE.MATERIAL_TRANSFER
            ) {
              return this.cancelStockEntryFromErp(
                stockEntry.stock_id,
                serverSettings,
                req,
              );
            } else {
              return of({});
            }
          }),
          switchMap(() => {
            if (
              stockEntry.stock_entry_type === STOCK_ENTRY_TYPE.MATERIAL_TRANSFER
            ) {
              if (stockEntry.status === STOCK_ENTRY_STATUS.delivered) {
                return this.cancelStockEntryFromErp(
                  stockEntry.trin_stock_id,
                  serverSettings,
                  req,
                ).pipe(
                  switchMap(() => {
                    return this.cancelStockEntryFromErp(
                      stockEntry.stock_id,
                      serverSettings,
                      req,
                    );
                  }),
                );
              } else if (stockEntry.status === STOCK_ENTRY_STATUS.in_transit) {
                return this.cancelStockEntryFromErp(
                  stockEntry.stock_id,
                  serverSettings,
                  req,
                );
              }
            } else {
              return of({});
            }
          }),
          switchMap(() => this.updateStockEntryReset(stockEntry)),
          switchMap(() => {
            if (
              stockEntry.stock_entry_type === STOCK_ENTRY_TYPE.MATERIAL_TRANSFER
            ) {
              return this.createTransferStockEntryLedger(
                stockEntry,
                req.token,
                serverSettings,
                's_warehouse',
              ).pipe(
                switchMap(() => {
                  stockEntry.stock_id = stockEntry.trin_stock_id;
                  return this.createTransferStockEntryLedger(
                    stockEntry,
                    req.token,
                    serverSettings,
                    't_warehouse',
                  );
                }),
              );
            }
            return this.createTransferStockEntryLedger(
              stockEntry,
              req.token,
              serverSettings,
            );
          }),
        );
      }),
      catchError(err => {
        return throwError(() => new BadRequestException(err));
      }),
    );
  }

  cancelStockEntryFromErp(
    docName: string,
    settings: ServerSettings,
    request: any,
  ) {
    const headers = {
      [AUTHORIZATION]: BEARER_HEADER_VALUE_PREFIX + request.token.accessToken,
    };
    return this.http
      .post(
        settings.authServerURL + FRAPPE_CLIENT_CANCEL,
        { doctype: 'Stock Entry', name: docName },
        { headers },
      )
      .pipe(
        catchError(err => {
          return throwError(() => new NotImplementedException(err));
        }),
      );
  }

  createTransferStockEntryLedger(
    payload: StockEntry,
    token,
    settings,
    warehouse_type?,
  ) {
    return from(payload.items).pipe(
      concatMap((item: StockEntryItem) => {
        return this.createStockLedgerPayload(
          item,
          token,
          settings,
          warehouse_type,
        ).pipe(
          switchMap((response: StockLedger) => {
            // eslint-disable-next-line no-console
            if (payload.stock_id === null || payload.stock_id === undefined) {
              return of({});
            }
            return from(
              this.stockLedgerService.deleteMany({
                voucher_no: payload.stock_id,
              }),
            );
          }),
        );
      }),
    );
  }

  createStockLedgerPayload(
    deliveryNoteItem: StockEntryItem,
    token: any,
    settings: ServerSettings,
    warehouse_type?: string,
  ) {
    return this.settingService.getFiscalYear(settings).pipe(
      switchMap(fiscalYear => {
        const date = new DateTime(settings.timeZone).toJSDate();
        const stockPayload = new StockLedger();
        stockPayload.name = uuidv4();
        stockPayload.modified = date;
        stockPayload.modified_by = token.email;
        stockPayload.item_code = deliveryNoteItem.item_code;
        stockPayload.posting_date = date;
        stockPayload.posting_time = date;
        stockPayload.voucher_type = STOCK_MATERIAL_TRANSFER;
        stockPayload.voucher_no = '';
        stockPayload.voucher_detail_no = '';
        stockPayload.outgoing_rate = 0;
        stockPayload.qty_after_transaction = stockPayload.actual_qty;

        if (warehouse_type === 't_warehouse') {
          stockPayload.actual_qty = -deliveryNoteItem.qty;
          stockPayload.incoming_rate = 0;
          stockPayload.valuation_rate = 0;
          stockPayload.warehouse = deliveryNoteItem.t_warehouse;
          stockPayload.batch_no = '';
        }
        if (warehouse_type === 's_warehouse') {
          stockPayload.actual_qty = deliveryNoteItem.qty;
          stockPayload.valuation_rate = 0;
          stockPayload.incoming_rate = 0;
          stockPayload.warehouse = deliveryNoteItem.s_warehouse;
          stockPayload.batch_no = '';
        }

        stockPayload.stock_value =
          stockPayload.qty_after_transaction * stockPayload.valuation_rate || 0;
        stockPayload.stock_value_difference =
          stockPayload.actual_qty * stockPayload.valuation_rate || 0;
        stockPayload.company = settings.defaultCompany;
        stockPayload.fiscal_year = fiscalYear;
        return of(stockPayload);
      }),
    );
  }

  saveDraft(payload: StockEntryDto, req: any) {
    if (payload.uuid) {
      payload.stock_id = payload.uuid;
      return from(
        this.stockEntryService.updateOne(
          { uuid: payload.uuid },
          { $set: payload },
        ),
      ).pipe(switchMap(() => of(payload)));
    }
    return this.settingService.find().pipe(
      switchMap(settings => {
        const stockEntry = this.setStockEntryDefaults(payload, req, settings);
        stockEntry.status = STOCK_ENTRY_STATUS.draft;
        stockEntry.stock_id = stockEntry.uuid;
        return from(this.stockEntryService.create(stockEntry)).pipe(
          switchMap(() => {
            return of(stockEntry);
          }),
          catchError(err => {
            return throwError(err);
          }),
        );
      }),
    );
  }

  setStockEntryDefaults(
    payload: StockEntryDto,
    clientHttpRequest: any,
    settings: ServerSettings,
  ): StockEntry {
    const stockEntry = new StockEntry();
    Object.assign(stockEntry, payload);
    delete stockEntry.names;
    stockEntry.uuid = uuidv4();
    stockEntry.doctype = STOCK_ENTRY;
    stockEntry.set_posting_time = 1;
    stockEntry.createdOn = payload.posting_date;
    stockEntry.createdAt = new DateTime(settings.timeZone).toJSDate();
    stockEntry.createdByEmail = clientHttpRequest.token.email;
    stockEntry.createdBy = clientHttpRequest.token.fullName;
    stockEntry.status = STOCK_ENTRY_STATUS.in_transit;
    stockEntry.isSynced = false;
    stockEntry.inQueue = true;
    stockEntry.docstatus = 1;
    return stockEntry;
  }

  StockEntryFromFile(file: File, req: any) {
    return from(this.getJsonData(file)).pipe(
      switchMap((data: StockEntryDto) => {
        if (!data) {
          return throwError(() => new BadRequestException(INVALID_FILE));
        }
        return this.createStockEntry(data, req);
      }),
    );
  }

  getJsonData(file: any) {
    return of(JSON.parse(file.buffer));
  }

  getStockEntryList(offset, limit, sort, filter_query, req) {
    return this.stockEntryService.list(
      offset,
      limit,
      sort,
      getUserPermissions(req),
      filter_query,
    );
  }

  getStockEntry(uuid: string, req: any) {
    return from(this.stockEntryService.findOne({ where: { uuid } })).pipe(
      switchMap(stockEntry => {
        if (!stockEntry) {
          return throwError(
            () => new BadRequestException(STOCK_ENTRY_NOT_FOUND),
          );
        }
        return this.stockEntryPolicies
          .validateStockPermission(
            stockEntry.stock_entry_type,
            STOCK_OPERATION.read,
            req,
          )
          .pipe(switchMap(() => of(stockEntry)));
      }),
    );
  }

  rejectStockEntry(uuid: string, req: any) {
    const settings$ = this.settingService.find();

    return from(this.stockEntryService.findOne({ where: { uuid } })).pipe(
      switchMap(stockEntry => {
        if (!stockEntry) {
          return throwError(
            () => new BadRequestException(STOCK_ENTRY_NOT_FOUND),
          );
        }

        return this.stockEntryPolicies
          .validateStockPermission(
            stockEntry.stock_entry_type,
            STOCK_OPERATION.delete,
            req,
          )
          .pipe(switchMap(() => of(stockEntry)));
      }),
      switchMap(stockEntry => {
        return forkJoin({
          validateSerialState:
            this.stockEntryPolicies.validateSerialState(stockEntry),
          validateJobState:
            this.stockEntryPolicies.validateStockEntryQueue(stockEntry),
        }).pipe(switchMap(() => of(stockEntry)));
      }),
      concatMap(stockEntry => {
        if (!stockEntry) {
          return throwError(
            () => new BadRequestException(STOCK_ENTRY_NOT_FOUND),
          );
        }

        return settings$.pipe(
          switchMap(settings => {
            return this.stockEntrySyncService
              .createFrappeStockEntry(stockEntry, settings, req.token)
              .pipe(
                switchMap(res => {
                  const voucherNumber = res.data.data.name;
                  stockEntry.trin_stock_id = stockEntry.stock_id;

                  return this.stockEntrySyncService
                    .createStockEntry(
                      {
                        payload: stockEntry,
                        token: req.token,
                        type: REJECT_STOCK_ENTRY_JOB,
                        parent: stockEntry.uuid,
                        settings,
                      },
                      voucherNumber,
                    )
                    .pipe(
                      switchMap(() => {
                        return this.stockEntryService
                          .updateOne(
                            { uuid },
                            { $set: { status: STOCK_ENTRY_STATUS.returned } },
                          )
                          .catch(() => {})
                          .then(() => stockEntry);
                      }),
                    );
                }),
                catchError(err => {
                  return throwError(() => new BadRequestException(err));
                }),
              );
          }),
        );
      }),
    );
  }

  acceptStockEntry(uuid: string, req: any) {
    return from(this.stockEntryService.findOne({ where: { uuid } })).pipe(
      switchMap((stockEntry: any) => {
        const settingsObservable = this.settingService.find();
        return this.stockEntryPolicies
          .validateStockPermission(
            stockEntry.stock_entry_type,
            STOCK_OPERATION.accept,
            req,
          )
          .pipe(
            switchMap(() => {
              return forkJoin({
                validateSerialState:
                  this.stockEntryPolicies.validateSerialState(stockEntry),
                validateJobState:
                  this.stockEntryPolicies.validateStockEntryQueue(stockEntry),
              });
            }),
            switchMap(() => {
              if (!stockEntry) {
                return throwError(
                  new BadRequestException(STOCK_ENTRY_NOT_FOUND),
                );
              }

              if (stockEntry.status === STOCK_ENTRY_STATUS.delivered) {
                return throwError(
                  () => new BadRequestException('Already Delivered'),
                );
              }
              this.stockEntryService.updateOne(
                { uuid },
                { $set: { status: STOCK_ENTRY_STATUS.delivered } },
              );
              const mongoSerials: SerialHash =
                this.getStockEntryMongoSerials(stockEntry);

              if (mongoSerials) {
                return from(Object.keys(mongoSerials)).pipe(
                  mergeMap(key => {
                    return from(
                      this.serialNoService.updateOne(
                        { serial_no: { $in: mongoSerials[key].serial_no } },
                        {
                          $set: {
                            warehouse: mongoSerials[key].t_warehouse,
                            queue_state: {
                              stock_entry: {
                                parent: stockEntry.uuid,
                                warehouse: mongoSerials[key].t_warehouse,
                              },
                            },
                          },
                        },
                      ),
                    );
                  }),
                  toArray(),
                  switchMap(() => {
                    return settingsObservable.pipe(
                      switchMap(settings => {
                        const stockEntryTemp: any = { ...stockEntry._doc };
                        stockEntryTemp.naming_series = 'TRIN-.YYYY.-';
                        const currentDateTime = this.getCurrentDateAndTime();
                        stockEntryTemp.posting_date = currentDateTime.date;
                        stockEntryTemp.posting_time = currentDateTime.time;
                        stockEntryTemp.items = stockEntryTemp.items.map(
                          item => {
                            return {
                              ...item,
                              serial_no: '',
                              s_warehouse: 'Work In Progress - ETL',
                            };
                          },
                        );
                        return this.http
                          .post(
                            settings.authServerURL + STOCK_ENTRY_API_ENDPOINT,
                            stockEntryTemp,
                            {
                              headers: {
                                [AUTHORIZATION]:
                                  BEARER_HEADER_VALUE_PREFIX +
                                  req.token.accessToken,
                                [CONTENT_TYPE]: APP_JSON,
                                [ACCEPT]: APPLICATION_JSON_CONTENT_TYPE,
                              },
                            },
                          )
                          .pipe(
                            switchMap(response => {
                              stockEntry.items = stockEntry.items.map(item => {
                                return {
                                  ...item,
                                  s_warehouse: 'Work In Progress - ETL',
                                };
                              });
                              stockEntry.trin_stock_id =
                                response.data.data.name;
                              return this.stockEntrySyncService.createStockEntry(
                                {
                                  payload: stockEntry,
                                  token: req.token,
                                  type: ACCEPT_STOCK_ENTRY_JOB,
                                  parent: stockEntry.uuid,
                                  settings,
                                },
                              );
                            }),
                            switchMap(() => of(stockEntry)),
                          );
                      }),
                      catchError(err => {
                        if (err.response.data.exception) {
                          return throwError(
                            () =>
                              new BadRequestException(
                                err.response.data.exception,
                              ),
                          );
                        }
                        return throwError(() => new BadRequestException(err));
                      }),
                    );
                  }),
                );
              } else {
                return settingsObservable.pipe(
                  switchMap(settings => {
                    let stockEntryTemp: any = { ...stockEntry };
                    stockEntryTemp = stockEntry._doc
                      ? stockEntry._doc
                      : stockEntry;
                    stockEntryTemp.naming_series = 'TRIN-.YYYY.-';
                    // const updatedTime = this.updateTime(
                    //   stockEntryTemp.posting_time,
                    // );
                    // stockEntryTemp.posting_time = updatedTime;
                    const currentDateTime = this.getCurrentDateAndTime();
                    stockEntryTemp.posting_date = currentDateTime.date;
                    stockEntryTemp.posting_time = currentDateTime.time;
                    stockEntryTemp.items = stockEntryTemp.items.map(item => {
                      return {
                        ...item,
                        serial_no: '',
                        s_warehouse: 'Work In Progress - ETL',
                      };
                    });
                    return this.http
                      .post(
                        settings.authServerURL + STOCK_ENTRY_API_ENDPOINT,
                        stockEntryTemp,
                        {
                          headers: {
                            [AUTHORIZATION]:
                              BEARER_HEADER_VALUE_PREFIX +
                              req.token.accessToken,
                            [CONTENT_TYPE]: APP_JSON,
                            [ACCEPT]: APPLICATION_JSON_CONTENT_TYPE,
                          },
                        },
                      )
                      .pipe(
                        switchMap(response => {
                          stockEntry.items = stockEntry.items.map(item => {
                            return {
                              ...item,
                              s_warehouse: 'Work In Progress - ETL',
                            };
                          });
                          stockEntry.trin_stock_id = response.data.data.name;
                          return this.stockEntrySyncService.createStockEntry({
                            payload: stockEntry,
                            token: req.token,
                            type: ACCEPT_STOCK_ENTRY_JOB,
                            parent: stockEntry.uuid,
                            settings,
                          });
                        }),
                        switchMap(() => of(stockEntry)),
                      );
                  }),
                  catchError(err => {
                    if (err?.response?.data?.exception) {
                      return throwError(
                        () =>
                          new BadRequestException(err.response.data.exception),
                      );
                    }
                    return throwError(() => new BadRequestException(err));
                  }),
                );
              }
            }),
            catchError(err => {
              if (err?.response?.data?.exception) {
                return throwError(
                  () => new BadRequestException(err.response.data.exception),
                );
              }
              return throwError(() => new BadRequestException(err));
            }),
          );
      }),
    );
  }

  getCurrentDateAndTime() {
    // Get the current date and time
    const currentDate = moment().utcOffset('+06:00');

    return {
      date: `${currentDate.format('YYYY-MM-DD')}`,
      time: `${currentDate.format('HH:mm:ss')}`,
    };
  }

  updateTime(time) {
    const givenTime = time;

    // Split the time into hours, minutes, and seconds
    const [hours, minutes, seconds] = givenTime.split(':').map(Number);

    // Add 20 seconds
    const newSeconds = seconds + 10;

    // Check if the seconds exceed 59
    if (newSeconds > 59) {
      const newMinutes = minutes + 1;
      const formattedSeconds = '00';
      const formattedMinutes = newMinutes.toString().padStart(2, '0');

      // Check if the minutes exceed 59
      if (newMinutes > 59) {
        const newHours = hours + 1;
        // Reset minutes and seconds to 0
        const formattedMinutes = '00';
        const formattedSeconds = '00';
        const formattedHours = newHours.toString().padStart(2, '0');
        return `${formattedHours}:${formattedMinutes}:${formattedSeconds}`;
      } else {
        // If minutes don't exceed 59, format the new time
        return `${hours}:${formattedMinutes}:${formattedSeconds}`;
      }
    } else {
      // If seconds don't exceed 59, format the new time
      const formattedSeconds = newSeconds.toString().padStart(2, '0');
      return `${hours}:${minutes}:${formattedSeconds}`;
    }
  }

  updateStockEntryReset(stockEntry: StockEntry) {
    return from(
      this.stockEntryService.updateOne(
        { uuid: stockEntry.uuid },
        {
          $set: {
            status: STOCK_ENTRY_STATUS.reseted,
          },
        },
      ),
    );
  }

  resetStockEntrySerial(stockEntry: StockEntry) {
    switch (stockEntry.stock_entry_type) {
      case STOCK_ENTRY_TYPE.MATERIAL_RECEIPT:
        return from(
          this.serialNoService.deleteMany({
            purchase_invoice_name: stockEntry.uuid,
          }),
        );

      case STOCK_ENTRY_TYPE.MATERIAL_ISSUE:
        return this.resetMaterialIssueSerials(stockEntry);

      case STOCK_ENTRY_TYPE.RnD_PRODUCTS:
        return this.resetMaterialIssueSerials(stockEntry);

      case STOCK_ENTRY_TYPE.MATERIAL_TRANSFER:
        return this.resetMaterialTransfer(stockEntry);

      default:
        return throwError(
          () => new BadRequestException(INVALID_STOCK_ENTRY_TYPE),
        );
    }
  }

  resetMaterialTransfer(stockEntry: StockEntry) {
    return from(stockEntry.items).pipe(
      concatMap(item => {
        if (!item.has_serial_no) {
          return of(true);
        }
        return from(
          this.serialNoService.updateMany(
            {
              serial_no: { $in: item.serial_no },
            },
            {
              $set: { warehouse: item.s_warehouse },
            },
          ),
        );
      }),
    );
  }

  resetMaterialIssueSerials(stockEntry: StockEntry) {
    return from(stockEntry.items).pipe(
      concatMap(item => {
        if (!item.has_serial_no) {
          return of(true);
        }
        return from(
          this.serialNoService.updateMany(
            {
              serial_no: { $in: item.serial_no },
            },
            {
              $set: {
                warehouse: item.s_warehouse,
              },
              $unset: {
                sales_document_type: null,
                sales_document_no: null,
                sales_invoice_name: null,
                'warranty.salesWarrantyDate': null,
                'warranty.soldOn': null,
              },
            },
          ),
        );
      }),
    );
  }

  resetStockEntrySerialHistory(stockEntry: StockEntry) {
    switch (stockEntry.stock_entry_type) {
      case STOCK_ENTRY_TYPE.MATERIAL_RECEIPT:
        return from(
          this.serialHistoryService.deleteMany({
            parent_document: stockEntry.uuid,
          }),
        );

      case STOCK_ENTRY_TYPE.MATERIAL_ISSUE:
        return from(
          this.serialHistoryService.deleteMany({
            parent_document: stockEntry.uuid,
          }),
        );

      case STOCK_ENTRY_TYPE.RnD_PRODUCTS:
        return from(
          this.serialHistoryService.deleteMany({
            parent_document: stockEntry.uuid,
          }),
        );

      case STOCK_ENTRY_TYPE.MATERIAL_TRANSFER:
        return from(
          this.serialHistoryService.deleteMany({
            parent_document: stockEntry.uuid,
          }),
        );

      default:
        return throwError(
          () => new BadRequestException(INVALID_STOCK_ENTRY_TYPE),
        );
    }
  }

  getStockEntryDeliveredSerials(offset, limit, search, uuid, req) {
    return from(this.stockEntryService.findOne({ where: { uuid } })).pipe(
      switchMap(stockEntry => {
        if (!stockEntry) {
          return throwError(() => new NotFoundException(STOCK_ENTRY_NOT_FOUND));
        }
        const serials = [];
        const regex = new RegExp(search, 'i');
        stockEntry.items.forEach(item => {
          if (item.has_serial_no) {
            if (search && search !== '') {
              item.serial_no.forEach(eachSerial =>
                regex.test(eachSerial) ? serials.push(eachSerial) : null,
              );
            } else {
              serials.push(...item.serial_no);
            }
          }
        });

        const serialNoQuery: any = { serial_no: { $in: serials } };

        return forkJoin({
          docs: this.serialNoService.aggregateList(
            offset,
            limit,
            serialNoQuery,
          ),
          length: of(serials.length),
          offset: of(offset),
        });
      }),
    );
  }

  syncStockEntryDocument(req: any, stockPrintBody: any) {
    let url: string = '';
    return this.settingService.find().pipe(
      switchMap(setting => {
        if (!setting.authServerURL) {
          return throwError(() => new NotImplementedException());
        }
        url = `${setting.authServerURL}${POST_STOCK_PRINT_ENDPOINT}`;
        return this.http.get(`${url}/${stockPrintBody.uuid}`, {
          headers: {
            authorization: req.body.headers.Authorization,
            Accept: APPLICATION_JSON_CONTENT_TYPE,
          },
        });
      }),
      map(res => res.data),
      switchMap(() => {
        return this.http.put(`${url}/${stockPrintBody.uuid}`, stockPrintBody, {
          headers: {
            authorization: req.body.headers.Authorization,
            Accept: APPLICATION_JSON_CONTENT_TYPE,
          },
        });
      }),
      map(res => res.data),
      catchError(err => {
        if (err.response.status === 404) {
          return this.http.post(url, stockPrintBody, {
            headers: {
              authorization: req.body.headers.Authorization,
              Accept: APPLICATION_JSON_CONTENT_TYPE,
            },
          });
        }
        return throwError(
          () => new BadRequestException(err.response.statusText),
        );
      }),
      map(res => res.data),
    );
  }
}

export interface SerialHash {
  [key: string]: { serial_no: string[]; t_warehouse: string };
}
