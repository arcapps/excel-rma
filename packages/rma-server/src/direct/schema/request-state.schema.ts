import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type RequestStateDocument = HydratedDocument<RequestState>;

@Schema({ collection: 'request_state' })
export class RequestState {
  @Prop()
  uuid: string;

  @Prop()
  redirect: string;

  @Prop()
  creation: Date;

  @Prop()
  email: string;

  @Prop()
  syncDocType: string;
}

export const RequestStateSchema = SchemaFactory.createForClass(RequestState);
