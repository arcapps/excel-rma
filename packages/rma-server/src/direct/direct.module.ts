import { Module } from '@nestjs/common';
// import { TypeOrmModule } from '@nestjs/typeorm';
import { DirectController } from './controllers/direct/direct.controller';
import { DirectService } from './aggregates/direct/direct.service';
// import { DEFAULT } from '../constants/typeorm.connection';
// import { RequestState } from './entities/request-state/request-state.entity';
import { RequestStateService } from './entities/request-state/request-state.service';
import { MongooseModule } from '@nestjs/mongoose';
import { RequestStateSchema } from './schema/request-state.schema';

@Module({
  imports: [
    // TypeOrmModule.forFeature([RequestState], DEFAULT),
    MongooseModule.forFeature([
      { name: 'RequestState', schema: RequestStateSchema },
    ]),
  ],
  controllers: [DirectController],
  providers: [DirectService, RequestStateService],
  exports: [DirectService, RequestStateService],
})
export class DirectModule {}
