import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { firstValueFrom } from 'rxjs';
import { WarrantyClaimAggregateService } from '../../aggregates/warranty-claim-aggregate/warranty-claim-aggregate.service';
import { AddWarrantyClaimCommand } from './add-warranty-claim.command';

@CommandHandler(AddWarrantyClaimCommand)
export class AddWarrantyClaimCommandHandler
  implements ICommandHandler<AddWarrantyClaimCommand>
{
  constructor(
    private publisher: EventPublisher,
    private manager: WarrantyClaimAggregateService,
  ) {}
  async execute(command: AddWarrantyClaimCommand) {
    const { warrantyclaimPayload: warrantyclaimPayload, clientHttpRequest } =
      command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await firstValueFrom(
      aggregate.createClaim(warrantyclaimPayload, clientHttpRequest),
    );
    aggregate.commit();
  }
}
