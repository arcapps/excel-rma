import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { firstValueFrom } from 'rxjs';
import { WarrantyClaimAggregateService } from '../../aggregates/warranty-claim-aggregate/warranty-claim-aggregate.service';
import { CreateBulkClaimsCommand } from './create-bulk-claims.command';

@CommandHandler(CreateBulkClaimsCommand)
export class CreateBulkClaimsHandler
  implements ICommandHandler<CreateBulkClaimsCommand>
{
  constructor(
    private publisher: EventPublisher,
    private manager: WarrantyClaimAggregateService,
  ) {}
  async execute(command: CreateBulkClaimsCommand) {
    const { claimsPayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await firstValueFrom(
      aggregate.addBulkClaims(claimsPayload, clientHttpRequest),
    );
    aggregate.commit();
  }
}
