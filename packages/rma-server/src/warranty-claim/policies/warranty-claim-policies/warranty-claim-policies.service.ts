import { HttpService } from '@nestjs/axios';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
  NotImplementedException,
} from '@nestjs/common';
import { DateTime } from 'luxon';
import { forkJoin, from, of, throwError } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import {
  CANCEL_SERVICE_INVOICES,
  CANCEL_STOCK_ENTRIES,
  CLAIM_CANNOT_BE_CANCELLED,
  REVERT_STATUS_HISTORY,
  VERDICT,
  WARRANTY_STATUS,
} from '../../../constants/app-strings';
import {
  INVALID_CUSTOMER,
  INVALID_SERIAL_NO,
  INVALID_WARRANTY_CLAIM_AT_POSITION,
  SUPPLIER_NOT_FOUND,
} from '../../../constants/messages';
import { FRAPPE_API_GET_CUSTOMER_ENDPOINT } from '../../../constants/routes';
import { SerialNo } from '../../../serial-no/schema/serial-no.schema';
import { SerialNoService } from '../../../serial-no/entity/serial-no/serial-no.service';
import { ServiceInvoiceService } from '../../../service-invoice/entity/service-invoice/service-invoice.service';
import { SupplierService } from '../../../supplier/entity/supplier/supplier.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { WarrantyClaimDto } from '../../../warranty-claim/entity/warranty-claim/warranty-claim-dto';
import {
  BulkWarrantyClaim,
  BulkWarrantyClaimInterface,
} from '../../entity/warranty-claim/create-bulk-warranty-claim.interface';
import { WarrantyClaimService } from '../../entity/warranty-claim/warranty-claim.service';

@Injectable()
export class WarrantyClaimPoliciesService {
  constructor(
    private readonly supplierService: SupplierService,
    private readonly invoiceService: ServiceInvoiceService,
    private readonly clientToken: ClientTokenManagerService,
    private readonly serialNoService: SerialNoService,
    private readonly warrantyService: WarrantyClaimService,
    private readonly settings: SettingsService,
    private readonly http: HttpService,
  ) {}

  validateBulkWarrantyClaim(warrantyClaim: BulkWarrantyClaimInterface) {
    return this.validateWarrantySupplier(warrantyClaim.supplier).pipe(
      switchMap(() => {
        // next validate items inside claims and validate keys for claims data
        return this.validateWarrantyClaims(warrantyClaim.claims);
      }),
    );
  }

  validateWarrantySupplier(supplierName) {
    return from(
      this.supplierService.findOne({ where: { name: supplierName } }),
    ).pipe(
      switchMap(supplier => {
        if (!supplier) {
          return throwError(() => new NotFoundException(SUPPLIER_NOT_FOUND));
        }
        return of(true);
      }),
    );
  }

  validateWarrantyClaims(claims: BulkWarrantyClaim[]) {
    let i = 0;
    claims.forEach(claim => {
      if (
        !claim.supplier ||
        !claim.serial_no ||
        !claim.item_code ||
        !claim.itemWarrantyDate ||
        !claim.company
      ) {
        return throwError(
          new BadRequestException(INVALID_WARRANTY_CLAIM_AT_POSITION + i),
        );
      }
      i++;
    });
    return of(true);
  }

  validateWarrantyCustomer(customer_name: string) {
    return forkJoin({
      headers: this.clientToken.getServiceAccountApiHeaders(),
      settings: this.settings.find(),
    }).pipe(
      switchMap(({ headers, settings }) => {
        if (!settings || !settings.authServerURL)
          return throwError(() => new NotImplementedException());
        const url = `${settings.authServerURL}${FRAPPE_API_GET_CUSTOMER_ENDPOINT}/${customer_name}`;
        return this.http.get(url, { headers }).pipe(
          switchMap(customer => {
            if (!customer) {
              return throwError(() => new NotFoundException(INVALID_CUSTOMER));
            }
            return of(true);
          }),
        );
      }),
    );
  }

  validateWarrantySerialNo(claimsPayload: WarrantyClaimDto) {
    return from(
      this.serialNoService.findOne({
        where: {
          serial_no: claimsPayload.serial_no,
          item_code: claimsPayload.item_code,
        },
      }),
    ).pipe(
      switchMap(serialNo => {
        if (!serialNo) {
          return throwError(() => new BadRequestException(INVALID_SERIAL_NO));
        }
        return this.validateWarrantyDate(serialNo, claimsPayload);
      }),
    );
  }

  validateWarrantyDate(serial: SerialNo, claimsPayload: WarrantyClaimDto) {
    return this.settings.find().pipe(
      switchMap(settings => {
        if (!settings) {
          settings.timeZone;
          return throwError(() => new NotImplementedException());
        }
        claimsPayload.warranty_end_date = serial.warranty.salesWarrantyDate;
        const warrantyEndDate = DateTime.fromISO(
          serial.warranty.salesWarrantyDate,
        ).setZone(settings.timeZone);
        const warrantyClaimDate = DateTime.fromISO(
          claimsPayload.warranty_claim_date,
        ).setZone(settings.timeZone);
        if (warrantyEndDate > warrantyClaimDate) {
          claimsPayload.warranty_status = WARRANTY_STATUS.VALID;
          return of(claimsPayload);
        }
        claimsPayload.warranty_status = WARRANTY_STATUS.EXPIRED;
        return of(claimsPayload);
      }),
    );
  }

  validateCancelClaim(uuid: string) {
    return from(
      this.warrantyService.findOne({
        where: {
          uuid,
          progress_state: { $exists: true },
        },
      }),
    ).pipe(
      switchMap(claim => {
        if (claim) {
          if (claim?.progress_state?.length) {
            return throwError(
              new BadRequestException(
                `${CLAIM_CANNOT_BE_CANCELLED} ${CANCEL_STOCK_ENTRIES}`,
              ),
            );
          }
          return of(true);
        }
        if (
          claim.status_history[claim.status_history?.length - 1].verdict ===
          VERDICT.RECEIVED_FROM_CUSTOMER
        ) {
          return of(true);
        }
        return throwError(
          new BadRequestException(
            `${CLAIM_CANNOT_BE_CANCELLED} ${REVERT_STATUS_HISTORY}`,
          ),
        );
      }),
    );
  }

  validateServiceInvoice(warrantyClaimUuid: string) {
    return from(
      this.invoiceService.findOne({
        where: { warrantyClaimUuid, docstatus: 1 },
      }),
    ).pipe(
      switchMap(serviceInvoice => {
        if (serviceInvoice) {
          return throwError(
            new BadRequestException(
              `${CLAIM_CANNOT_BE_CANCELLED} ${CANCEL_SERVICE_INVOICES}`,
            ),
          );
        }
        return of(true);
      }),
    );
  }
}
