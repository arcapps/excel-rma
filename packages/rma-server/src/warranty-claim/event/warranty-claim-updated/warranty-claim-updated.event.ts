import { IEvent } from '@nestjs/cqrs';
import { WarrantyClaim } from '../../schema/warranty-claim.schema';

export class WarrantyClaimUpdatedEvent implements IEvent {
  constructor(public updatePayload: WarrantyClaim) {}
}
