import { IEvent } from '@nestjs/cqrs';
import { WarrantyClaim } from '../../schema/warranty-claim.schema';

export class WarrantyClaimRemovedEvent implements IEvent {
  constructor(public warrantyclaim: WarrantyClaim) {}
}
