import { IQuery } from '@nestjs/cqrs';

export class RetrieveWarrantyClaimBySINVQuery implements IQuery {
  constructor(public readonly uuid: string, public readonly req: any) {}
}
