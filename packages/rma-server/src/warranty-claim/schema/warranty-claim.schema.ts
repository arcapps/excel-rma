import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { WarrantyClaimStatus } from '../../constants/warranty-claim-status.enum';

export type WarrantyClaimDocument = HydratedDocument<WarrantyClaim>;

@Schema({ collection: 'warranty_claim' })
export class WarrantyClaim {
  @Prop()
  uuid: string;

  @Prop()
  modifiedOn: Date;

  @Prop()
  createdOn: Date;

  @Prop()
  serialNo: string;

  // @Prop()
  // _id: mongoose.Schema.Types.ObjectId;

  @Prop()
  claim_no: string;

  @Prop()
  claim_type: string;

  @Prop()
  received_date: Date;

  @Prop()
  deliver_date: Date;

  @Prop()
  customer_third_party: string;

  @Prop()
  item_code: string;

  @Prop()
  claimed_serial: string;

  @Prop()
  invoice_no: string;

  @Prop()
  service_charge: string;

  @Prop()
  claim_status: string;

  @Prop()
  warranty_status: string;

  @Prop()
  receiving_branch: string;

  @Prop()
  delivery_branch: string;

  @Prop()
  received_by: string;

  @Prop()
  delivered_by: string;

  @Prop()
  customer: string;

  @Prop()
  customer_code: string;

  @Prop()
  customer_contact: string;

  @Prop()
  customer_address: string;

  @Prop()
  serial_no: string;

  @Prop()
  third_party_name: string;

  @Prop()
  third_party_contact: string;

  @Prop()
  third_party_address: string;

  @Prop()
  warranty_claim_date: Date;

  @Prop()
  warranty_end_date: Date;

  @Prop()
  received_on: string;

  @Prop()
  delivery_date: Date;

  @Prop()
  item_name: string;

  @Prop()
  product_brand: string;

  @Prop()
  problem: string;

  @Prop()
  problem_details: string;

  @Prop()
  remarks: string;

  @Prop()
  created_by_email: string;

  @Prop()
  created_by: string;

  @Prop()
  status_history: StatusHistory[];

  @Prop()
  progress_state: any[];

  @Prop()
  replace_serial: string;

  @Prop()
  replace_product: string;

  @Prop()
  replace_warehouse: string;

  @Prop()
  damaged_serial: string;

  @Prop()
  damage_warehouse: string;

  @Prop()
  damage_product: string;

  @Prop()
  billed_amount: number;

  @Prop()
  outstanding_amount: number;

  @Prop()
  category: string;

  @Prop()
  set: string;

  @Prop()
  bulk_products: WarrantyBulkProducts[];

  @Prop()
  parent: string;

  @Prop()
  service_vouchers: string[];

  @Prop()
  service_items: string[];

  @Prop()
  posting_time: string;

  @Prop()
  subclaim_state: string;

  @Prop()
  attach_link: string;

  @Prop({
    type: String,
    default: '',
  })
  receiving_count: string;

  @Prop({
    type: String,
    enum: WarrantyClaimStatus,
    default: WarrantyClaimStatus.OPEN,
  })
  current_status: string;
}

export const WarrantyClaimSchema = SchemaFactory.createForClass(WarrantyClaim);

export class WarrantyBulkProducts {
  serial_no: string;
  claim_type: string;
  invoice_no: string;
  warranty_end_date: string;
  item_name: string;
  product_brand: string;
  problem: string;
  problem_details: string;
  remarks: string;
  item_code: string;
}

export class StatusHistory {
  posting_date: Date;
  time: Date;
  status_from: string;
  transfer_branch: string;
  verdict: string;
  description: string;
  delivery_status: string;
  status: string;
  created_by_email: string;
  created_by: string;
}
