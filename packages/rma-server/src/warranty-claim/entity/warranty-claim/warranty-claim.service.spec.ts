import { Test, TestingModule } from '@nestjs/testing';
import { getConnectionToken, getModelToken } from '@nestjs/mongoose';
import { WarrantyClaimService } from './warranty-claim.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { Connection } from 'mongoose';

describe('WarrantyClaimService', () => {
  let service: WarrantyClaimService;
  let connection: Connection;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        WarrantyClaimService,
        {
          provide: getModelToken('WarrantyClaim'),
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: getConnectionToken(),
          useValue: {
            createConnection: jest.fn(),
            close: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<WarrantyClaimService>(WarrantyClaimService);
    connection = module.get<Connection>(getConnectionToken());
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
    expect(connection).toBeDefined();
  });
});
