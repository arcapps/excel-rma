import { Injectable } from '@nestjs/common';
// import { InjectRepository } from '@nestjs/typeorm';
import { DateTime } from 'luxon';
import { firstValueFrom, Observable } from 'rxjs';
// import { MongoFindOneOptions } from 'typeorm/find-options/mongodb/MongoFindOneOptions';
import { v4 as uuidv4 } from 'uuid';
import {
  CATEGORY,
  DATE_TYPE,
  DEFAULT_NAMING_SERIES,
  PARSE_REGEX,
} from '../../../constants/app-strings';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
// import { WarrantyClaim } from './warranty-claim.entity';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model, PipelineStage } from 'mongoose';
import {
  WarrantyClaim,
  WarrantyClaimDocument,
} from '../../schema/warranty-claim.schema';
@Injectable()
export class WarrantyClaimService {
  constructor(
    @InjectModel('WarrantyClaim')
    private warrantyClaimModel: Model<WarrantyClaimDocument>,
    // @InjectRepository(WarrantyClaim)
    // private readonly warrantyClaimRepository: MongoRepository<WarrantyClaim>,
    private readonly settings: SettingsService,
    @InjectConnection() private readonly connection: Connection,
  ) {}

  async find(query?) {
    const res = await this.warrantyClaimModel.find(query);
    return res;
  }

  async create(warrantyClaim: WarrantyClaim) {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    //  @ts-ignore
    if (warrantyClaim._doc) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      //  @ts-ignore
      warrantyClaim = warrantyClaim._doc;
    }

    if (warrantyClaim.set === 'Part' && warrantyClaim.category === 'Bulk') {
      warrantyClaim.uuid = uuidv4();
    }

    if (!['Bulk', 'Part'].includes(warrantyClaim.set)) {
      warrantyClaim.claim_no = await this.generateNamingSeries(
        warrantyClaim.set,
      );
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      //  @ts-ignore
      delete warrantyClaim._id;

      const res = await new this.warrantyClaimModel(warrantyClaim).save();
      return res;
    }
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    //  @ts-ignore
    delete warrantyClaim._id;
    warrantyClaim.claim_no = uuidv4();

    //
    if (warrantyClaim.set === 'Bulk') {
      warrantyClaim.serial_no = '';
    }

    // const res = await this.warrantyClaimModel.create(warrantyClaim);
    const res = await new this.warrantyClaimModel(warrantyClaim).save();
    return res;
  }

  async findOne(options) {
    // await this.warrantyClaimModel.findOne([
    //   {
    //     $match: {},
    //   },
    // ]);
    return await this.warrantyClaimModel.findOne(options.where);
  }

  // find warranty claims based on serial no
  async findManyBySerial(options) {
    return await this.warrantyClaimModel
      .find(options.where)
      .sort({ createdOn: -1 });
  }

  async findOnePipeline(pipeline: any, options?) {
    const items = await this.warrantyClaimModel.aggregate(pipeline, options);
    return items[0];
  }

  async list(
    skip,
    take,
    sort,
    filter_query?,
    territory?,
    multiple_claim?,
    clientHttpRequest?,
  ) {
    // prepare sort query
    let sortQuery: any;
    let dateQuery: any = {};
    try {
      sortQuery = JSON.parse(sort);
    } catch (error) {
      sortQuery = { createdOn: -1 };
    }
    sortQuery =
      Object.keys(sortQuery).length === 0 ? { createdOn: -1 } : sortQuery;

    // prepare date query
    if (filter_query?.from_date && filter_query?.to_date) {
      const fromDate = new Date(filter_query.from_date);
      const toDate = new Date(filter_query.to_date);

      if (filter_query.date_type === DATE_TYPE.RECEIVED_DATE) {
        dateQuery = { createdOn: { $gte: fromDate, $lte: toDate } };
      } else if (filter_query.date_type === DATE_TYPE.DELIVERED_DATE) {
        dateQuery = {
          delivery_date: {
            $gte: fromDate.toISOString().split('T')[0],
            $lte: toDate.toISOString().split('T')[0],
          },
        };
      }
    }

    // Filter unnecessary fields
    delete filter_query?.date_type;

    // territory filter query
    const territories = Array.isArray(clientHttpRequest.token.territory)
      ? clientHttpRequest.token.territory
      : [clientHttpRequest.token.territory];

    const orConditions = [
      { 'status_history.transfer_branch': { $in: territories } },
      { 'status_history.status_from': { $in: territories } },
    ];

    // verdict filter
    const verdictQuery = {
      status_history: {
        $elemMatch: {},
      },
    };

    if (filter_query?.verdict?.length) {
      // eslint-disable-next-line dot-notation
      verdictQuery.status_history.$elemMatch['verdict'] = filter_query.verdict;
      delete filter_query.verdict;
    }

    if (filter_query?.created_by) {
      // eslint-disable-next-line dot-notation
      verdictQuery.status_history.$elemMatch['created'] = {
        $regex: PARSE_REGEX(filter_query.created_by),
        $options: 'i',
      };
      delete filter_query?.created_by;
    }

    // prepare sort queries
    let sortQry: Record<string, number> = { createdOn: -1 };
    if (Object.values(sortQuery || {}).length) {
      sortQry = Object.keys(sortQuery).reduce(
        (acc: Record<string, number>, key: string) => {
          if (typeof sortQuery[key] == 'number') acc[key] = sortQuery[key];
          else if (
            typeof sortQuery[key] == 'string' &&
            sortQuery[key] === 'desc'
          )
            acc[key] = -1;
          else acc[key] = 1;
          return acc;
        },
        {},
      );
    }

    // prepared and query conditions
    const andConditions = [
      { $or: orConditions },
      filter_query ? this.getFilterQuery(filter_query) : {},
      verdictQuery,
      dateQuery,
    ];

    // check territory
    if (territory) {
      andConditions.push({ set: { $in: territory.set } });
    }

    // prepare where condition clause
    const where: { $and: any } = { $and: andConditions };

    // if query has voucher no then return result from this portion
    if (filter_query?.voucher_no) {
      const db = this.connection.db;

      const res = await db
        .collection('stock_entry')
        .findOne({ stock_id: filter_query?.voucher_no });

      const wrntyClaim = await this.warrantyClaimModel
        .findOne({ uuid: res?.warrantyClaimUuid })
        .exec();

      return {
        docs: wrntyClaim ? [wrntyClaim] : [],
        length: 1,
        offset: skip,
      };
    }

    // if query has is multiple true, at that time return this result
    if (multiple_claim === 1)
      return await this.getMultipleFilter(where, sortQry, skip, take);

    // general warranty claim list
    const aggregationPipeline = [
      {
        $match: where,
      },

      {
        $sort: sortQry,
      },
      {
        $skip: skip,
      },
      {
        $limit: take,
      },
    ] as PipelineStage[];

    // Execute the aggregation
    const results = await this.warrantyClaimModel
      .aggregate(aggregationPipeline)
      .exec();

    // // Optimize query by combining sorting and limit in the database query
    // const results = await this.warrantyClaimModel
    //   .find(where)
    //   .sort(sortQuery)
    //   .skip(skip)
    //   .limit(take)
    //   .exec();

    // Count total documents matching the query without applying the limit
    const totalDocsResult = await this.warrantyClaimModel
      .aggregate([
        {
          $match: where,
        },
        { $count: 'totalDocs' },
      ])
      .exec();
    const totalDocs =
      totalDocsResult.length > 0 ? totalDocsResult[0].totalDocs : 0;

    // Sorting can be improved by combining operations
    // const sortedData = results.sort(this.customSort);

    return {
      docs: results,
      length: totalDocs,
      offset: skip,
    };
  }

  async getMultipleFilter(where, sortQuery, skip, take) {
    // aggregation pipeline for multiple claim items
    const aggregationPipeline = [
      { $match: where },
      {
        $group: {
          _id: '$serial_no',
          count: { $sum: 1 },
          records: { $push: '$$ROOT' },
        },
      },
      {
        $match: {
          $and: [{ _id: { $ne: '' } }, { _id: { $ne: null } }],
          count: { $gt: 1 },
        },
      },
      { $project: { _id: 0, count: 0 } },
      { $unwind: { path: '$records' } },
      { $replaceRoot: { newRoot: '$records' } },
      { $sort: sortQuery },
      { $skip: skip },
      { $limit: take },
    ] as PipelineStage[];

    // Pipeline for counting total documents matching the filter
    const countPipeline = [
      { $match: where },
      {
        $group: {
          _id: '$serial_no',
          count: { $sum: 1 },
        },
      },
      {
        $match: {
          $and: [{ _id: { $ne: '' } }, { _id: { $ne: null } }],
          count: { $gt: 1 },
        },
      },
      {
        $group: {
          _id: null,
          totalCount: { $sum: '$count' },
        },
      },
    ];

    const results = await this.warrantyClaimModel
      .aggregate(aggregationPipeline)
      .exec();

    // console.log(results);

    const totalDocsResult = await this.warrantyClaimModel
      .aggregate(countPipeline)
      .exec();

    // const totalDocs = await this.warrantyClaimModel.find().countDocuments();

    return {
      docs: results,
      length: totalDocsResult?.[0]?.totalCount,
      // length: totalDocs,
      offset: skip,
    };
  }

  // async list(skip, take, sort, filter_query?, territory?, clientHttpRequest?) {
  //   let sortQuery: any;
  //   let dateQuery = {};

  //   try {
  //     sortQuery = JSON.parse(sort);
  //   } catch (error) {
  //     sortQuery = {
  //       modifiedOn: 'desc',
  //     };
  //   }
  //   sortQuery =
  //     Object.keys(sortQuery).length === 0 ? { modifiedOn: 'desc' } : sortQuery;

  //   if (filter_query?.from_date && filter_query?.to_date) {
  //     if (filter_query.date_type === DATE_TYPE.RECEIVED_DATE) {
  //       dateQuery = {
  //         createdOn: {
  //           $gte: new Date(filter_query.from_date),
  //           $lte: new Date(filter_query.to_date),
  //         },
  //       };
  //     } else if (filter_query.date_type === DATE_TYPE.DELIVERED_DATE) {
  //       dateQuery = {
  //         delivery_date: {
  //           $gte: new Date(filter_query.from_date).toISOString().split('T')[0],
  //           $lte: new Date(filter_query.to_date).toISOString().split('T')[0],
  //         },
  //       };
  //     }
  //   }

  //   for (const key of Object.keys(sortQuery)) {
  //     sortQuery[key] = sortQuery[key].toUpperCase();
  //     if (!sortQuery[key]) {
  //       delete sortQuery[key];
  //     }
  //   }
  //   const $or: any[] = [
  //     {
  //       'status_history.transfer_branch': {
  //         $in: clientHttpRequest.token.territory,
  //       },
  //     },
  //     {
  //       'status_history.status_from': {
  //         $in: clientHttpRequest.token.territory,
  //       },
  //     },
  //   ];

  //   delete filter_query.date_type;
  //   const $and: any[] = [
  //     { $or },
  //     { set: { $in: territory.set } },
  //     filter_query ? this.getFilterQuery(filter_query) : {},
  //     dateQuery,
  //   ];

  //   const where: { $and: any } = { $and };

  //   // const results = await this.warrantyClaimModel.find({
  //   //   skip,
  //   //   take,
  //   //   where,
  //   //   order: sortQuery,
  //   // });
  //   const results = await this.warrantyClaimModel
  //     .find()
  //     .where(where)
  //     .skip(skip)
  //     .limit(take)
  //     .sort(sortQuery);

  //   return {
  //     docs: results,
  //     length: await this.warrantyClaimModel.count(),
  //     offset: skip,
  //   };
  // }

  customSort(a, b) {
    const claimA = a.claim_no;
    const claimB = b.claim_no;
    // Check if either claim number starts with "BRMA"
    const isBRMAA = claimA.startsWith('BRMA');
    const isBRMAB = claimB.startsWith('BRMA');
    if (isBRMAA || isBRMAB) {
      // Both start with "BRMA", no change in order
      return 0;
    } else {
      // Neither starts with "BRMA", compare them normally
      return claimB.localeCompare(claimA);
    }
  }

  async report(filter_query) {
    let dateQuery = {};
    if (filter_query?.fromDate && filter_query?.toDate) {
      dateQuery = {
        createdOn: {
          $gte: new Date(new Date(filter_query.fromDate).setHours(0, 0, 0, 0)),
          $lte: new Date(
            new Date(filter_query.toDate).setHours(23, 59, 59, 59),
          ),
        },
      };
    }

    let deliveryQuery = {};
    if (filter_query?.delivery_status) {
      deliveryQuery = {
        status_history: {
          $elemMatch: { delivery_status: filter_query?.delivery_status },
        },
      };
    }

    const $or: any[] = [
      {
        'status_history.transfer_branch': {
          $in: [filter_query?.territory],
        },
      },
      {
        'status_history.status_from': {
          $in: [filter_query?.territory],
        },
      },
    ];

    const $and: any[] = [
      filter_query.territory ? { $or } : {},
      filter_query ? this.getReportFilterQuery(filter_query) : {},
      deliveryQuery,
      dateQuery,
    ];

    const where: { $and: any } = { $and };

    const results = await this.warrantyClaimModel.find(where);

    return {
      docs: results || [],
      length: await this.warrantyClaimModel.count(where),
    };
  }

  getReportFilterQuery(query) {
    const keys = Object.keys(query);
    keys.forEach(key => {
      if (query[key]) {
        if (
          key === 'fromDate' ||
          key === 'toDate' ||
          key === 'delivery_status' ||
          key === 'territory'
        ) {
          delete query[key];
        }
      } else {
        delete query[key];
      }
    });
    return query;
  }

  getFilterQuery(query) {
    const keys = Object.keys(query);
    keys.forEach(key => {
      if (query[key]) {
        if (key === 'claim_status' && query[key] === 'All') {
          delete query[key];
        } else {
          if (typeof query[key] === 'string') {
            if (['claim_type'].includes(key)) {
              return;
            }
            query[key] = { $regex: PARSE_REGEX(query[key]), $options: 'i' };
          } else {
            delete query[key];
          }
        }
      } else {
        delete query[key];
      }
    });
    return query;
  }

  async deleteOne(query, options?): Promise<any> {
    return await this.warrantyClaimModel.deleteOne(query, options);
  }

  async deleteMany(query, options?): Promise<any> {
    return await this.warrantyClaimModel.deleteMany(query, options);
  }

  async updateOne(query, options?) {
    const res = await this.warrantyClaimModel.updateOne(query, options);
    return res;
  }

  async updateMany(query, options?) {
    return await this.warrantyClaimModel.updateMany(query, options);
  }

  async insertMany(query, options?) {
    return await this.warrantyClaimModel.insertMany(query, options);
  }

  async count(query) {
    return await this.warrantyClaimModel.count(query);
  }

  asyncAggregate(query, collation): Observable<any> {
    const promise = this.warrantyClaimModel.aggregate(query, collation);
    return new Observable(observer => {
      promise.then(
        settings => {
          observer.next(settings);
          observer.complete();
        },
        error => {
          observer.error(error);
        },
      );
    });
  }

  async generateNamingSeries(type: string) {
    const settings = await firstValueFrom(this.settings.find());
    const date = new DateTime(settings.timeZone).year;
    let sortedDocument;
    switch (type) {
      case 'Bulk':
        sortedDocument = await firstValueFrom(
          this.asyncAggregate(
            [
              {
                $match: {
                  claim_no: { $regex: PARSE_REGEX('RMA-'), $options: 'i' },
                  $expr: { $eq: [{ $year: '$createdOn' }, date] },
                  set: type,
                },
              },
              { $sort: { claim_no: -1 } },
              { $limit: 1 },
            ],
            {
              collation: {
                locale: 'en_US',
                numericOrdering: true,
              },
            },
          ),
        );

        if (!sortedDocument.length) {
          return DEFAULT_NAMING_SERIES.bulk_warranty_claim + date + '-' + '1';
        }

        return this.generateClaimString(sortedDocument.find(x => x).claim_no);

      default:
        sortedDocument = await firstValueFrom(
          this.asyncAggregate(
            [
              {
                $match: {
                  claim_no: { $regex: PARSE_REGEX('RMA-'), $options: 'i' },
                  $expr: {
                    $and: [
                      { $eq: [{ $year: '$createdOn' }, date] },
                      { $ne: ['$claim_no', '$uuid'] },
                    ],
                  },
                  $or: [{ set: CATEGORY.SINGLE }, { set: CATEGORY.PART }],
                },
              },
              { $sort: { claim_no: -1 } },
              { $limit: 1 },
            ],
            {
              collation: {
                locale: 'en_US',
                numericOrdering: true,
              },
            },
          ),
        );

        if (!sortedDocument.length) {
          return DEFAULT_NAMING_SERIES.warranty_claim + date + '-' + '1';
        }
        return this.generateClaimString(
          sortedDocument.find(x => x).claim_no,
          date,
        );
    }
  }

  generateClaimString(claim_no, date?) {
    if (!claim_no) {
      return DEFAULT_NAMING_SERIES.warranty_claim;
    }
    claim_no = claim_no.split('-');
    claim_no[2] = parseInt(claim_no[2], 10) + 1;
    claim_no = claim_no.join('-');
    return claim_no;
  }
}
