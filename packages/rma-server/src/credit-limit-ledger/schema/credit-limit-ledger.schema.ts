import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type CreditLimitLedgerDocument = HydratedDocument<CreditLimitLedger>;

@Schema({ collection: 'credit_limit_ledger' })
export class CreditLimitLedger {
  @Prop()
  brand_id: string;

  @Prop()
  customer_id: string;

  @Prop()
  actual_qty: number;

  @Prop()
  balance_qty: number;

  @Prop()
  doc_type: string;

  @Prop()
  doc_id: string;

  @Prop({ default: Date.now })
  created_at: Date;
}

export const CreditLimitLedgerSchema =
  SchemaFactory.createForClass(CreditLimitLedger);
