import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PARSE_REGEX } from '../../constants/app-strings';
import { BrandWiseCreditLimitDto } from '../../customer/entity/customer/customer-webhook-interface';
import { Customer } from '../../customer/schemas/customer.schema';
import {
  CreditLimitLedger,
  CreditLimitLedgerDocument,
} from '../schema/credit-limit-ledger.schema';

@Injectable()
export class CreditLimitLedgerService {
  constructor(
    @InjectModel('CreditLimitLedger')
    private model: Model<CreditLimitLedgerDocument>,
  ) {}

  async listLedger(skip, take, search, sort) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }

    const sortQuery = { created_at: sort };
    const where = { $and: JSON.parse(search) };

    if (where.$and.find(x => x.customer_id) === undefined) {
      return {
        docs: [],
        length: 0,
        offset: skip,
      };
    }

    const startFilter = where.$and.find(x => x.start);
    if (startFilter) {
      startFilter.created_at = {
        $gte: new Date(startFilter.start),
      };

      delete startFilter.start;
    }

    const endFilter = where.$and.find(x => x.end);
    if (endFilter) {
      endFilter.created_at = {
        $lte: new Date(endFilter.end),
      };

      delete endFilter.end;
    }

    const results = await this.model
      .find(where)
      .skip(skip)
      .limit(take)
      .sort(sortQuery);

    return {
      docs: results || [],
      length: await this.model.count(where),
      offset: skip,
    };
  }

  async createLedgerEntry(ledger: CreditLimitLedger) {
    if (ledger.actual_qty === 0) {
      return;
    }

    const newLedgerEntry = new this.model(ledger);
    return await newLedgerEntry.save();
  }

  async getLedgerEntry(query) {
    return await this.model.findOne(query);
  }

  async updateCustomerLedger(
    customer: Customer,
    brandLimitChanges: BrandWiseCreditLimitDto[],
    source_doc: string | null,
  ) {
    for (const brandLimitChange of brandLimitChanges) {
      const customerBrandLimit = customer.brand_limits.find(
        item => item.brand_id === brandLimitChange.brand,
      );

      if (brandLimitChange.limit !== 0) {
        await this.createLedgerEntry({
          brand_id: brandLimitChange.brand,
          customer_id: customer.name,
          actual_qty: brandLimitChange.limit,
          balance_qty: customerBrandLimit ? customerBrandLimit.limit : 0,
          doc_type: source_doc
            ? source_doc.indexOf('RV') === 0
              ? 'Payment Entry'
              : 'Journal Entry'
            : 'Customer',
          doc_id: source_doc || 'Manual',
          created_at: new Date(),
        });
      }
    }
  }

  async updateLedgerEntry(query, payload) {
    return await this.model.updateOne(query, payload);
  }

  handleSearchPatterns(columns, search) {
    const searchQuery: any[] = [];

    columns.forEach(field => {
      const fieldType = this.model.schema.paths[field].instance;
      const filter = {};

      if (fieldType === 'String') {
        filter[field] = { $regex: PARSE_REGEX(search), $options: 'i' };
      } else if (fieldType === 'Number') {
        const numericSearch = parseFloat(search);
        if (!isNaN(numericSearch)) {
          filter[field] = numericSearch;
        }
      } else if (fieldType === 'Array') {
        const arraySearch = Array.isArray(search) ? search : [search];
        filter[field] = { $in: arraySearch };
      }

      // Check if the filter object is not empty before pushing it into the array
      if (Object.keys(filter).length > 0) {
        searchQuery.push(filter);
      }
    });

    return searchQuery;
  }
}
