import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../auth/guards/token.guard';
import { RetrieveCreditLimitLedgerQuery } from '../query/list-credit-limit-ledger/retrieve-credit-limit-ledger.query';

@Controller('credit-limit-ledger')
export class CreditLimitLedgerController {
  constructor(private readonly queryBus: QueryBus) {}

  @Get('v1/list')
  @UseGuards(TokenGuard)
  async getCreditLimitLedger(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort: string,
  ) {
    try {
      search = decodeURIComponent(search);
    } catch {}

    return await this.queryBus.execute(
      new RetrieveCreditLimitLedgerQuery(offset, limit, search, sort),
    );
  }
}
