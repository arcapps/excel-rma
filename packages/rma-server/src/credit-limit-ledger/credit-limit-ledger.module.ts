import { Module } from '@nestjs/common';
import { CreditLimitLedgerController } from './controllers/credit-limit-ledger.controller';
import { CreditLimitLedgerEntitiesModule } from './credit-limit-ledger-entities.module';
import { RetrieveCreditLimitLedgerHandler } from './query/list-credit-limit-ledger/retrieve-credit-limit-ledger-query.handler';

@Module({
  imports: [CreditLimitLedgerEntitiesModule],
  controllers: [CreditLimitLedgerController],
  providers: [RetrieveCreditLimitLedgerHandler],
  exports: [CreditLimitLedgerEntitiesModule],
})
export class CreditLimitLedgerModule {}
