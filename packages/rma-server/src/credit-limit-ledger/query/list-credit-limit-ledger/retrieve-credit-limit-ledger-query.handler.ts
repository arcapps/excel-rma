import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { CreditLimitLedgerService } from '../../credit-limit-ledger-service/credit-limit-ledger.service';
import { RetrieveCreditLimitLedgerQuery } from './retrieve-credit-limit-ledger.query';

@QueryHandler(RetrieveCreditLimitLedgerQuery)
export class RetrieveCreditLimitLedgerHandler
  implements IQueryHandler<RetrieveCreditLimitLedgerQuery>
{
  constructor(private readonly manager: CreditLimitLedgerService) {}

  async execute(query: RetrieveCreditLimitLedgerQuery) {
    const { offset, limit, search, sort } = query;
    return await this.manager.listLedger(
      Number(offset),
      Number(limit),
      search,
      sort.toUpperCase(),
    );
  }
}
