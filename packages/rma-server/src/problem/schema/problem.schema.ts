import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type ProblemDocument = HydratedDocument<Problem>;

@Schema({ collection: 'problem' })
export class Problem {
  @Prop()
  uuid: string;

  @Prop()
  problem_name: string;
}

export const ProblemSchema = SchemaFactory.createForClass(Problem);
