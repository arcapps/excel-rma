import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type PurchaseReceiptDocument = HydratedDocument<PurchaseReceipt>;

@Schema({ collection: 'purchase_receipt' })
export class PurchaseReceipt {
  @Prop()
  purchase_invoice_name: string;

  @Prop()
  amount: number;

  @Prop()
  cost_center: string;

  @Prop()
  expense_account: string;

  @Prop()
  item_code: string;

  @Prop()
  item_name: string;

  @Prop()
  purchase_order?: string;

  @Prop()
  name: string;

  @Prop()
  qty: number;

  @Prop()
  rate: number;

  @Prop()
  serial_no: string[];

  @Prop()
  warehouse: string;

  @Prop()
  deliveredBy: string;

  @Prop()
  deliveredByEmail: string;
}

export const PurchaseReceiptSchema =
  SchemaFactory.createForClass(PurchaseReceipt);
