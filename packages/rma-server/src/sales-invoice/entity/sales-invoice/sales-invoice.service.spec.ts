import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { SalesInvoiceService } from './sales-invoice.service';

describe('RequestStateService', () => {
  let service: SalesInvoiceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SalesInvoiceService,
        {
          provide: getModelToken('SalesInvoice'),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<SalesInvoiceService>(SalesInvoiceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
