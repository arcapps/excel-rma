import { HttpService } from '@nestjs/axios';
import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { forkJoin, from, of, throwError } from 'rxjs';
import { concatMap, map, mergeMap, switchMap, toArray } from 'rxjs/operators';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import { getParsedPostingDate } from '../../../constants/agenda-job';
import {
  AUTHORIZATION,
  BEARER_HEADER_VALUE_PREFIX,
  CANCELED_STATUS,
  COMPLETED_STATUS,
  DRAFT_STATUS,
  TO_DELIVER_STATUS,
} from '../../../constants/app-strings';
import {
  CREDIT_LIMIT_ERROR,
  CUSTOMER_AND_CONTACT_INVALID,
  DELIVERY_NOTE_IN_QUEUE,
  INVALID_ITEM_TOTAL,
  ITEMS_SHOULD_BE_UNIQUE,
  SALES_INVOICE_CANNOT_BE_SUBMITTED,
  SALES_INVOICE_NOT_FOUND,
} from '../../../constants/messages';
import {
  FRAPPE_API_GET_CUSTOMER_ENDPOINT,
  FRAPPE_API_SALES_INVOICE_ENDPOINT,
  POST_DELIVERY_NOTE_ENDPOINT,
} from '../../../constants/routes';
import { CustomerWebhookDto } from '../../../customer/entity/customer/customer-webhook-interface';
import { CustomerService } from '../../../customer/entity/customer/customer.service';
import { SalesInvoiceService } from '../../../sales-invoice/entity/sales-invoice/sales-invoice.service';
import { AssignSerialNoPoliciesService } from '../../../serial-no/policies/assign-serial-no-policies/assign-serial-no-policies.service';
import { SerialNoPoliciesService } from '../../../serial-no/policies/serial-no-policies/serial-no-policies.service';
import { StockLedgerService } from '../../../stock-ledger/entity/stock-ledger/stock-ledger.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import {
  ItemDto,
  SalesInvoiceDto,
} from '../../entity/sales-invoice/sales-invoice-dto';
import { CreateSalesReturnDto } from '../../entity/sales-invoice/sales-return-dto';
import { SalesInvoice } from '../../schema/sales-invoice.schema';

@Injectable()
export class SalesInvoicePoliciesService {
  constructor(
    private readonly salesInvoiceService: SalesInvoiceService,
    private readonly customerService: CustomerService,
    private readonly assignSerialPolicyService: AssignSerialNoPoliciesService,
    private readonly serialNoPoliciesService: SerialNoPoliciesService,
    private readonly http: HttpService,
    private readonly clientToken: ClientTokenManagerService,
    private readonly settings: SettingsService,
    private readonly stockLedgerService: StockLedgerService,
  ) {}

  validateSalesInvoice(uuid: string) {
    // eslint-disable-next-line
    // console.log(uuid);
    return from(this.salesInvoiceService.findOne({ where: { uuid } })).pipe(
      switchMap(salesInvoice => {
        // eslint-disable-next-line
        // console.log(salesInvoice);
        if (!salesInvoice) {
          return throwError(
            () => new BadRequestException(SALES_INVOICE_NOT_FOUND),
          );
        }
        return of(salesInvoice);
      }),
    );
  }

  validateItems(items: ItemDto[]) {
    const itemSet = new Set();
    items.forEach(item => {
      itemSet.add(item.item_code);
    });

    const item_code: any[] = Array.from(itemSet);

    if (item_code.length !== items.length) {
      return throwError(() => new BadRequestException(ITEMS_SHOULD_BE_UNIQUE));
    }

    return this.validateItemsTotal(items).pipe(
      switchMap(() => {
        return this.assignSerialPolicyService.validateItem(item_code);
      }),
      switchMap(() => {
        return from(items).pipe(
          mergeMap(item => {
            return this.assignSerialPolicyService.validateItemRate(item);
          }),
        );
      }),
      toArray(),
      switchMap(() => of(true)),
    );
  }

  validateItemsTotal(items: ItemDto[]) {
    for (let i = 0; i <= items.length - 1; i++) {
      if (items[i].amount !== items[i].qty * items[i].rate) {
        return throwError(
          () =>
            new BadRequestException(
              this.assignSerialPolicyService.getMessage(
                INVALID_ITEM_TOTAL,
                items[i].qty * items[i].rate,
                items[i].amount,
              ),
            ),
        );
      }
    }

    return of({});
  }

  validateCustomer(
    salesInvoicePayload: SalesInvoice | SalesInvoiceDto,
    serverSettings: any,
    clientHttpRequest: any,
  ) {
    if (serverSettings.brandWiseCreditLimit === false) {
      return of(true);
    }

    const headers = {
      [AUTHORIZATION]:
        BEARER_HEADER_VALUE_PREFIX + clientHttpRequest.token.accessToken,
    };

    return from(
      this.http.get(
        serverSettings.authServerURL +
          FRAPPE_API_GET_CUSTOMER_ENDPOINT +
          '/' +
          salesInvoicePayload.customer,
        {
          headers,
        },
      ),
    ).pipe(
      switchMap(res => {
        const customer: CustomerWebhookDto = res.data.data;
        if (!customer) {
          return throwError(
            () => new BadRequestException(CUSTOMER_AND_CONTACT_INVALID),
          );
        }

        this.validateBrandsLimit(customer, salesInvoicePayload);
        return of(true);
      }),
    );
  }

  validateBrandsLimit(
    customer: CustomerWebhookDto,
    salesInvoicePayload: SalesInvoice | SalesInvoiceDto,
  ) {
    const brandWiseLimits = customer.custom_brand_wise_allocations;
    const otherBrandsLimit = customer.custom_other_brands_limit;

    const brandTotals = {};
    let otherBrandsTotal = 0;

    for (const item of salesInvoicePayload.items) {
      const brand = item.brand || null;

      const brandWiseLimit = brandWiseLimits.find(x => x.brand === brand);
      if (brand && brandWiseLimit !== undefined) {
        brandTotals[brand] = (brandTotals[brand] || 0) + item.amount;

        if (brandTotals[brand] > brandWiseLimit.limit) {
          throw new BadRequestException(
            `Total sum for brand '${brand}' can't exceed ${brandWiseLimit.limit.toLocaleString()}.`,
          );
        }
      } else {
        otherBrandsTotal += item.amount;

        if (otherBrandsTotal > otherBrandsLimit) {
          throw new BadRequestException(
            `Total sum for other brands can't exceed ${otherBrandsLimit}.`,
          );
        }
      }
    }
  }

  validateCustomerCreditLimit(salesInvoicePayload: {
    customer: string;
    contact_email: string;
  }) {
    return forkJoin({
      customer: from(
        this.customerService.findOne({
          where: {
            name: salesInvoicePayload.customer,
            owner: salesInvoicePayload.contact_email,
          },
        }),
      ),
      settings: this.settings.find(),
    }).pipe(
      switchMap(({ customer, settings }) => {
        if (!customer) {
          return throwError(
            new BadRequestException(CUSTOMER_AND_CONTACT_INVALID),
          );
        }

        // Get customer credit limit for default company.
        let credit_limits = [];
        if (customer.credit_limits && customer.credit_limits.length > 0) {
          credit_limits = customer.credit_limits.filter(limit => {
            if (limit.company === settings.defaultCompany) {
              return limit;
            }
          });
        }

        // Check if fields exist
        if (
          credit_limits.length > 0 &&
          customer.baseCreditLimitAmount &&
          customer.tempCreditLimitPeriod
        ) {
          // Check if credit limit has failed to reset
          if (
            credit_limits[0].credit_limit > customer.baseCreditLimitAmount &&
            customer.tempCreditLimitPeriod < new Date()
          ) {
            return throwError(
              () => new UnauthorizedException(CREDIT_LIMIT_ERROR),
            );
          }
        }
        return of(true);
      }),
    );
  }

  validateSubmittedState(salesInvoicePayload: { uuid: string }) {
    return from(
      this.salesInvoiceService.findOne({
        where: { uuid: salesInvoicePayload.uuid },
      }),
    ).pipe(
      switchMap(salesInvoice => {
        if (salesInvoice.status !== DRAFT_STATUS) {
          return throwError(
            new BadRequestException(
              salesInvoice.status + SALES_INVOICE_CANNOT_BE_SUBMITTED,
            ),
          );
        }
        return of(true);
      }),
    );
  }

  validateQueueState(salesInvoicePayload: { uuid: string }) {
    return from(
      this.salesInvoiceService.findOne({
        where: { uuid: salesInvoicePayload.uuid },
      }),
    ).pipe(
      switchMap(queueState => {
        if (queueState.inQueue) {
          return throwError(
            () => new BadRequestException(DELIVERY_NOTE_IN_QUEUE),
          );
        }
        return of(queueState);
      }),
    );
  }

  validateSalesInvoiceStock(sales_invoice: SalesInvoice, req) {
    return this.settings.find().pipe(
      switchMap(settings => {
        if (!settings.validateStock) {
          return of(true);
        }
        return from(sales_invoice.items).pipe(
          concatMap(item => {
            if (item.has_bundle_item || item.is_stock_item === 0) {
              return of(true);
            }
            const body = {
              item_code: item.item_code,
              warehouse: sales_invoice.delivery_warehouse,
            };
            // const headers = this.settings.getAuthorizationHeaders(req.token);
            return from(
              this.stockLedgerService.asyncAggregate([
                {
                  $match: {
                    item_code: item.item_code,
                    warehouse: body.warehouse,
                  },
                },
                {
                  $group: { _id: null, sum: { $sum: '$actual_qty' } },
                },
                { $project: { sum: 1 } },
              ]),
            ).pipe(
              switchMap((stockCount: [{ sum: number }]) => {
                const message = stockCount.find(summedData => summedData).sum;
                if (message < item.qty) {
                  return throwError(
                    new BadRequestException(`
                  Only ${message} quantity available in stock for item ${item.item_name}, 
                  at warehouse ${sales_invoice.delivery_warehouse}.
                  `),
                  );
                }
                return of(true);
              }),
            );
          }),
        );
      }),
      toArray(),
      switchMap(() => of(true)),
    );
  }

  validateSalesReturnItems(payload: CreateSalesReturnDto) {
    return from(payload.items).pipe(
      mergeMap(item => {
        if (!item.has_serial_no) {
          return of(true);
        }
        const serialSet = new Set();
        const duplicateSerials = [];
        const serials = item.serial_no.split('\n');
        serials.forEach(no => {
          serialSet.has(no) ? duplicateSerials.push(no) : null;
          serialSet.add(no);
        });
        if (Array.from(serialSet).length !== serials.length) {
          return throwError(
            new BadRequestException(
              `Found following as duplicate serials for ${
                item.item_name || item.item_code
              }. 
              ${duplicateSerials.splice(0, 50).join(', ')}...`,
            ),
          );
        }
        return of(true);
      }),
      toArray(),
      switchMap(() => of(true)),
    );
  }

  validateReturnPostingDate(
    createReturnPayload: CreateSalesReturnDto,
    salesInvoice,
  ) {
    if (
      getParsedPostingDate(salesInvoice) >
      getParsedPostingDate(createReturnPayload)
    ) {
      return throwError(
        new BadRequestException(
          'posting date cannot be before sales invoice posting.',
        ),
      );
    }
    return of(salesInvoice);
  }

  validateSalesReturn(createReturnPayload: CreateSalesReturnDto) {
    const items = createReturnPayload.items;
    const data = new Set();
    items.forEach(item => {
      data.add(item.against_sales_invoice);
    });
    const salesInvoiceName: any[] = Array.from(data);
    if (salesInvoiceName.length === 1) {
      return from(
        this.salesInvoiceService.findOne({
          where: { name: salesInvoiceName[0] },
        }),
      ).pipe(
        switchMap(salesInvoice => {
          return this.validateReturnPostingDate(
            createReturnPayload,
            salesInvoice,
          );
        }),
      );
    }
    return throwError(
      new BadRequestException(
        this.getMessage(SALES_INVOICE_NOT_FOUND, 1, salesInvoiceName.length),
      ),
    );
  }

  validateReturnSerials(payload: CreateSalesReturnDto) {
    return from(payload.items).pipe(
      mergeMap(item => {
        if (!item.has_serial_no) {
          return of({ notFoundSerials: [] });
        }
        return this.serialNoPoliciesService.validateReturnSerials({
          delivery_note_names: payload.delivery_note_names,
          item_code: item.item_code,
          serials: item.serial_no.split('\n'),
          warehouse: payload.set_warehouse,
        });
      }),
      toArray(),
      switchMap((res: { notFoundSerials: string[] }[]) => {
        const invalidSerials = [];

        res.forEach(invalidSerial => {
          invalidSerials.push(...invalidSerial.notFoundSerials);
        });

        if (invalidSerials.length > 0) {
          return throwError(
            new BadRequestException({
              invalidSerials: invalidSerials.splice(0, 50).join(', '),
            }),
          );
        }
        return of(true);
      }),
    );
  }

  getMessage(notFoundMessage, expected, found) {
    return `${notFoundMessage}, expected ${expected || 0} found ${found || 0}`;
  }

  validateInvoiceStateForCancel(status: string) {
    if (status === TO_DELIVER_STATUS || status === COMPLETED_STATUS) {
      return of(true);
    }
    return throwError(
      new BadRequestException(
        `Cannot cancel sales invoice with status ${status}`,
      ),
    );
  }

  validateInvoiceOnErp(salesInvoicePayload: { uuid: string; name: string }) {
    return forkJoin({
      headers: this.clientToken.getServiceAccountApiHeaders(),
      settings: this.settings.find(),
    }).pipe(
      switchMap(({ headers, settings }) => {
        return this.http
          .get(
            `${settings.authServerURL}${FRAPPE_API_SALES_INVOICE_ENDPOINT}/${salesInvoicePayload.name}`,
            { headers },
          )
          .pipe(
            map(res => res.data),
            switchMap(async (invoice: { docstatus: number }) => {
              if (invoice.docstatus === 2) {
                await this.salesInvoiceService.updateOne(
                  { uuid: salesInvoicePayload.uuid },
                  {
                    $set: {
                      status: CANCELED_STATUS,
                      inQueue: false,
                      isSynced: true,
                    },
                  },
                );
                return throwError(
                  new BadRequestException('Invoice already Cancelled'),
                );
              }
              return of(true);
            }),
          );
      }),
    );
  }

  getDeliveryNotes(sales_invoice_name: string) {
    return forkJoin({
      headers: this.clientToken.getServiceAccountApiHeaders(),
      settings: this.settings.find(),
    }).pipe(
      switchMap(({ headers, settings }) => {
        const params = {
          filters: JSON.stringify([
            ['against_sales_invoice', '=', sales_invoice_name],
            ['docstatus', '!=', 2],
          ]),
        };
        return this.http
          .get(`${settings.authServerURL}${POST_DELIVERY_NOTE_ENDPOINT}`, {
            params,
            headers,
          })
          .pipe(
            map(res => res.data.data),
            switchMap((deliveryNotes: any[]) => {
              return of(deliveryNotes.map(delivery_note => delivery_note.name));
            }),
          );
      }),
    );
  }

  getSalesInvoices(sales_invoice_name: string) {
    return forkJoin({
      headers: this.clientToken.getServiceAccountApiHeaders(),
      settings: this.settings.find(),
    }).pipe(
      switchMap(({ headers, settings }) => {
        const params = {
          filters: JSON.stringify([
            ['return_against', '=', sales_invoice_name],
            ['docstatus', '!=', 2],
          ]),
        };
        return this.http
          .get(
            `${settings.authServerURL}${FRAPPE_API_SALES_INVOICE_ENDPOINT}`,
            {
              params,
              headers,
            },
          )
          .pipe(
            map(res => res.data.data),
            switchMap((salesInvoices: any[]) => {
              if (salesInvoices.length !== 0)
                return of(
                  salesInvoices.map(sales_invoice => sales_invoice.name),
                );
              return of([]);
            }),
          );
      }),
    );
  }
}
