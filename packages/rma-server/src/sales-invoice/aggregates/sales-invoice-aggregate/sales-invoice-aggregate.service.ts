import { HttpService } from '@nestjs/axios';

import {
  BadRequestException,
  Injectable,
  NotFoundException,
  NotImplementedException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { DateTime } from 'luxon';
import { firstValueFrom, forkJoin, from, of, throwError } from 'rxjs';
import {
  catchError,
  concatMap,
  map,
  switchMap,
  tap,
  toArray,
} from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
// import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';
import { TokenCache } from '../../../auth/schemas/tokenCache.schema';
import {
  formatDateForPosting,
  formatTimeNow,
  getParsedPostingDate,
} from '../../../constants/agenda-job';
import {
  ACCEPT,
  ALL_TERRITORIES,
  APPLICATION_JSON_CONTENT_TYPE,
  APP_JSON,
  // APP_WWW_FORM_URLENCODED,
  AUTHORIZATION,
  BEARER_HEADER_VALUE_PREFIX,
  CANCELED_STATUS,
  CONTENT_TYPE,
  DEFAULT_NAMING_SERIES,
  DELIVERY_NOTE,
  DELIVERY_NOTE_LIST_FIELD,
  DRAFT_STATUS,
  INVOICE_DELIVERY_STATUS,
  REJECTED_STATUS,
  RETURN_DELIVERY_NOTE,
  SALES_INVOICE_STATUS,
  SYSTEM_MANAGER,
} from '../../../constants/app-strings';
import {
  PLEASE_RUN_SETUP,
  SALES_INVOICE_CANNOT_BE_UPDATED,
  SALES_INVOICE_MANDATORY,
} from '../../../constants/messages';
import {
  FRAPPE_API_SALES_INVOICE_ENDPOINT,
  FRAPPE_API_SALES_INVOICE_ITEM_ENDPOINT,
  FRAPPE_CLIENT_CANCEL,
  INVOICE_LIST,
  LIST_CREDIT_NOTE_ENDPOINT,
  LIST_DELIVERY_NOTES_ENDPOINT,
  LIST_DELIVERY_NOTE_ENDPOINT,
  POST_DELIVERY_NOTE_ENDPOINT,
} from '../../../constants/routes';
import { CustomerService } from '../../../customer/entity/customer/customer.service';
import {
  CreateDeliveryNoteInterface,
  CreateDeliveryNoteItemInterface,
} from '../../../delivery-note/entity/delivery-note-service/create-delivery-note-interface';
import { DeliveryNoteWebhookDto } from '../../../delivery-note/entity/delivery-note-service/delivery-note-webhook.dto';
import { DeliveryNote } from '../../../delivery-note/entity/delivery-note-service/delivery-note.entity';
import { DeliveryNoteService } from '../../../delivery-note/entity/delivery-note-service/delivery-note.service';
import { ErrorLogService } from '../../../error-log/error-log-service/error-log.service';
import { ItemAggregateService } from '../../../item/aggregates/item-aggregate/item-aggregate.service';
import { ItemService } from '../../../item/entity/item/item.service';
import { Item } from '../../../item/schema/item.schema';
import { SalesInvoicePoliciesService } from '../../../sales-invoice/policies/sales-invoice-policies/sales-invoice-policies.service';
import { SerialNoHistoryService } from '../../../serial-no/entity/serial-no-history/serial-no-history.service';
import { SerialNoService } from '../../../serial-no/entity/serial-no/serial-no.service';
import { SerialNoHistoryPoliciesService } from '../../../serial-no/policies/serial-no-history-policies/serial-no-history-policies.service';
import { SerialNoPoliciesService } from '../../../serial-no/policies/serial-no-policies/serial-no-policies.service';
import {
  EventType,
  SerialNoHistoryInterface,
} from '../../../serial-no/schema/serial-no-history.schema';
import { StockLedgerService } from '../../../stock-ledger/entity/stock-ledger/stock-ledger.service';
import { StockLedger } from '../../../stock-ledger/schema/stock-ledger.schema';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ServerSettings } from '../../../system-settings/schemas/server-settings.schema';
import {
  MRPRateUpdateInterface,
  SalesInvoiceDto,
} from '../../entity/sales-invoice/sales-invoice-dto';
import { SalesInvoiceUpdateDto } from '../../entity/sales-invoice/sales-invoice-update-dto';
import { SalesInvoiceService } from '../../entity/sales-invoice/sales-invoice.service';
import { SalesReturnCancelDto } from '../../entity/sales-invoice/sales-return-cancel-dto';
import {
  CreateSalesReturnDto,
  SalesReturnItemDto,
} from '../../entity/sales-invoice/sales-return-dto';
import { SalesInvoiceAddedEvent } from '../../event/sales-invoice-added/sales-invoice-added.event';
import { SalesInvoiceRemovedEvent } from '../../event/sales-invoice-removed/sales-invoice-removed.event';
import { SalesInvoiceSubmittedEvent } from '../../event/sales-invoice-submitted/sales-invoice-submitted.event';
import { SalesInvoiceUpdatedEvent } from '../../event/sales-invoice-updated/sales-invoice-updated.event';
import { SalesInvoice } from '../../schema/sales-invoice.schema';
import { DELIVERY_NOTE_FILTER_BY_SALES_INVOICE_QUERY } from '../../../constants/query';
@Injectable()
export class SalesInvoiceAggregateService extends AggregateRoot {
  constructor(
    private readonly clientToken: ClientTokenManagerService,
    private readonly customerService: CustomerService,
    private readonly deliveryNoteService: DeliveryNoteService,
    private readonly errorLogService: ErrorLogService,
    private readonly http: HttpService,
    private readonly itemAggregateService: ItemAggregateService,
    private readonly itemService: ItemService,
    private readonly salesInvoiceService: SalesInvoiceService,
    private readonly serialNoHistoryService: SerialNoHistoryService,
    private readonly serialNoPolicyService: SerialNoPoliciesService,
    private readonly serialNoService: SerialNoService,
    private readonly settingsService: SettingsService,
    private readonly stockLedgerService: StockLedgerService,
    private readonly validateSalesInvoicePolicy: SalesInvoicePoliciesService,
    private readonly validateSerialNoHistoryPolicy: SerialNoHistoryPoliciesService,
  ) {
    super();
  }

  addSalesInvoice(salesInvoicePayload: SalesInvoiceDto, clientHttpRequest) {
    return this.settingsService.find().pipe(
      switchMap(settings => {
        return this.validateSalesInvoicePolicy
          .validateCustomer(salesInvoicePayload, settings, clientHttpRequest)
          .pipe(
            switchMap(() => {
              return this.validateSalesInvoicePolicy.validateItems(
                salesInvoicePayload.items,
              );
            }),
            switchMap(() => {
              const salesInvoice = new SalesInvoice();
              Object.assign(salesInvoice, salesInvoicePayload);
              salesInvoice.createdByEmail = clientHttpRequest.token.email;
              salesInvoice.createdBy = clientHttpRequest.token.fullName;
              salesInvoice.uuid = uuidv4();
              salesInvoice.delivery_status =
                INVOICE_DELIVERY_STATUS.DELIVERED_TO_CUSTOMER;
              salesInvoice.created_on = new DateTime(
                settings.timeZone,
              ).toJSDate();

              if (!salesInvoice.posting_date) {
                salesInvoice.posting_date = formatDateForPosting(
                  salesInvoice.created_on,
                );
              }
              salesInvoice.timeStamp = {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                //  @ts-ignore
                created_on: formatTimeNow(),
              };
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              //  @ts-ignore
              salesInvoice.created_on = formatTimeNow();
              salesInvoice.isSynced = false;
              salesInvoice.inQueue = false;
              this.apply(
                new SalesInvoiceAddedEvent(salesInvoice, clientHttpRequest),
              );
              return of(salesInvoice);
            }),
          );
      }),
    );
  }

  async retrieveSalesInvoice(uuid: string, req) {
    let filter = {};
    const territory = this.getUserTerritories(req);
    if (!territory?.includes(ALL_TERRITORIES)) {
      filter = { territory: { $in: territory } };
    }

    const provider = await this.salesInvoiceService.findOne(
      { where: { $or: [{ uuid }, { name: uuid }], ...filter } },
      undefined,
      true,
    );
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async retrieveSalesInvoiceForPOS(uuid: string, req) {
    let filter = {};
    const territory = this.getUserTerritories(req);
    if (!territory?.includes(ALL_TERRITORIES)) {
      filter = { territory: { $in: territory } };
    }

    const provider = await this.salesInvoiceService.findOne(
      { where: { $or: [{ uuid }, { name: uuid }], ...filter } },
      undefined,
      true,
    );
    if (!provider) return null;
    return provider;
  }

  async getSalesInvoiceList(
    offset,
    limit,
    sort,
    filter_query,
    clientHttpRequest: { token: TokenCache },
  ) {
    const territory = this.getUserTerritories(clientHttpRequest);
    return await this.salesInvoiceService.list(
      offset || 0,
      limit || 10,
      sort,
      filter_query,
      territory,
    );
  }

  getUserTerritories(clientHttpRequest: { token: TokenCache }) {
    return clientHttpRequest.token.roles?.includes(SYSTEM_MANAGER)
      ? [ALL_TERRITORIES]
      : clientHttpRequest.token.territory;
  }

  async remove(uuid: string) {
    const salesInvoice = await this.salesInvoiceService.findOne({
      where: { uuid },
    });
    if (!salesInvoice) {
      throw new NotFoundException();
    }
    if (salesInvoice.status !== SALES_INVOICE_STATUS.draft) {
      return throwError(
        () =>
          new BadRequestException(
            `Sales Invoice with ${salesInvoice.status} status cannot be deleted.`,
          ),
      );
    }
    this.apply(new SalesInvoiceRemovedEvent(salesInvoice));
  }

  async update(updatePayload: SalesInvoiceUpdateDto, clientHttpRequest: any) {
    const isValid = await firstValueFrom(
      this.validateSalesInvoicePolicy.validateItems(updatePayload.items),
    );

    if (!isValid) {
      throw new BadRequestException('Failed to validate Sales Invoice Items.');
    }

    const settings = await firstValueFrom(this.settingsService.find());
    await firstValueFrom(
      this.validateSalesInvoicePolicy.validateCustomer(
        updatePayload,
        settings,
        clientHttpRequest,
      ),
    );

    const provider = await this.salesInvoiceService.findOne({
      where: { uuid: updatePayload.uuid },
    });

    if (!provider) {
      throw new NotFoundException();
    }

    if (provider.status !== DRAFT_STATUS) {
      throw new BadRequestException(
        provider.status + SALES_INVOICE_CANNOT_BE_UPDATED,
      );
    }

    this.apply(new SalesInvoiceUpdatedEvent(updatePayload));
  }

  submitSalesInvoice(uuid: string, clientHttpRequest: any) {
    return this.settingsService.find().pipe(
      switchMap(settings => {
        return this.validateSalesInvoicePolicy.validateSalesInvoice(uuid).pipe(
          switchMap(salesInvoice => {
            return this.validateSalesInvoicePolicy
              .validateCustomer(salesInvoice, settings, clientHttpRequest)
              .pipe(
                switchMap(() => {
                  return this.validateSalesInvoicePolicy.validateItems(
                    salesInvoice.items,
                  );
                }),
                switchMap(() => {
                  return this.validateSalesInvoicePolicy.validateCustomerCreditLimit(
                    salesInvoice,
                  );
                }),
                switchMap(() => {
                  return this.validateSalesInvoicePolicy.validateSubmittedState(
                    salesInvoice,
                  );
                }),
                switchMap(() => {
                  return this.validateSalesInvoicePolicy.validateQueueState(
                    salesInvoice,
                  );
                }),
                switchMap(() => {
                  return this.validateSalesInvoicePolicy.validateSalesInvoiceStock(
                    salesInvoice,
                    clientHttpRequest,
                  );
                }),
                switchMap(() => {
                  salesInvoice.naming_series =
                    DEFAULT_NAMING_SERIES.sales_invoice;
                  return from(
                    this.salesInvoiceService.updateOne(
                      { uuid: salesInvoice.uuid },
                      { $set: { inQueue: true } },
                    ),
                  );
                }),
                toArray(),
                switchMap(() => {
                  return this.syncSubmittedSalesInvoice(
                    salesInvoice,
                    clientHttpRequest,
                  );
                }),
                switchMap(res => {
                  this.apply(new SalesInvoiceSubmittedEvent(salesInvoice));

                  return this.customerService
                    .updateCustomerLimit(
                      clientHttpRequest,
                      settings,
                      salesInvoice.items.map(item => ({
                        brand: item.brand,
                        limit: item.amount,
                      })),
                      salesInvoice.customer,
                      res.name,
                      'Sales Invoice',
                      false,
                    )
                    .pipe(
                      map(() => salesInvoice),
                      catchError(err => {
                        return throwError(() => new BadRequestException(err));
                      }),
                    );
                }),
              );
          }),
        );
      }),
    );
  }

  getDeliveryNotes(salesInvoiceName: string, req) {
    if (!salesInvoiceName) {
      return throwError(() => new BadRequestException(SALES_INVOICE_MANDATORY));
    }
    return this.settingsService.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(
            () => new NotImplementedException(PLEASE_RUN_SETUP),
          );
        }
        // const headers = this.getAuthorizationHeaders(req.token);
        const params = {
          filters: JSON.stringify([
            [...DELIVERY_NOTE_FILTER_BY_SALES_INVOICE_QUERY, salesInvoiceName],
          ]),
          fields: JSON.stringify(DELIVERY_NOTE_LIST_FIELD),
        };
        return this.http
          .get(settings.authServerURL + LIST_DELIVERY_NOTES_ENDPOINT, {
            headers: {
              [AUTHORIZATION]:
                BEARER_HEADER_VALUE_PREFIX + req.token.accessToken,
              [CONTENT_TYPE]: APP_JSON,
              [ACCEPT]: APPLICATION_JSON_CONTENT_TYPE,
            },
            params,
          })
          .pipe(
            switchMap(response => {
              if (!response?.data?.data?.length) return of([]);
              const uniqueArray = response.data.data.filter(
                (item, index, self) =>
                  index ===
                  self.findIndex(
                    t =>
                      t.name === item.name &&
                      t.title === item.title &&
                      t.status === item.status &&
                      t.posting_date === item.posting_date &&
                      t.total === item.total &&
                      t.owner === item.owner &&
                      t.modified_by === item.modified_by,
                  ),
              );
              return of(uniqueArray);
            }),
          );
      }),
      catchError(error => {
        return throwError(() => new BadRequestException(error));
      }),
    );
  }

  getDeliveryNoteItems(deliveryNote, req) {
    if (!deliveryNote) {
      return throwError(
        () => new BadRequestException('Delivery Note is Mandatory'),
      );
    }
    return this.settingsService.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(
            () => new NotImplementedException(PLEASE_RUN_SETUP),
          );
        }
        const headers = this.getAuthorizationHeaders(req.token);
        return this.http
          .get(
            settings.authServerURL + LIST_DELIVERY_NOTE_ENDPOINT + deliveryNote,
            {
              headers,
            },
          )
          .pipe(
            switchMap(response => {
              return of(response.data.data);
            }),
          );
      }),
      catchError(error => {
        return throwError(() => new BadRequestException(error));
      }),
    );
  }

  getAuthorizationHeaders(token) {
    return {
      [AUTHORIZATION]: BEARER_HEADER_VALUE_PREFIX + token.accessToken,
    };
  }

  createSalesReturn(
    createReturnPayload: CreateSalesReturnDto,
    clientHttpRequest,
  ) {
    let creditNoteName = null;
    // pretty bad code. will need cleanup. could be done when this is changed to queue.
    return this.settingsService.find().pipe(
      switchMap(settings => {
        if (!settings) {
          return throwError(() => new NotImplementedException());
        }

        return forkJoin({
          items:
            this.validateSalesInvoicePolicy.validateSalesReturnItems(
              createReturnPayload,
            ),
          salesInvoice:
            this.validateSalesInvoicePolicy.validateSalesReturn(
              createReturnPayload,
            ),
          valid:
            this.validateSalesInvoicePolicy.validateReturnSerials(
              createReturnPayload,
            ),
          validSerialHistory:
            this.validateSerialNoHistoryPolicy.validateSerialHistory(
              createReturnPayload,
            ),
        }).pipe(
          switchMap(({ salesInvoice }) => {
            delete createReturnPayload.delivery_note_names;
            const serialMap = this.getSerialMap(createReturnPayload);
            let deliveryNote = new DeliveryNote();
            Object.assign(deliveryNote, createReturnPayload);
            deliveryNote.instructions = createReturnPayload.remarks;
            delete deliveryNote.credit_note_items;

            return from(
              this.deliveryNoteService.findOne({
                where: {
                  sales_invoice_name: salesInvoice.name,
                  name: { $exists: true },
                },
              }),
            ).pipe(
              switchMap(salesDeliveryNote => {
                deliveryNote = this.setDeliveryNoteDefaults(
                  deliveryNote,
                  createReturnPayload,
                );
                const deliveryNoteTemp: any = deliveryNote;
                deliveryNoteTemp.return_against =
                  createReturnPayload.return_against;
                deliveryNoteTemp.territory = salesInvoice.territory;

                return this.http
                  .post(
                    settings.authServerURL + POST_DELIVERY_NOTE_ENDPOINT,
                    deliveryNoteTemp,
                    {
                      headers: {
                        [AUTHORIZATION]:
                          BEARER_HEADER_VALUE_PREFIX +
                          clientHttpRequest.token.accessToken,
                        [CONTENT_TYPE]: APP_JSON,
                        [ACCEPT]: APPLICATION_JSON_CONTENT_TYPE,
                      },
                    },
                  )
                  .pipe(
                    map(data => {
                      return data.data.data;
                    }),
                    switchMap((response: DeliveryNoteWebhookDto) => {
                      response.items.filter(item => {
                        item.serial_no = serialMap[item.item_code];
                        return item;
                      });
                      this.salesInvoiceService.updateOne(
                        { name: salesInvoice.name },
                        { return_delivery_note: response.name },
                      );

                      const items = this.mapSerialsFromItem(response.items);
                      const { returned_items_map } = this.getReturnedItemsMap(
                        items,
                        salesInvoice,
                      );

                      const salesInvoiceTemp = JSON.parse(
                        JSON.stringify(salesInvoice),
                      );
                      salesInvoiceTemp.delivery_warehouse =
                        createReturnPayload.set_warehouse;

                      this.createCreditNote(
                        settings,
                        createReturnPayload,
                        clientHttpRequest,
                        salesInvoice,
                        items,
                      )
                        .pipe(
                          switchMap(res => {
                            creditNoteName = res?.data?.data?.name;
                            return this.linkSalesReturn(
                              items,
                              response.name,
                              clientHttpRequest.token,
                              salesInvoiceTemp,
                              response.set_warehouse,
                              settings,
                              creditNoteName,
                            ).pipe(
                              switchMap(() => {
                                return this.customerService.updateCustomerLimit(
                                  clientHttpRequest,
                                  settings,
                                  createReturnPayload.items.map(item => ({
                                    brand: item.brand,
                                    limit: item.amount,
                                  })),
                                  createReturnPayload.customer,
                                  creditNoteName,
                                  'Sales Return Invoice',
                                  true,
                                );
                              }),
                            );
                          }),
                        )
                        .subscribe();

                      this.salesInvoiceService
                        .updateOne(
                          { uuid: salesInvoice.uuid },
                          { $set: { returned_items_map } },
                        )
                        .then(success => {})
                        .catch(error => {});

                      return of({});
                    }),
                    catchError(err => {
                      this.errorLogService.createErrorLog(
                        err,
                        'Delivery Note',
                        'deliveryNote',
                        clientHttpRequest,
                      );
                      return throwError(err);
                    }),
                  );
              }),
            );
          }),
          catchError(err => {
            if (
              err &&
              err.response &&
              err.response.data &&
              err.response.data.exc
            ) {
              return throwError(err.response.data.exc);
            }
            return throwError(err);
          }),
        );
      }),
      catchError(err => {
        this.errorLogService.createErrorLog(
          err,
          'Delivery Note',
          'deliveryNote',
          clientHttpRequest,
        );
        return throwError(err);
      }),
    );
  }

  setDeliveryNoteDefaults(deliveryNote: DeliveryNote, createReturnPayload) {
    deliveryNote.naming_series = DEFAULT_NAMING_SERIES.delivery_return;
    deliveryNote.items = createReturnPayload?.items?.map(item => {
      return {
        ...item,
        excel_serials:
          item.serial_no === 'Non Serial Item'
            ? 'Non Serial Item'
            : item?.serial_no?.replace(/\n/g, ', '),
        serial_no: undefined,
      };
    });
    return deliveryNote;
  }

  addSalesInvoiceBundleMap(salesInvoice: SalesInvoice) {
    let itemsMap = {};
    salesInvoice.items.forEach(item => {
      itemsMap[item.item_code] = item.qty;
    });
    return this.itemAggregateService.getBundleItems(itemsMap).pipe(
      switchMap(data => {
        itemsMap = {};
        data.forEach(item => {
          itemsMap[Buffer.from(item.item_code).toString('base64')] = item.qty;
        });
        return from(
          this.salesInvoiceService.updateOne(
            { uuid: salesInvoice.uuid },
            {
              $set: {
                bundle_items_map: itemsMap,
              },
            },
          ),
        );
      }),
    );
  }

  syncSubmittedSalesInvoice(
    salesInvoice: SalesInvoice,
    clientHttpRequest: any,
  ) {
    return this.settingsService
      .find()
      .pipe(
        switchMap(settings => {
          if (!settings || !settings.authServerURL) {
            this.salesInvoiceService
              .updateOne(
                { uuid: salesInvoice.uuid },
                {
                  $set: {
                    inQueue: false,
                    'timeStamp.created_on': getParsedPostingDate(salesInvoice),
                  },
                },
              )
              .then(() => {})
              .catch(() => {});
            return throwError(() => new NotImplementedException());
          }
          const body = this.mapSalesInvoice(salesInvoice);

          return this.http.post(
            settings.authServerURL + FRAPPE_API_SALES_INVOICE_ENDPOINT,
            body,
            {
              headers: {
                [AUTHORIZATION]:
                  BEARER_HEADER_VALUE_PREFIX +
                  clientHttpRequest.token.accessToken,
                [CONTENT_TYPE]: APP_JSON,
                [ACCEPT]: APPLICATION_JSON_CONTENT_TYPE,
              },
            },
          );
        }),
      )
      .pipe(map(data => data.data.data))
      .pipe(
        switchMap(successResponse => {
          this.updateBundleItem(salesInvoice);
          return from(
            this.salesInvoiceService.updateOne(
              { uuid: salesInvoice.uuid },
              {
                $set: {
                  isSynced: true,
                  submitted: true,
                  inQueue: false,
                  name: successResponse.name,
                },
              },
            ),
          ).pipe(map(() => successResponse));
        }),
        catchError(err => {
          this.errorLogService.createErrorLog(
            err,
            'Sales Invoice',
            'salesInvoice',
            clientHttpRequest,
          );
          this.salesInvoiceService
            .updateOne(
              { uuid: salesInvoice.uuid },
              {
                $set: {
                  inQueue: false,
                  isSynced: false,
                  submitted: false,
                  status: DRAFT_STATUS,
                },
              },
            )
            .then(() => {})
            .catch(() => {});
          return throwError(
            new BadRequestException(err.response ? err.response.data.exc : err),
          );
        }),
      );
  }

  mapSalesInvoice(salesInvoice: SalesInvoice) {
    salesInvoice.items = salesInvoice.items?.map(item => {
      if (!item.rate) {
        item.price_list_rate = 0.0;
        return item;
      }
      return item;
    });
    return {
      // title: salesInvoice.title ,
      docstatus: 1,
      customer: salesInvoice.customer,
      company: salesInvoice.company,
      posting_date: salesInvoice.posting_date,
      set_posting_time: salesInvoice.set_posting_time,
      due_date: salesInvoice.due_date,
      contact_email: salesInvoice.contact_email,
      territory: salesInvoice.territory,
      total_qty: salesInvoice.total_qty,
      update_stock: false,
      total: salesInvoice.total,
      items: salesInvoice.items.filter(each => {
        delete each.owner;
        return each;
      }),
      timesheets: salesInvoice.timesheets,
      discount_amount: salesInvoice.discount_amount,
      taxes_and_charges: salesInvoice.taxes_and_charges,
      taxes: salesInvoice.taxes,
      payment_schedule: salesInvoice.payment_schedule,
      payments: salesInvoice.payments,
      sales_team: salesInvoice.sales_team,
      remarks: salesInvoice.remarks,
      is_pos: salesInvoice.is_pos,
      // status: 'Draft',
      // taxes:[{
      // charge_type:,
      // account_head:
      // if charge_type = On Net Total then tax_rate:'' if actual then tax_amount
      // }]
    };
  }

  private cancelDoc(docName: string, settings: ServerSettings, request: any) {
    const headers = {
      [AUTHORIZATION]: BEARER_HEADER_VALUE_PREFIX + request.token.accessToken,
    };

    return this.http.post(
      settings.authServerURL + FRAPPE_CLIENT_CANCEL,
      { doctype: 'Sales Invoice', name: docName },
      { headers },
    );
  }

  private cancelReturnDeliveryNote(
    docName: string,
    settings: ServerSettings,
    request: any,
  ) {
    const headers = {
      [AUTHORIZATION]: BEARER_HEADER_VALUE_PREFIX + request.token.accessToken,
    };

    return this.http.post(
      settings.authServerURL + FRAPPE_CLIENT_CANCEL,
      { doctype: 'Delivery Note', name: docName },
      { headers },
    );
  }

  private async resetSalesReturnItemsQuantities(
    items: CreateDeliveryNoteItemInterface[],
    salesInvoice: SalesInvoice,
  ) {
    const { returned_items_map } = this.getReturnedItemsMap(
      items,
      salesInvoice,
    );

    await this.salesInvoiceService.updateOne(
      { name: salesInvoice.name },
      { $set: { returned_items_map } },
    );
  }

  private async resetSalesReturnSerials(
    returnInvoiceName: string,
    salesInvoice: SalesInvoice,
    settings,
    request,
  ) {
    // eslint-disable-next-line no-console
    this.serialNoHistoryService
      .deleteMany({
        document_no: returnInvoiceName,
      })
      .then(res => {})
      .catch(err => {});

    this.serialNoService
      .find({
        sales_return_name: returnInvoiceName,
      })
      .then(serials => {
        const serialNumbers = serials.map(serial => serial.serial_no);

        // eslint-disable-next-line no-console
        this.serialNoHistoryService
          .findOne({
            where: {
              parent_document: salesInvoice.name,
              document_type: 'Delivery Note',
            },
          })
          .then(deliveryHistory => {
            if (serialNumbers) {
              const formattedString = serialNumbers.join('\n');
              const updatedReturnedItems = salesInvoice.returned_items.filter(
                item => {
                  return item.serial_no !== formattedString;
                },
              );
              this.salesInvoiceService
                .updateOne(
                  { name: salesInvoice.name },
                  {
                    $set: {
                      returned_items: updatedReturnedItems,
                    },
                  },
                )
                .then(res => {})
                .catch(err => {});
            }
            this.serialNoService
              .updateMany(
                { serial_no: { $in: serialNumbers } },
                {
                  $set: {
                    customer: salesInvoice.customer,
                    sales_invoice_name: salesInvoice.name,
                    'warranty.soldOn': salesInvoice.posting_date,
                    warehouse: salesInvoice.delivery_warehouse,
                    delivery_note: deliveryHistory?.document_no,
                  },
                  $unset: { sales_return_name: '' },
                },
              )
              .then(res => {})
              .catch(err => {});
          });
      });

    // const salesInvoiceData = await this.salesInvoiceService.findOne({
    //   where: {
    //     name: salesInvoice.name,
    //   },
    // });
  }

  getWarrantyDate(postingDate) {
    // Parse the input date string into a Date object
    const inputDate = new Date(postingDate);

    // Get the year, month, and day components
    const year = inputDate.getFullYear();
    let month: any = inputDate.getMonth() + 1; // Month is 0-based, so add 1
    let day: any = inputDate.getDate();

    // Ensure single-digit months and days are formatted with leading zeros
    if (month < 10) {
      month = '0' + month;
    }
    if (day < 10) {
      day = '0' + day;
    }

    // Format the components into the desired string format
    return year + '-' + month + '-' + day;
  }

  private async resetItemsInvoiceAndLedgers(
    ledgers: StockLedger[],
    returnInvoiceName: string,
    salesInvoiceName: string,
  ) {
    const pullReturnedItemsPromises = [];
    const updateSalesInvoicePromises = [];
    const deleteStockLedgersPromises = [];

    for (const ledger of ledgers) {
      // updateSalesInvoicePromises.push(
      //   this.salesInvoiceService.updateOneWithOptions(
      //     { name: salesInvoiceName },
      //     { $inc: { 'items.$[e].qty': ledger.actual_qty } },
      //     { arrayFilters: [{ 'e.item_code': ledger.item_code }] },
      //   ),
      // );

      pullReturnedItemsPromises.push(
        this.salesInvoiceService.updateOne(
          { name: salesInvoiceName },
          {
            $pull: {
              returned_items: {
                item_code: ledger.item_code,
                sales_return_name: returnInvoiceName,
              },
            },
          },
        ),
      );

      deleteStockLedgersPromises.push(
        this.stockLedgerService.deleteOne({
          name: ledger.name,
        }),
      );
    }

    await Promise.all(
      [].concat(
        updateSalesInvoicePromises,
        pullReturnedItemsPromises,
        deleteStockLedgersPromises,
      ),
    );
  }

  async cancelSalesReturn(
    cancelSalesReturnDto: SalesReturnCancelDto,
    request: any,
  ) {
    // eslint-disable-next-line no-console
    const { returnInvoiceName, saleInvoiceName } = cancelSalesReturnDto;
    let salesInvoice: SalesInvoice;
    try {
      // const promisesArray = [];
      // eslint-disable-next-line no-console

      const settings = await firstValueFrom(this.settingsService.find());

      if (!settings.authServerURL) {
        throw new NotImplementedException(PLEASE_RUN_SETUP);
      }
      // eslint-disable-next-line no-console

      salesInvoice = await this.salesInvoiceService.findOne({
        where: { name: saleInvoiceName },
      });

      const returnedInvoiceItems = salesInvoice.returned_items.filter(
        item => item.credit_note === returnInvoiceName,
      );

      const returnedItemsTemp = salesInvoice.returned_items.find(
        item => item.credit_note === returnInvoiceName,
      );

      const returnedItemsSerialNos = [];
      returnedInvoiceItems.forEach(item => {
        if (
          item.serial_no &&
          item.serial_no.toLowerCase() !== 'non serial item'
        ) {
          returnedItemsSerialNos.push(...item.serial_no.split('\n'));
        }
      });

      if (returnedItemsSerialNos.length) {
        // eslint-disable-next-line no-console

        const invalidSerialNos =
          await this.serialNoPolicyService.findInvalidCancelReturnSerials(
            returnedItemsSerialNos,
            saleInvoiceName,
          );

        if (invalidSerialNos.length) {
          const serialNosString = invalidSerialNos.join(' ');

          const message =
            invalidSerialNos.length > 1
              ? `Serial numbers (${serialNosString}) are ` +
                `already sold. You cannot cancel this return.`
              : `Serial number (${serialNosString}) is ` +
                `already sold. You cannot cancel this return.`;

          throw new BadRequestException(message);
        }
      }
      // promisesArray.push(
      //   firstValueFrom(this.cancelDoc(returnInvoiceName, settings, request)),
      // );
      firstValueFrom(this.cancelDoc(returnInvoiceName, settings, request));
      if (returnedItemsTemp?.sales_return_name) {
        // promisesArray.push(
        //   firstValueFrom(
        //     this.cancelReturnDeliveryNote(
        //       returnedItemsTemp?.sales_return_name,
        //       settings,
        //       request,
        //     ),
        //   ),
        // );
        // eslint-disable-next-line no-console
        firstValueFrom(
          this.cancelReturnDeliveryNote(
            returnedItemsTemp?.sales_return_name,
            settings,
            request,
          ),
        );
      }
      const returnDeliveryNote = salesInvoice.returned_items.find(
        item => item.credit_note === cancelSalesReturnDto.returnInvoiceName,
      );

      const ledgers = await this.stockLedgerService.find({
        // voucher_no: returnInvoiceName,
        voucher_no: returnDeliveryNote.sales_return_name,
      });

      const items = ledgers.map(ledger => {
        const item: CreateDeliveryNoteItemInterface = {
          item_code: ledger.item_code,
          qty: ledger.actual_qty,
          has_serial_no: 0,
        };

        return item;
      });
      await this.resetItemsInvoiceAndLedgers(
        ledgers,
        returnInvoiceName,
        salesInvoice.name,
      );
      // promisesArray.push(
      //   this.resetItemsInvoiceAndLedgers(
      //     ledgers,
      //     returnInvoiceName,
      //     salesInvoice.name,
      //   ),
      // );
      // promisesArray.push(
      //   this.resetSalesReturnItemsQuantities(items, salesInvoice),
      // );
      // eslint-disable-next-line no-console

      await this.resetSalesReturnItemsQuantities(items, salesInvoice);
      if (returnedItemsTemp?.sales_return_name) {
        this.resetSalesReturnSerials(
          returnedItemsTemp?.sales_return_name,
          salesInvoice,
          settings,
          request,
        )
          .then(() => {})
          .catch(() => {});
        // promisesArray.push(
        //   this.resetSalesReturnSerials(
        //     returnedItemsTemp?.sales_return_name,
        //     salesInvoice,
        //     settings,
        //     request,
        //   ),
        // );
      }
      await firstValueFrom(
        this.customerService.updateCustomerLimit(
          request,
          settings,
          returnedInvoiceItems.map(item => ({
            brand: item.brand,
            limit: item.amount,
          })),
          salesInvoice.customer,
          returnInvoiceName,
          'Sales Return Invoice',
          true, // Adding up negative values
        ),
      );
      // promisesArray.push(
      //   firstValueFrom(
      //     this.customerService.updateCustomerLimit(
      //       request,
      //       settings,
      //       returnedInvoiceItems.map(item => ({
      //         brand: item.brand,
      //         limit: item.amount,
      //       })),
      //       salesInvoice.customer,
      //       returnInvoiceName,
      //       'Sales Return Invoice',
      //       true, // Adding up negative values
      //     ),
      //   ),
      // );
      // eslint-disable-next-line no-console

      // await Promise.all(promisesArray);
      // eslint-disable-next-line no-console

      // await this.deleteReturnedItems(salesInvoice);
    } catch (error) {
      this.errorLogService.createErrorLog(error, 'Credit Note', 'creditNote');
      throw error;
    }
  }

  async deleteReturnedItems(salesInvoice) {
    await this.salesInvoiceService.updateOne(
      { name: salesInvoice.name },
      { $set: { returned_items_map: {}, returned_items: [] } },
    );
  }

  getSerialMap(createReturnPayload: CreateSalesReturnDto) {
    const hash_map = {};
    createReturnPayload.items.forEach(item => {
      hash_map[item.item_code]
        ? (hash_map[item.item_code] += item.serial_no)
        : (hash_map[item.item_code] = item.serial_no);
    });
    return hash_map;
  }

  getItemSerialNumbers(items: any[]) {
    const serials = items
      .filter(item => item.serial_no)
      .map(item => item.serial_no.split('\n'));

    return serials;
  }

  linkSalesReturn(
    items: any[],
    sales_return_name: string,
    token: any,
    sales_invoice: SalesInvoice,
    warehouse,
    settings,
    creditNote,
  ) {
    const serials = [];

    items = items.filter(item => {
      if (item.serial_no) {
        serials.push(...item.serial_no.split('\n'));
      }
      item.deliveredBy = token.fullName;
      item.deliveredByEmail = token.email;
      item.sales_return_name = sales_return_name;
      item.credit_note = creditNote;
      return item;
    });
    this.serialNoService
      .updateMany(
        { serial_no: { $in: serials } },
        {
          $set: {
            sales_return_name,
            warehouse,
          },
          $unset: {
            customer: '',
            'warranty.soldOn': '',
            'warranty.salesWarrantyDate': '',
            sales_invoice_name: '',
          },
        },
      )
      .then(success => {
        const serialHistory: SerialNoHistoryInterface = {};
        serialHistory.created_by = token.fullName;
        serialHistory.created_on = new DateTime(settings.timeZone).toJSDate();
        serialHistory.document_no = sales_return_name;
        serialHistory.document_type = DELIVERY_NOTE;
        serialHistory.eventDate = new DateTime(settings.timeZone);
        serialHistory.eventType = EventType.SerialReturned;
        serialHistory.parent_document = sales_invoice.name;
        serialHistory.transaction_from = sales_invoice.customer;
        serialHistory.transaction_to = warehouse;
        this.serialNoHistoryService
          .addSerialHistory(serials, serialHistory)
          .subscribe({
            next: success => {},
            error: err => {},
          });
      })
      .then(() => {})
      .catch(() => {});

    this.settingsService
      .getFiscalYear(settings)
      .pipe(
        switchMap((fiscalYear: string) => {
          return from(items).pipe(
            concatMap(item => {
              // query for fetching available stock
              const filter_query = [
                ['item_code', 'like', `${item.item_code}`],
                ['warehouse', 'like', `${warehouse}`],
                ['actual_qty', '!=', 0],
              ];

              const filter_Obj: any = {};
              filter_query.forEach(element => {
                if (element[0] === 'item_code') {
                  filter_Obj['item.item_code'] = element[2];
                }
                if (element[0] === 'warehouse') {
                  filter_Obj['_id.warehouse'] = element[2];
                }
                if (element[1] === '!=') {
                  filter_Obj.stockAvailability = { $gt: element[2] };
                }
              });
              const obj: any = {
                _id: {
                  warehouse: '$warehouse',
                  item_code: '$item_code',
                },
                stockAvailability: {
                  $sum: '$actual_qty',
                },
              };
              const $group: any = obj;
              const where: any = [];
              where.push({ $group });
              const $lookup: any = {
                from: 'item',
                localField: '_id.item_code',
                foreignField: 'item_code',
                as: 'item',
              };
              where.push({ $lookup });
              const $unwind: any = '$item';
              where.push({ $unwind });
              const $match: any = filter_Obj;
              where.push({ $match });
              return this.stockLedgerService.asyncAggregate(where).pipe(
                switchMap(data => {
                  // fetch latest incoming ledger report
                  const where = [];
                  const ledger_filter_obj = {
                    item_code: `${item.item_code}`,
                    warehouse: `${warehouse}`,
                    actual_qty: { $gt: 0 },
                  };
                  const $match: any = ledger_filter_obj;
                  where.push({ $match });
                  const $sort: any = {
                    modified: -1,
                  };
                  where.push({ $sort });
                  return this.stockLedgerService.asyncAggregate(where).pipe(
                    switchMap((latest_stock_ledger: StockLedger) => {
                      const filter_obj = {
                        voucher_no: `${sales_invoice.name}`,
                        item_code: `${item.item_code}`,
                      };
                      const $match: any = filter_obj;
                      const where: any = [];
                      where.push({ $match });
                      return this.stockLedgerService.asyncAggregate(where).pipe(
                        switchMap((created_invoice_ledger: StockLedger) => {
                          return this.createStockLedgerPayload(
                            {
                              saleReturnName: sales_return_name,
                              salesInvoice: sales_invoice as SalesInvoiceDto,
                              returnItem: item,
                            },
                            token,
                            fiscalYear,
                            settings.timeZone,
                            settings.defaultCompany,
                            data,
                            latest_stock_ledger,
                            created_invoice_ledger,
                          ).pipe(
                            switchMap((response: StockLedger) => {
                              return from(
                                this.stockLedgerService.create(response),
                              );
                            }),
                          );
                        }),
                      );
                    }),
                  );
                }),
              );
            }),
            toArray(),
          );
        }),
      )
      .subscribe();

    this.salesInvoiceService
      .updateMany(
        { name: sales_invoice.name },
        { $push: { returned_items: { $each: items } } },
      )
      .then(success => {})
      .catch(err => {});
    return of({});
  }

  updateInvoiceItemsQuantities(
    items: SalesReturnItemDto[],
    salesInvoiceUuid: string,
  ) {
    return from(items).pipe(
      tap(async item => {
        return of(
          await this.salesInvoiceService.updateOneWithOptions(
            { uuid: salesInvoiceUuid },
            { $inc: { 'items.$[e].qty': item.qty } },
            { arrayFilters: [{ 'e.item_code': item.item_code }] },
          ),
        );
      }),
    );
  }

  createStockLedgerPayload(
    payload: {
      saleReturnName: string;
      salesInvoice: SalesInvoiceDto;
      returnItem: SalesReturnItemDto;
    },
    token,
    fiscalYear: string,
    timeZone: any,
    defaultCompany: string,
    data,
    latest_stock_ledger,
    created_invoice_ledger,
  ) {
    let current_valuation_rate;
    let available_stock;
    let pre_incoming_rate;
    if (latest_stock_ledger && latest_stock_ledger.length > 0) {
      current_valuation_rate = latest_stock_ledger[0]?.valuation_rate || 0;
      pre_incoming_rate = latest_stock_ledger[0]?.incoming_rate || 0;
    } else {
      current_valuation_rate = payload.returnItem.rate;
      pre_incoming_rate = payload.returnItem.rate;
    }
    if (data && data.length > 0) {
      available_stock = data[0].stockAvailability;
    } else {
      available_stock = 0;
    }
    const new_quantity = -payload.returnItem.qty + available_stock;

    const date = new DateTime(timeZone).toJSDate();
    const stockPayload = new StockLedger();
    stockPayload.name = uuidv4();
    stockPayload.modified = date;
    stockPayload.modified_by = token.email;
    stockPayload.item_code = payload.returnItem.item_code;
    stockPayload.actual_qty = -payload.returnItem.qty;
    stockPayload.incoming_rate = created_invoice_ledger[0].valuation_rate;

    if (pre_incoming_rate !== stockPayload.incoming_rate) {
      stockPayload.valuation_rate =
        parseFloat(
          this.calculateValuationRate(
            available_stock,
            stockPayload.actual_qty,
            stockPayload.incoming_rate,
            current_valuation_rate,
            new_quantity,
          ),
        ) || 0;
    } else {
      stockPayload.valuation_rate = current_valuation_rate || 0;
    }
    stockPayload.batch_no = '';
    stockPayload.balance_qty = new_quantity;
    stockPayload.balance_value =
      parseFloat(
        (stockPayload.balance_qty * stockPayload.valuation_rate).toFixed(2),
      ) || 0;
    if (payload?.salesInvoice?.posting_date) {
      stockPayload.posting_date = this.formatDateTime(
        stockPayload.posting_date,
        stockPayload.posting_time,
        timeZone,
      );
      // stockPayload.posting_date = payload.salesInvoice.posting_date;
    } else {
      stockPayload.posting_date = date;
    }
    stockPayload.posting_time = date;
    // stockPayload.voucher_type = DELIVERY_NOTE_DOCTYPE;
    stockPayload.voucher_no = payload.saleReturnName;
    stockPayload.voucher_detail_no = '';
    stockPayload.voucher_type = RETURN_DELIVERY_NOTE;
    stockPayload.outgoing_rate = 0;
    stockPayload.qty_after_transaction = stockPayload.actual_qty;
    stockPayload.warehouse = payload.salesInvoice.delivery_warehouse;
    stockPayload.stock_value =
      stockPayload.qty_after_transaction * stockPayload.valuation_rate || 0;
    stockPayload.stock_value_difference =
      stockPayload.actual_qty * stockPayload.valuation_rate || 0;
    stockPayload.company = defaultCompany;
    stockPayload.fiscal_year = fiscalYear;

    return of(stockPayload);
  }

  formatDateTime(dateString, timeString, timeZone) {
    if (!dateString) return new DateTime(timeZone).toJSDate();
    const dateParts = dateString.split('-');
    const timeParts = timeString.split(':');

    // Parse the year, month, and day parts
    const year = parseInt(dateParts[0], 10);
    const month = parseInt(dateParts[1], 10) - 1; // Months are 0-based (0 = January)
    const day = parseInt(dateParts[2], 10);

    // Parse the time parts
    const hours = parseInt(timeParts[0], 10);
    const minutes = parseInt(timeParts[1], 10);
    const seconds = parseInt(timeParts[2], 10);

    // Create a new Date object with the parsed values
    const formattedDate = new Date(year, month, day, hours, minutes, seconds);

    // Format the date in ISO 8601 format
    const isoString = formattedDate.toISOString();

    return isoString;
  }

  calculateValuationRate(
    preQty,
    incomingQty,
    incomingRate,
    preValuation,
    totalQty,
  ) {
    const result =
      (preQty * preValuation + incomingQty * incomingRate) / totalQty;
    return result.toFixed(2);
  }

  updateOutstandingAmount(invoice_name: string) {
    return forkJoin({
      headers: this.clientToken.getServiceAccountApiHeaders(),
      settings: this.settingsService.find(),
    }).pipe(
      switchMap(({ headers, settings }) => {
        if (!settings || !settings.authServerURL)
          return throwError(() => new NotImplementedException());
        const url = `${settings.authServerURL}${FRAPPE_API_SALES_INVOICE_ENDPOINT}/${invoice_name}`;
        return this.http.get(url, { headers }).pipe(
          map(res => res.data.data),
          switchMap(sales_invoice => {
            this.salesInvoiceService
              .updateOne(
                { name: invoice_name },
                {
                  $set: {
                    outstanding_amount: sales_invoice.outstanding_amount,
                  },
                },
              )
              .then(() => {})
              .catch(() => {});
            return of({ outstanding_amount: sales_invoice.outstanding_amount });
          }),
        );
      }),
    );
  }

  async updateDeliveryStatus(payload) {
    const salesInvoice = await this.salesInvoiceService.findOne({
      where: { uuid: payload.uuid },
    });
    if (!salesInvoice) {
      throw new BadRequestException('Failed to fetch Sales Invoice.');
    }
    if ([CANCELED_STATUS, REJECTED_STATUS]?.includes(salesInvoice.status)) {
      this.salesInvoiceService.updateOne(
        { uuid: payload.uuid },
        {
          $set: { delivery_status: payload.delivery_status },
        },
      );
    }
  }

  createCreditNote(
    settings: ServerSettings,
    assignPayload: CreateSalesReturnDto,
    clientHttpRequest: any,
    salesInvoice: SalesInvoice,
    items,
  ) {
    const body = this.mapCreditNote(assignPayload, salesInvoice);
    return this.http
      .post(settings.authServerURL + LIST_CREDIT_NOTE_ENDPOINT, body, {
        headers: {
          [AUTHORIZATION]:
            BEARER_HEADER_VALUE_PREFIX + clientHttpRequest.token.accessToken,
          [CONTENT_TYPE]: APPLICATION_JSON_CONTENT_TYPE,
          [ACCEPT]: APPLICATION_JSON_CONTENT_TYPE,
        },
      })
      .pipe(
        switchMap(res => {
          return of(res);
        }),
      );
  }

  mapCreditNote(
    assignPayload: CreateSalesReturnDto,
    salesInvoice: SalesInvoice,
  ) {
    // cleanup math calculations after DTO validations are added
    const body = {
      docstatus: 1,
      naming_series: DEFAULT_NAMING_SERIES.sales_return,
      customer: assignPayload.customer,
      is_return: 1,
      company: assignPayload.company,
      posting_date: assignPayload.posting_date,
      return_against: salesInvoice.name,
      posting_time: assignPayload.posting_time,
      remarks: assignPayload.remarks,
      cost_center: assignPayload.items.find(item => item).cost_center,
      territory: salesInvoice.territory,
      items: assignPayload.items.map(item => {
        return {
          item_code: item.item_code,
          qty: item.qty,
          rate: item.rate,
          amount: item.amount,
          cost_center: item.cost_center,
        };
      }),
    };
    if (assignPayload?.credit_note_items?.length) {
      body.items = assignPayload.credit_note_items;
    }
    return body;
  }

  mapCreateDeliveryNote(
    assignPayload: DeliveryNoteWebhookDto,
  ): CreateDeliveryNoteInterface {
    const deliveryNoteBody: CreateDeliveryNoteInterface = {};
    deliveryNoteBody.docstatus = 1;
    deliveryNoteBody.posting_date = assignPayload.posting_date;
    deliveryNoteBody.posting_time = assignPayload.posting_time;
    deliveryNoteBody.is_return = true;
    deliveryNoteBody.issue_credit_note = true;
    deliveryNoteBody.contact_email = assignPayload.contact_email;
    deliveryNoteBody.set_warehouse = assignPayload.set_warehouse;
    deliveryNoteBody.customer = assignPayload.customer;
    deliveryNoteBody.company = assignPayload.company;
    deliveryNoteBody.total_qty = assignPayload.total_qty;
    deliveryNoteBody.total = assignPayload.total;
    deliveryNoteBody.items = this.mapSerialsFromItem(assignPayload.items);
    return deliveryNoteBody;
  }

  mapSerialsFromItem(items: CreateDeliveryNoteItemInterface[]) {
    const itemData = [];
    items.forEach(eachItemData => {
      itemData.push({
        item_code: eachItemData.item_code,
        qty: eachItemData.qty,
        rate: eachItemData.rate,
        serial_no: eachItemData.serial_no,
        against_sales_invoice: eachItemData.against_sales_invoice,
        amount: eachItemData.amount,
        cost_center: eachItemData.cost_center,
        brand: eachItemData.brand,
      });
    });
    return itemData;
  }

  getReturnedItemsMap(
    items: CreateDeliveryNoteItemInterface[],
    sales_invoice: SalesInvoice,
  ) {
    const returnItemsMap = {};
    items.forEach(item => {
      returnItemsMap[Buffer.from(item.item_code).toString('base64')] = item.qty;
    });
    for (const key of Object.keys(returnItemsMap)) {
      sales_invoice.delivered_items_map[key] += returnItemsMap[key];
      if (sales_invoice.returned_items_map[key]) {
        sales_invoice.returned_items_map[key] += returnItemsMap[key];
      } else {
        sales_invoice.returned_items_map[key] = returnItemsMap[key];
      }
    }
    return {
      returned_items_map: sales_invoice.returned_items_map,
      delivered_items_map: sales_invoice.delivered_items_map,
    };
  }

  updateBundleItem(salesInvoice: SalesInvoice) {
    const itemCodes = [];
    salesInvoice.items.forEach(item => itemCodes.push(item.item_code));
    return from(
      this.itemService.count({
        item_code: { $in: itemCodes },
        bundle_items: { $exists: true, $ne: [] },
      }),
    )
      .pipe(
        switchMap(count => {
          if (count) {
            return forkJoin({
              updateBundle: from(
                this.salesInvoiceService.updateOne(
                  { uuid: salesInvoice.uuid },
                  {
                    $set: {
                      has_bundle_item: true,
                    },
                  },
                ),
              ),
              updateBundleMap: this.addSalesInvoiceBundleMap(salesInvoice),
            });
          }
          return of(true);
        }),
      )
      .subscribe({
        next: () => {},
        error: () => {},
      });
  }

  getErpSalesInvoice(invoice_name: string, req) {
    return this.settingsService.find().pipe(
      switchMap(settings => {
        const url = `${settings.authServerURL}${FRAPPE_API_SALES_INVOICE_ENDPOINT}/${invoice_name}`;
        return this.http.get(url, {
          headers: this.settingsService.getAuthorizationHeaders(req.token),
        });
      }),
      map(res => res.data.data),
    );
  }

  updateErpSalesInvoice(url, body, req) {
    return this.settingsService.find().pipe(
      switchMap(settings => {
        return this.http.put(`${settings.authServerURL}${url}`, body, {
          headers: this.settingsService.getAuthorizationHeaders(req.token),
        });
      }),
      map(res => res.data.data),
    );
  }

  createERPSalesInvoiceItemBody(payload: MRPRateUpdateInterface) {
    return from(
      this.itemService.findOne({ where: { item_code: payload.item_code } }),
    ).pipe(
      switchMap((item: Item) => {
        payload.mrp_sales_rate = item.mrp;
        payload.mrp_sales_amount = payload.qty * item.mrp;
        return of(payload);
      }),
    );
  }

  updateSalesInvoiceItemMRPRate(invoice_name: string, req) {
    let mrp_sales_grand_total = 0;
    return this.getErpSalesInvoice(invoice_name, req).pipe(
      switchMap((salesInvoice: SalesInvoiceDto) => {
        return from(salesInvoice.items ? salesInvoice.items : []).pipe(
          concatMap(item => {
            return this.createERPSalesInvoiceItemBody(item);
          }),
          toArray(),
        );
      }),
      switchMap(items => {
        (items ? items : []).forEach(item => {
          mrp_sales_grand_total += item.mrp_sales_amount;
        });
        return this.updateERPSalesInvoiceItems(items, req);
      }),
      switchMap(() => {
        return this.updateErpSalesInvoice(
          `${FRAPPE_API_SALES_INVOICE_ENDPOINT}/${invoice_name}`,
          { mrp_sales_grand_total },
          req,
        );
      }),
    );
  }

  updateERPSalesInvoiceItems(items: MRPRateUpdateInterface[], req) {
    return from(items).pipe(
      concatMap((item: MRPRateUpdateInterface) => {
        return this.updateErpSalesInvoice(
          `${FRAPPE_API_SALES_INVOICE_ITEM_ENDPOINT}/${item.name}`,
          {
            mrp_sales_rate: item.mrp_sales_rate,
            mrp_sales_amount: item.mrp_sales_amount,
          },
          req,
        );
      }),
      toArray(),
    );
  }

  async getPosDraft(clientHttpRequest) {
    const territory = this.getUserTerritories(clientHttpRequest);

    return await this.salesInvoiceService.findPosDrafts(territory);
  }

  getCostCenter(serial_name: string, req: any) {
    return this.settingsService.find().pipe(
      switchMap(settings => {
        return this.http.get(
          `${settings.authServerURL}${INVOICE_LIST}/${serial_name}`,
          {
            headers: this.settingsService.getAuthorizationHeaders(req.token),
          },
        );
      }),
    );
  }
}
