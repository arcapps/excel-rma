import { HttpService } from '@nestjs/axios';
import { BadRequestException, Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { DateTime } from 'luxon';
import { Observable, forkJoin, from, of, throwError } from 'rxjs';
import { catchError, concatMap, map, switchMap, toArray } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import {
  AUTHORIZATION,
  BEARER_HEADER_VALUE_PREFIX,
  DELIVERY_NOTE_DOCTYPE,
  DOC_NAMES,
  DOC_RESET_INFO,
  SALES_INVOICE_STATUS,
} from '../../../constants/app-strings';
import {
  FRAPPE_CLIENT_CANCEL,
  GET_FRAPPE_LINKED_DOCS_ENDPOINT,
} from '../../../constants/routes';
import { CustomerService } from '../../../customer/entity/customer/customer.service';
import { DocInfoInterface } from '../../../purchase-order/policies/purchase-order-policies/purchase-order-policies.service';
import { SerialNoHistoryService } from '../../../serial-no/entity/serial-no-history/serial-no-history.service';
import { SerialNoService } from '../../../serial-no/entity/serial-no/serial-no.service';
import { StockLedgerService } from '../../../stock-ledger/entity/stock-ledger/stock-ledger.service';
import { StockLedger } from '../../../stock-ledger/schema/stock-ledger.schema';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ServerSettings } from '../../../system-settings/schemas/server-settings.schema';
import { SalesInvoiceService } from '../../entity/sales-invoice/sales-invoice.service';
import { SalesInvoiceResetPoliciesService } from '../../policies/sales-invoice-reset-policies/sales-invoice-reset-policies.service';
import { SalesInvoice } from '../../schema/sales-invoice.schema';
@Injectable()
export class SalesInvoiceResetAggregateService extends AggregateRoot {
  constructor(
    private readonly salesInvoiceService: SalesInvoiceService,
    private readonly settingsService: SettingsService,
    private readonly http: HttpService,
    private readonly serialNoHistoryService: SerialNoHistoryService,
    private readonly salesResetPolicies: SalesInvoiceResetPoliciesService,
    private readonly serialNoService: SerialNoService,
    private readonly stockLedgerService: StockLedgerService,
    private readonly customerService: CustomerService,
  ) {
    super();
  }

  cancel(uuid: string, req) {
    let serverSettings;
    // eslint-disable-next-line
    // console.log('Server Settings', serverSettings);
    return from(this.salesInvoiceService.findOne({ where: { uuid } })).pipe(
      switchMap(salesInvoice => {
        if (!salesInvoice) {
          // eslint-disable-next-line
          // console.log('Sale Invoice from DB', salesInvoice);
          return throwError(
            () => new BadRequestException('Sales Invoice not found'),
          );
        }
        // eslint-disable-next-line
        // console.log(
        //   'Before salesInvoice?.delivery_note_items?.length',
        //   salesInvoice?.delivery_note_items?.length,
        // );
        if (salesInvoice?.delivery_note_items?.length)
          return this.salesResetPolicies.validateSalesInvoiceReset(
            salesInvoice,
          );
        // eslint-disable-next-line
        // console.log('After salesInvoice?.delivery_note_items?.length');
        return of(salesInvoice);
      }),
      switchMap(salesInvoice => {
        // eslint-disable-next-line
        // console.log('Sales Invoice', salesInvoice);
        return this.settingsService.find().pipe(
          switchMap(settings => {
            serverSettings = settings;
            return this.cancelERPNextSalesInvoice(salesInvoice, settings, req);
          }),
          switchMap(success => {
            return this.cancelERPNextDocs(
              { [DOC_NAMES.SALES_INVOICE]: [salesInvoice.name] },
              req,
              serverSettings,
            );
          }),
          switchMap(success => of(salesInvoice)),
        );
      }),
      switchMap(salesInvoice => {
        return this.getSalesInvoiceSerials(salesInvoice).pipe(
          switchMap((data: { serials: string[] }) => {
            if (!data || !data.serials) {
              return of(true);
            }
            return this.resetSalesInvoiceSerialState(
              salesInvoice,
              data.serials,
            );
          }),
          switchMap(success => of(salesInvoice)),
        );
      }),
      switchMap(salesInvoice => {
        return from(
          this.salesInvoiceService.updateOne(
            { name: salesInvoice.name },
            {
              $set: {
                docstatus: 2,
                status: SALES_INVOICE_STATUS.canceled,
              },
            },
          ),
        ).pipe(
          switchMap(() => {
            return of(salesInvoice);
          }),
        );
      }),
      switchMap((salesInvoice: SalesInvoice) => {
        return this.customerService
          .updateCustomerLimit(
            req,
            serverSettings,
            salesInvoice.items.map(item => ({
              brand: item.brand,
              limit: item.amount,
            })),
            salesInvoice.customer,
            salesInvoice.name,
            'Sales Invoice',
            true,
          )
          .pipe(
            map(() => salesInvoice), // Return salesInvoice after the additional code is executed
          );
      }),
      switchMap((salesInvoice: SalesInvoice) => {
        if (
          salesInvoice.delivery_note_items.length &&
          !salesInvoice.returned_items.length
        ) {
          return from(salesInvoice.delivery_note_items).pipe(
            concatMap(item => {
              return this.createStockLedgerPayload(
                {
                  warehouse: salesInvoice.delivery_warehouse,
                  deliveryNoteItem: item,
                },
                req.token,
                serverSettings,
                salesInvoice,
              ).pipe(
                switchMap(() => {
                  return from(
                    this.stockLedgerService.deleteOne({
                      voucher_no: salesInvoice.name,
                    }),
                  );
                }),
              );
            }),
            toArray(),
          );
        }

        return of(true);
      }),
      catchError(err => {
        return throwError(() => new BadRequestException(err));
      }),
    );
  }

  createStockLedgerPayload(
    payload: { warehouse: string; deliveryNoteItem },
    token,
    settings: ServerSettings,
    SalesInvoice,
  ) {
    return this.settingsService.getFiscalYear(settings).pipe(
      switchMap(fiscalYear => {
        const date = new DateTime(settings.timeZone).toJSDate();
        const stockPayload = new StockLedger();
        stockPayload.name = uuidv4();
        stockPayload.modified = date;
        stockPayload.modified_by = token.email;
        stockPayload.warehouse = payload.warehouse;
        stockPayload.item_code = payload.deliveryNoteItem.item_code;
        stockPayload.actual_qty = payload.deliveryNoteItem.qty;
        stockPayload.incoming_rate = payload.deliveryNoteItem.rate;
        stockPayload.valuation_rate = 0;
        stockPayload.batch_no = '';
        stockPayload.posting_date = date;
        stockPayload.posting_time = date;
        stockPayload.voucher_type = DELIVERY_NOTE_DOCTYPE;
        stockPayload.voucher_no = SalesInvoice.name;
        stockPayload.voucher_detail_no = '';
        stockPayload.outgoing_rate = 0;
        stockPayload.qty_after_transaction = stockPayload.actual_qty;
        stockPayload.stock_value =
          stockPayload.qty_after_transaction * stockPayload.valuation_rate || 0;
        stockPayload.stock_value_difference =
          stockPayload.actual_qty * stockPayload.valuation_rate || 0;
        stockPayload.company = settings.defaultCompany;
        stockPayload.fiscal_year = fiscalYear;
        return of(stockPayload);
      }),
    );
  }

  resetSalesInvoiceSerialState(salesInvoice: SalesInvoice, serials: string[]) {
    return forkJoin({
      resetSerialHistory: from(
        this.serialNoHistoryService.deleteMany({
          parent_document: salesInvoice.name,
        }),
      ),
      resetSerialState: from(
        this.serialNoService.updateMany(
          {
            serial_no: { $in: serials },
          },
          {
            $unset: {
              customer: '',
              'warranty.salesWarrantyDate': '',
              'warranty.soldOn': '',
              delivery_note: '',
              sales_invoice_name: '',
              sales_return_name: '',
            },
          },
        ),
      ),
    }).pipe(switchMap(success => this.setSerialWarehouseState(serials)));
  }

  getSalesInvoiceSerials(salesInvoice) {
    const returned_serials = [];
    salesInvoice.returned_items?.forEach(item =>
      item.serial_no
        ? returned_serials.push(...item.serial_no.split('\n'))
        : null,
    );
    return this.serialNoService
      .asyncAggregate([
        {
          $match: {
            $or: [
              { sales_invoice_name: salesInvoice.name },
              { serial_no: { $in: returned_serials } },
            ],
          },
        },
        {
          $group: {
            _id: 1,
            serials: {
              $push: '$serial_no',
            },
          },
        },
      ])
      .pipe(map((data: any) => (data?.length ? data[0] : undefined)));
  }

  setSerialWarehouseState(serials: string[]) {
    if (!serials?.length) {
      return of(true);
    }
    return this.serialNoHistoryService
      .asyncAggregate([
        {
          $match: {
            serial_no: {
              $in: serials,
            },
          },
        },
        {
          $group: {
            _id: '$serial_no',
            warehouse: {
              $last: '$$ROOT.transaction_to',
            },
          },
        },
        {
          $group: {
            _id: '$warehouse',
            serials: {
              $push: '$_id',
            },
          },
        },
      ])
      .pipe(
        switchMap((data: { _id: string; serials: string[] }[]) => {
          if (!data?.length) {
            return of(true);
          }
          return from(data).pipe(
            switchMap(serialData => {
              return from(
                this.serialNoService.updateMany(
                  {
                    serial_no: { $in: serialData.serials },
                  },
                  { $set: { warehouse: serialData._id } },
                ),
              );
            }),
            toArray(),
          );
        }),
        switchMap(success => of(true)),
      );
  }

  // cancelERPNextSalesInvoice(salesInvoice: SalesInvoice, settings, req) {
  //   return this.getERPNextLinkedDocs(
  //     DOC_NAMES.SALES_INVOICE,
  //     salesInvoice.name,
  //     DOC_RESET_INFO[DOC_NAMES.SALES_INVOICE],
  //     settings,
  //     req,
  //   ).pipe(
  //     switchMap(docs => {
  //       const docsToProcess = docs.message.docs;
  //       // eslint-disable-next-line no-console
  //       if (docsToProcess.length > 0) {
  //         const observables = docsToProcess.map((doc: any) => {
  //           return this.cancelDoc(doc.doctype, doc.name, settings, req).pipe(
  //             catchError(error => {
  //               // eslint-disable-next-line no-console
  //               return throwError(new BadRequestException(error));
  //             }),
  //           );
  //         });

  //         // Use forkJoin to wait for all observables to complete
  //         return forkJoin(observables);
  //       } else {
  //         return of({});
  //       }
  //     }),
  //     catchError(error => {
  //       return throwError(new BadRequestException(error));
  //     }),
  //   );
  // }
  cancelERPNextSalesInvoice(salesInvoice: SalesInvoice, settings, req) {
    return this.getERPNextLinkedDocs(
      DOC_NAMES.SALES_INVOICE,
      salesInvoice.name,
      DOC_RESET_INFO[DOC_NAMES.SALES_INVOICE],
      settings,
      req,
    ).pipe(
      switchMap(docs => {
        const docsToProcess = docs.message.docs;

        // Define a recursive function to process one document at a time
        const processNextDoc = index => {
          if (index < docsToProcess.length) {
            const doc = docsToProcess[index];
            return this.cancelDoc(doc.doctype, doc.name, settings, req).pipe(
              catchError(error => {
                return of(null);
              }),
              switchMap(() => processNextDoc(index + 1)), // Process the next document
            );
          } else {
            // All documents have been processed
            return of({});
          }
        };

        // Start processing the documents from index 0
        return processNextDoc(0);
      }),
      catchError(error => {
        // eslint-disable-next-line
        // console.log('Error', error);
        return throwError(new BadRequestException(error));
      }),
    );
  }

  cancelERPNextDocs(docs: { [key: string]: string[] }, req, settings) {
    return of({}).pipe(
      switchMap(obj => {
        return from(Object.keys(docs)).pipe(
          concatMap((docType: string) => {
            return from(docs[docType]).pipe(
              concatMap(doc => {
                return this.cancelDoc(docType, doc, settings, req);
              }),
              switchMap(success => of(true)),
            );
          }),
          catchError(err => {
            if (
              err?.response?.data?.exc &&
              err?.response?.data?.exc.includes(
                'Cannot edit cancelled document',
              )
            ) {
              return of(true);
            }
            return throwError(err);
          }),
        );
      }),
      toArray(),
    );
  }

  cancelDoc(doctype, docName, settings: ServerSettings, req) {
    const doc = {
      doctype,
      name: docName,
    };
    return this.http
      .post(settings.authServerURL + FRAPPE_CLIENT_CANCEL, doc, {
        headers: {
          [AUTHORIZATION]: BEARER_HEADER_VALUE_PREFIX + req.token.accessToken,
        },
      })
      .pipe(
        catchError(err => {
          return throwError(new BadRequestException(err));
        }),
      );
  }

  getERPNextLinkedDocs(
    docTypeName,
    docName,
    docInfo,
    settings: ServerSettings,
    clienthttpReq,
  ): Observable<{ message: { [key: string]: DocInfoInterface[] } }> {
    return this.http
      .post(
        settings.authServerURL + GET_FRAPPE_LINKED_DOCS_ENDPOINT,
        {
          doctype: docTypeName,
          name: docName,
          linkinfo: docInfo,
        },
        {
          headers: {
            [AUTHORIZATION]:
              BEARER_HEADER_VALUE_PREFIX + clienthttpReq.token.accessToken,
          },
        },
      )
      .pipe(map(data => data.data));
  }
}
