import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveDeliveryNotesQuery } from './get-delivery-notes.query';
import { SalesInvoiceAggregateService } from '../../aggregates/sales-invoice-aggregate/sales-invoice-aggregate.service';

@QueryHandler(RetrieveDeliveryNotesQuery)
export class RetrieveDeliveryNotesHandler
  implements IQueryHandler<RetrieveDeliveryNotesQuery>
{
  constructor(
    private readonly salesInvoiceAggregateService: SalesInvoiceAggregateService,
  ) {}

  async execute(query) {
    const { salesInvoiceName, req } = query;
    return this.salesInvoiceAggregateService.getDeliveryNotes(
      salesInvoiceName,
      req,
    );
  }
}
