import { Module } from '@nestjs/common';
import { CreditLimitLedgerEntitiesModule } from '../credit-limit-ledger/credit-limit-ledger-entities.module';
import { CustomerModule } from '../customer/customer.module';
import { DeliveryNoteModule } from '../delivery-note/delivery-note.module';
import { DirectModule } from '../direct/direct.module';
import { ItemModule } from '../item/item.module';
import { SerialNoModule } from '../serial-no/serial-no.module';
import { StockLedgerEntitiesModule } from '../stock-ledger/entity/entity.module';
import { SalesInvoiceAggregatesManager } from './aggregates';
import { SalesInvoiceCommandManager } from './command';
import { SalesInvoiceWebhookController } from './controllers/sales-invoice-webhook/sales-invoice-webhook.controller';
import { SalesInvoiceController } from './controllers/sales-invoice/sales-invoice.controller';
import { SalesInvoiceEntitiesModule } from './entity/entity.module';
import { SalesInvoiceEventManager } from './event';
import { SalesInvoicePoliciesManager } from './policies';
import { SalesInvoiceQueryManager } from './query';

@Module({
  imports: [
    CreditLimitLedgerEntitiesModule,
    CustomerModule,
    DeliveryNoteModule,
    DirectModule,
    ItemModule,
    SalesInvoiceEntitiesModule,
    SerialNoModule,
    StockLedgerEntitiesModule,
  ],
  controllers: [SalesInvoiceController, SalesInvoiceWebhookController],
  providers: [
    ...SalesInvoiceAggregatesManager,
    ...SalesInvoiceQueryManager,
    ...SalesInvoiceEventManager,
    ...SalesInvoiceCommandManager,
    ...SalesInvoicePoliciesManager,
  ],
  exports: [SalesInvoiceEntitiesModule],
})
export class SalesInvoiceModule {}
