import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { randomBytes } from 'crypto';
import { firstValueFrom } from 'rxjs';
// import { map } from 'rxjs/operators';
import {
  AUTH_ENDPOINT,
  PROFILE_ENDPOINT,
  REVOKE_ENDPOINT,
  SCOPE,
  TOKEN_ENDPOINT,
} from '../../../constants/app-strings';
import { settingsAlreadyExists } from '../../../constants/exceptions';
// import { GET_TIME_ZONE_ENDPOINT } from '../../../constants/routes';
import { ServerSettings } from '../../../system-settings/schemas/server-settings.schema';
import { SettingsService } from '../../aggregates/settings/settings.service';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class SetupService {
  constructor(
    @InjectModel('ServerSettings')
    private serverSettingsModel: Model<ServerSettings>,
    protected readonly settingsService: SettingsService,
    protected readonly http: HttpService,
  ) {}

  async setup(params) {
    if (await firstValueFrom(this.settingsService.find())) {
      throw settingsAlreadyExists;
    }

    // const settings = new ServerSettings();
    const settings: any = {};
    Object.assign(settings, params);
    settings.profileURL = settings.authServerURL + PROFILE_ENDPOINT;
    settings.authorizationURL = settings.authServerURL + AUTH_ENDPOINT;
    settings.tokenURL = settings.authServerURL + TOKEN_ENDPOINT;
    settings.revocationURL = settings.authServerURL + REVOKE_ENDPOINT;
    settings.scope = SCOPE.split(' ');
    // settings.frontendCallbackURLs = [settings.appURL];
    // settings.backendCallbackURLs = [settings.appURL];

    settings.webhookApiKey = randomBytes(64).toString('hex');
    // const timeZone = await firstValueFrom(
    //   this.http
    //     .get(settings.authServerURL + GET_TIME_ZONE_ENDPOINT)
    //     .pipe(map(resTZ => resTZ.data.message.time_zone)),
    // );
    // settings.timeZone = timeZone;
    const createdService = new this.serverSettingsModel(settings);

    return await createdService.save();
  }

  async getInfo() {
    const info: any = await firstValueFrom(this.settingsService.find());
    if (info) {
      info._id = undefined;
      info.serviceAccountUser = undefined;
      info.serviceAccountSecret = undefined;
      info.serviceAccountApiSecret = undefined;
      info.webhookApiKey = undefined;
    }
    return info;
  }
}
