import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Supplier, SupplierDocument } from '../../schema/supplier.schema';
import { PARSE_REGEX } from '../../../constants/app-strings';

@Injectable()
export class SupplierService {
  constructor(
    @InjectModel('Supplier') private supplierModel: Model<SupplierDocument>,
  ) {}

  async find(query?) {
    return await this.supplierModel.find(query);
  }

  async create(supplierPayload: Supplier) {
    return await new this.supplierModel(supplierPayload).save();
  }

  async findOne(options): Promise<Supplier> {
    return await this.supplierModel.findOne(options.where);
  }

  async list(skip, take, search, sort) {
    const sortQuery = { name: sort };

    const columns = Object.keys(this.supplierModel.schema.paths);

    const $or = this.handleSearchPatterns(columns, search);
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };
    const results = await this.supplierModel
      .find(where)
      .skip(skip)
      .limit(take)
      .sort(sortQuery);

    return {
      docs: results || [],
      length: await this.supplierModel.count(where),
      offset: skip,
    };
  }

  handleSearchPatterns(columns, search) {
    const searchQuery: any[] = [];

    columns.forEach(field => {
      const fieldType = this.supplierModel.schema.paths[field].instance;
      const filter = {};

      if (fieldType === 'String') {
        filter[field] = { $regex: PARSE_REGEX(search), $options: 'i' };
      } else if (fieldType === 'Number') {
        const numericSearch = parseFloat(search);
        if (!isNaN(numericSearch)) {
          filter[field] = numericSearch;
        }
      } else if (fieldType === 'Array') {
        const arraySearch = Array.isArray(search) ? search : [search];
        filter[field] = { $in: arraySearch };
      }

      // Check if the filter object is not empty before pushing it into the array
      if (Object.keys(filter).length > 0) {
        searchQuery.push(filter);
      }
    });

    return searchQuery;
  }

  async deleteOne(query, options?): Promise<any> {
    return await this.supplierModel.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.supplierModel.updateOne(query, options);
  }

  async count(query) {
    await this.supplierModel.count(query);
  }
}
