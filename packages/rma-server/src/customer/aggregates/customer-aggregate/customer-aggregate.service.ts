import { HttpService } from '@nestjs/axios';
import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { forkJoin } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { MongoFindOneOptions } from 'typeorm/find-options/mongodb/MongoFindOneOptions';
import { v4 as uuidv4 } from 'uuid';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import {
  FRAPPE_API_GET_CUSTOMER_ENDPOINT,
  FRAPPE_API_GET_USER_INFO_ENDPOINT,
} from '../../../constants/routes';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { CustomerDto } from '../../entity/customer/customer-dto';
import { CustomerService } from '../../entity/customer/customer.service';
import { CustomerAddedEvent } from '../../event/customer-added/customer-added.event';
import { CustomerRemovedEvent } from '../../event/customer-removed/customer-removed.event';
import { CustomerUpdatedEvent } from '../../event/customer-updated/customer-updated.event';
import { Customer } from '../../schemas/customer.schema';

@Injectable()
export class CustomerAggregateService extends AggregateRoot {
  constructor(
    private readonly customerService: CustomerService,
    private readonly http: HttpService,
    private readonly clientToken: ClientTokenManagerService,
    private readonly settings: SettingsService,
  ) {
    super();
  }

  addCustomer(customerPayload: CustomerDto, clientHttpRequest) {
    const customer = new Customer();
    customer.uuid = uuidv4();
    Object.assign(customer, customerPayload);
    this.apply(new CustomerAddedEvent(customer, clientHttpRequest));
  }

  async retrieveCustomer(options: MongoFindOneOptions<Customer>, req) {
    const customer = await this.customerService.findOne(options);
    if (!customer) throw new NotFoundException();
    return customer;
  }

  async getCustomerList(offset, limit, search, sort, clientHttpRequest) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }

    return await this.customerService.list(
      offset,
      limit,
      search,
      sort,
      clientHttpRequest.token.territory,
    );
  }

  async removeCustomer(uuid: string) {
    const customerFound = await this.customerService.findOne({
      where: { uuid },
    });

    if (!customerFound) {
      throw new NotFoundException();
    }

    this.apply(new CustomerRemovedEvent(customerFound));
  }

  async updateCustomer(updatePayload) {
    if (updatePayload.tempCreditLimitPeriod) {
      updatePayload.tempCreditLimitPeriod = new Date(
        updatePayload.tempCreditLimitPeriod,
      );
      updatePayload.tempCreditLimitPeriod.setDate(
        updatePayload.tempCreditLimitPeriod.getDate() + 1,
      );
    }

    const customer = await this.customerService.findOne({
      where: { uuid: updatePayload.uuid },
    });

    if (!customer) {
      throw new NotFoundException();
    }
    const customerPayload = Object.assign(customer, updatePayload);
    this.apply(new CustomerUpdatedEvent(customerPayload));
  }

  getUserDetails(email: string) {
    return forkJoin({
      headers: this.clientToken.getServiceAccountApiHeaders(),
      settings: this.settings.find(),
    }).pipe(
      switchMap(({ headers, settings }) => {
        return this.http
          .get(
            settings.authServerURL + FRAPPE_API_GET_USER_INFO_ENDPOINT + email,
            { headers },
          )
          .pipe(map(res => res.data.data));
      }),
    );
  }

  async getBrandsLimits(customer_name: string) {
    return forkJoin({
      headers: this.clientToken.getServiceAccountApiHeaders(),
      settings: this.settings.find(),
    }).pipe(
      switchMap(({ headers, settings }) => {
        const url =
          settings.authServerURL +
          FRAPPE_API_GET_CUSTOMER_ENDPOINT +
          `/${customer_name}`;
        return this.http
          .get(url, {
            headers,
          })
          .pipe(
            map(res => {
              if (!res) throw new NotFoundException();
              return {
                totalCreditLimit: res.data.data.credit_limits.length
                  ? res.data.data.credit_limits[0].credit_limit
                  : '',
                brandsLimit: res.data.data.custom_brand_wise_allocations,
                othersBrandsLimit: res.data.data.custom_other_brands_limit,
              };
            }),
          );
      }),
    );
  }

  relayListCustomers(query) {
    return forkJoin({
      headers: this.clientToken.getServiceAccountApiHeaders(),
      settings: this.settings.find(),
    }).pipe(
      switchMap(({ headers, settings }) => {
        const url = settings.authServerURL + FRAPPE_API_GET_CUSTOMER_ENDPOINT;
        return this.http
          .get(url, {
            headers,
            params: query,
          })
          .pipe(map(res => res.data));
      }),
    );
  }
  relayCustomer(name) {
    return forkJoin({
      headers: this.clientToken.getServiceAccountApiHeaders(),
      settings: this.settings.find(),
    }).pipe(
      switchMap(({ headers, settings }) => {
        const url = `${settings.authServerURL}${FRAPPE_API_GET_CUSTOMER_ENDPOINT}/${name}`;
        return this.http
          .get(url, {
            headers,
          })
          .pipe(map(res => res.data));
      }),
    );
  }
}
