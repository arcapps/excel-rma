import { Type } from 'class-transformer';
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';

export class BrandWiseCreditLimitDto {
  @IsNotEmpty()
  @IsString()
  brand: string;

  @IsNotEmpty()
  @IsNumber()
  limit: number;
}

export class CreditLimitsDto {
  @IsNotEmpty()
  @IsString()
  company: string;

  @IsNotEmpty()
  @IsNumber()
  credit_limit: number;
}

export class CustomerWebhookDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  owner: string;

  @IsString()
  @IsNotEmpty()
  customer_name: string;

  @IsString()
  @IsNotEmpty()
  customer_type: string;

  @IsString()
  @IsOptional()
  gst_category: string;

  @IsString()
  @IsNotEmpty()
  customer_group: string;

  @IsString()
  @IsNotEmpty()
  territory: string;

  @IsOptional()
  @IsString()
  payment_terms: string;

  @IsOptional()
  @ValidateNested()
  @Type(() => CreditLimitsDto)
  credit_limits: CreditLimitsDto[];

  @IsOptional()
  sales_team: any[];

  @IsOptional()
  excel_remaining_balance: number;

  @IsOptional()
  custom_brand_wise_allocations: BrandWiseCreditLimitDto[];

  @IsOptional()
  custom_other_brands_limit: number;

  @IsOptional()
  custom_source: string | null;

  isSynced?: boolean;
  credit_days?: number;

  @IsOptional()
  @IsString()
  mobile_no: string;
}

export interface PaymentTemplateTermsInterface {
  invoice_portion: number;
  credit_days: number;
  credit_months: number;
}
