export enum WarrantyClaimStatus {
  OPEN = 'open',
  CLOSE = 'close',
}
