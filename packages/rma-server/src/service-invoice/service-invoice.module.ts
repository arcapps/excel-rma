import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { WarrantyClaimEntitiesModule } from '../warranty-claim/entity/entity.module';
import { ServiceInvoiceAggregatesManager } from './aggregates';
import { ServiceInvoiceCommandManager } from './command';
import { ServiceInvoiceController } from './controllers/service-invoice/service-invoice.controller';
import { ServiceInvoiceEntitiesModule } from './entity/entity.module';
import { ServiceInvoiceEventManager } from './event';
import { ServiceInvoicePoliciesService } from './policies/service-invoice-policies/service-invoice-policies.service';
import { ServiceInvoiceQueryManager } from './query';
import { CreditLimitLedgerModule } from '../credit-limit-ledger/credit-limit-ledger.module';
import { CustomerModule } from '../customer/customer.module';

@Module({
  imports: [
    ServiceInvoiceEntitiesModule,
    WarrantyClaimEntitiesModule,
    CqrsModule,
    HttpModule,
    CreditLimitLedgerModule,
    CustomerModule,
  ],
  controllers: [ServiceInvoiceController],
  providers: [
    ...ServiceInvoiceAggregatesManager,
    ...ServiceInvoiceQueryManager,
    ...ServiceInvoiceEventManager,
    ...ServiceInvoiceCommandManager,
    ServiceInvoicePoliciesService,
  ],
  exports: [ServiceInvoiceEntitiesModule],
})
export class ServiceInvoiceModule {}
