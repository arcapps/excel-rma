import { HttpService } from '@nestjs/axios';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
  NotImplementedException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { forkJoin, from, of, throwError } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import {
  ACCEPT,
  APPLICATION_JSON_CONTENT_TYPE,
  AUTHORIZATION,
  BEARER_HEADER_VALUE_PREFIX,
  CONTENT_TYPE,
  DEFAULT_NAMING_SERIES,
} from '../../../constants/app-strings';
import { FRAPPE_API_SALES_INVOICE_ENDPOINT } from '../../../constants/routes';
import { CustomerService } from '../../../customer/entity/customer/customer.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { WarrantyClaimService } from '../../../warranty-claim/entity/warranty-claim/warranty-claim.service';
import { ServiceInvoiceDto } from '../../entity/service-invoice/service-invoice-dto';
import { ServiceInvoiceService } from '../../entity/service-invoice/service-invoice.service';
import { UpdateServiceInvoiceDto } from '../../entity/service-invoice/update-service-invoice-dto';
import { ServiceInvoiceRemovedEvent } from '../../event/service-invoice-removed/service-invoice-removed.event';
import { ServiceInvoiceUpdatedEvent } from '../../event/service-invoice-updated/service-invoice-updated.event';
import { ServiceInvoice } from '../../schema/service-invoice.schema';

@Injectable()
export class ServiceInvoiceAggregateService extends AggregateRoot {
  constructor(
    private readonly clientToken: ClientTokenManagerService,
    private readonly serviceInvoiceService: ServiceInvoiceService,
    private readonly settings: SettingsService,
    private readonly http: HttpService,
    private readonly warrantyAggregateService: WarrantyClaimService,
    private readonly customerService: CustomerService,
  ) {
    super();
  }

  assignServiceInvoiceFields(
    serviceInvoicePayload: ServiceInvoiceDto,
    clientHttpRequest,
  ) {
    const serviceInvoice = new ServiceInvoice();
    Object.assign(serviceInvoice, serviceInvoicePayload);
    serviceInvoice.uuid = uuidv4();
    serviceInvoice.created_by = clientHttpRequest.token.fullName;
    serviceInvoice.invoice_no = serviceInvoicePayload.name;
    return of(serviceInvoice);
  }

  addServiceInvoice(serviceInvoice: ServiceInvoiceDto, clientHttpRequest) {
    let serverSettings;
    let invoiceName;
    return this.settings.find().pipe(
      switchMap(settings => {
        serverSettings = settings;
        const URL = `${settings.authServerURL}${FRAPPE_API_SALES_INVOICE_ENDPOINT}`;
        serviceInvoice.naming_series = DEFAULT_NAMING_SERIES.service_invoice;
        const body = serviceInvoice;
        body.territory = body.branch;
        return this.http.post(URL, body, {
          headers: {
            [AUTHORIZATION]:
              BEARER_HEADER_VALUE_PREFIX + clientHttpRequest.token.accessToken,
            [CONTENT_TYPE]: APPLICATION_JSON_CONTENT_TYPE,
            [ACCEPT]: APPLICATION_JSON_CONTENT_TYPE,
          },
        });
      }),
      map(data => data.data.data),
      switchMap((res: ServiceInvoiceDto) => {
        invoiceName = res.name;
        Object.assign(res, serviceInvoice);
        return this.assignServiceInvoiceFields(res, clientHttpRequest);
      }),
      switchMap(data => {
        return from(this.serviceInvoiceService.create(data));
      }),
      switchMap(() => {
        return this.serviceInvoiceService.asyncAggregate([
          { $match: { warrantyClaimUuid: serviceInvoice.warrantyClaimUuid } },
          { $unwind: '$items' },
          {
            $project: {
              invoice_no: 1,
              total: '$items.amount',
              item_name: '$items.item_name',
            },
          },
          {
            $group: {
              _id: '',
              service_items: { $push: '$item_name' },
              service_invoices: { $push: '$invoice_no' },
              total: { $sum: '$total' },
            },
          },
          {
            $project: {
              service_items: '$service_items',
              service_invoices: '$service_invoices',
              total: '$total',
            },
          },
        ]);
      }),
      switchMap((res: any) => {
        return this.warrantyAggregateService.updateOne(
          { uuid: serviceInvoice.warrantyClaimUuid },
          {
            $set: {
              service_items: res[0].service_items,
              service_vouchers: res[0].service_invoices,
              billed_amount: res[0].total,
            },
          },
        );
      }),
      switchMap(() => {
        if (serviceInvoice.status === 'Paid') return of(null);

        return this.customerService.updateCustomerLimit(
          clientHttpRequest,
          serverSettings,
          serviceInvoice.items.map(item => ({
            brand: item.brand,
            limit: item.amount,
          })),
          serviceInvoice.customer,
          invoiceName,
          'Service Invoice',
          false,
        );
      }),
      catchError(err => {
        const errs = err?.response?.data?.exception?.split(':');
        return throwError(
          () =>
            new BadRequestException(
              errs ? errs[errs?.length - 1]?.trim() : err,
            ),
        );
      }),
    );
  }

  mapBulkClaimPayload(warrantyClaim, serviceInvoice) {
    const payload = serviceInvoice.items.map(item => {
      return {
        brand: warrantyClaim.product_brand,
        amount: item.amount,
      };
    });

    return payload;
  }

  async retrieveServiceInvoice(uuid: string, req) {
    const provider = await this.serviceInvoiceService.findOne({
      where: { uuid },
    });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getServiceInvoiceList(offset, limit, sort, search, clientHttpRequest) {
    return await this.serviceInvoiceService.list(offset, limit, search, sort);
  }

  async remove(uuid: string, req) {
    const found = await this.serviceInvoiceService.findOne({ where: { uuid } });
    if (!found) {
      throw new NotFoundException();
    }

    const warrantyClaim = await this.warrantyAggregateService.findOne({
      where: { uuid: found.warrantyClaimUuid },
    });

    const limitsPayload = [
      {
        brand: warrantyClaim.product_brand,
        limit: found.total,
      },
    ];

    this.settings
      .find()
      .pipe(
        switchMap(settings => {
          return this.customerService.updateCustomerLimit(
            req,
            settings,
            limitsPayload,
            found.customer_name,
            found.name,
            'Service Invoice',
            true,
          );
        }),
      )
      .subscribe();

    this.apply(new ServiceInvoiceRemovedEvent(found));
  }

  async update(updatePayload: UpdateServiceInvoiceDto) {
    const provider = await this.serviceInvoiceService.findOne({
      where: { uuid: updatePayload.uuid },
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new ServiceInvoiceUpdatedEvent(update));
  }

  syncWithERP(invoice_no: string) {
    return forkJoin({
      headers: this.clientToken.getServiceAccountApiHeaders(),
      settings: this.settings.find(),
    }).pipe(
      switchMap(({ headers, settings }) => {
        if (!settings || !settings.authServerURL) {
          return throwError(() => new NotImplementedException());
        }

        const url = `${settings.authServerURL}${FRAPPE_API_SALES_INVOICE_ENDPOINT}/${invoice_no}`;
        return this.http.get(url, { headers }).pipe(
          map(res => res.data.data),
          switchMap(service_invoice => {
            this.serviceInvoiceService.updateOne(
              { invoice_no },
              {
                $set: {
                  docstatus: service_invoice.docstatus,
                  outstanding_amount: service_invoice.outstanding_amount,
                },
              },
            );

            return of({
              docstatus: service_invoice.docstatus,
              outstanding_amount: service_invoice.outstanding_amount,
            });
          }),
        );
      }),
    );
  }
}
