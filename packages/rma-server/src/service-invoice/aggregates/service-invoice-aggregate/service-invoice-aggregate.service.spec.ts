import { Test, TestingModule } from '@nestjs/testing';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ServiceInvoiceService } from '../../entity/service-invoice/service-invoice.service';
import { ServiceInvoiceAggregateService } from './service-invoice-aggregate.service';

import { HttpService } from '@nestjs/axios';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import { CustomerService } from '../../../customer/entity/customer/customer.service';
import { WarrantyClaimService } from '../../../warranty-claim/entity/warranty-claim/warranty-claim.service';

describe('ServiceInvoiceAggregateService', () => {
  let service: ServiceInvoiceAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ServiceInvoiceAggregateService,
        {
          provide: CustomerService,
          useValue: {},
        },
        {
          provide: ServiceInvoiceService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: WarrantyClaimService,
          useValue: {},
        },
        {
          provide: ClientTokenManagerService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ServiceInvoiceAggregateService>(
      ServiceInvoiceAggregateService,
    );
  });
  ServiceInvoiceAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
