import { Module, Global } from '@nestjs/common';
import { RoleGuard } from './guards/role.guard';
import { TokenGuard } from './guards/token.guard';
import { AuthControllers } from './controllers';
import { AuthAggregates } from './aggregates';
import { TokenCacheService } from './entities/token-cache/token-cache.service';
import { AuthSchedulers } from './schedulers';
import { CustomerEntitiesModule } from '../customer/entity/entity.module';
import { FrappeWebhookPipe } from './guards/webhook.pipe';
import { MongooseModule } from '@nestjs/mongoose';
import { TokenCacheSchema } from './schemas/tokenCache.schema';

@Global()
@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'TokenCache', schema: TokenCacheSchema },
    ]),
    CustomerEntitiesModule,
  ],
  providers: [
    TokenCacheService,
    RoleGuard,
    TokenGuard,
    FrappeWebhookPipe,
    ...AuthSchedulers,
    ...AuthAggregates,
  ],
  exports: [
    TokenCacheService,
    RoleGuard,
    TokenGuard,
    FrappeWebhookPipe,
    ...AuthAggregates,
  ],
  controllers: [...AuthControllers],
})
export class AuthModule {}
