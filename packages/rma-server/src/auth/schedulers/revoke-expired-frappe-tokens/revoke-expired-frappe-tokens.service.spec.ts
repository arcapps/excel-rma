import { HttpService } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { TokenCacheService } from '../../../auth/entities/token-cache/token-cache.service';
import { ErrorLogService } from '../../../error-log/error-log-service/error-log.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { AGENDA_TOKEN } from '../../../system-settings/providers/agenda.provider';
import { ClientTokenManagerService } from '../../aggregates/client-token-manager/client-token-manager.service';
import { RevokeExpiredFrappeTokensService } from './revoke-expired-frappe-tokens.service';

describe('RevokeExpiredFrappeTokensService', () => {
  let service: RevokeExpiredFrappeTokensService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RevokeExpiredFrappeTokensService,
        { provide: ServerSettingsService, useValue: {} },
        { provide: ClientTokenManagerService, useValue: {} },
        { provide: HttpService, useValue: {} },
        { provide: ErrorLogService, useValue: {} },
        { provide: TokenCacheService, useValue: {} },
        { provide: AGENDA_TOKEN, useValue: {} },
      ],
    }).compile();

    service = module.get<RevokeExpiredFrappeTokensService>(
      RevokeExpiredFrappeTokensService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
