import { IEvent } from '@nestjs/cqrs';
import { PurchaseInvoice } from '../../schemas/purchase-invoice.schema';

export class PurchaseInvoiceRemovedEvent implements IEvent {
  constructor(public purchaseInvoice: PurchaseInvoice) {}
}
