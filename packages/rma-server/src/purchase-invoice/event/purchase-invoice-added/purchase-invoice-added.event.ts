import { IEvent } from '@nestjs/cqrs';
import { PurchaseInvoice } from '../../schemas/purchase-invoice.schema';

export class PurchaseInvoiceAddedEvent implements IEvent {
  constructor(
    public purchaseInvoice: PurchaseInvoice,
    public clientHttpRequest: any,
  ) {}
}
