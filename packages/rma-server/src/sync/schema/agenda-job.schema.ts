import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';
import { TokenCache } from '../../auth/schemas/tokenCache.schema';
import { ServerSettings } from '../../system-settings/schemas/server-settings.schema';

export type AgendaJobsDocument = HydratedDocument<AgendaJobs>;

export class DataImportSuccessResponse {
  dataImportName?: string;
  file_name?: string;
  file_url?: string;
}

export class JobData {
  payload: any;
  settings: ServerSettings;
  type: string;
  parent: string;
  token: TokenCache;
  uuid: string;
  exported?: boolean;
  status?: string;
  lastError?: any;
  dataImport?: DataImportSuccessResponse;
}

@Schema({ collection: 'agendaJobs' })
export class AgendaJobs {
  @Prop()
  _id: mongoose.Schema.Types.ObjectId;

  @Prop()
  name: string;

  @Prop()
  type: string;

  @Prop()
  data: JobData;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  failedAt: any;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  failCount: any;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  failReason: any;

  @Prop()
  lastModifiedBy: Date;

  @Prop()
  nextRunAt: Date;

  @Prop()
  priority: number;

  @Prop()
  repeatInterval: string;

  @Prop()
  repeatTimezone: string;

  @Prop()
  lockedAt: Date;

  @Prop()
  lastRunAt: Date;

  @Prop()
  lastFinishedAt: Date;
}

export const AgendaJobsSchema = SchemaFactory.createForClass(AgendaJobs);
