import { PurchaseOrder } from './purchase-order.entity';
import { Injectable } from '@nestjs/common';
import { PARSE_REGEX } from '../../../constants/app-strings';
import { InjectModel } from '@nestjs/mongoose';
import { PurchaseOrderDocument } from '../../schema/purchase-order.schema';
import { Model } from 'mongoose';

@Injectable()
export class PurchaseOrderService {
  constructor(
    // @InjectRepository(PurchaseOrder)
    // private readonly purchaseOrderRepository: MongoRepository<PurchaseOrder>,
    @InjectModel('PurchaseOrder')
    private purchaseOrderModel: Model<PurchaseOrderDocument>,
  ) {}

  async find(query?) {
    return await this.purchaseOrderModel.find(query);
  }

  async create(purchaseOrder: PurchaseOrder) {
    return await new this.purchaseOrderModel(purchaseOrder).save();
  }

  async findOne(options) {
    return await this.purchaseOrderModel.findOne(options.where);
  }

  async list(skip, take, sort, filter_query?) {
    let sortQuery;

    try {
      sortQuery = JSON.parse(sort);
    } catch (error) {
      sortQuery = { created_on: 'desc' };
    }

    for (const key of Object.keys(sortQuery)) {
      sortQuery[key] = sortQuery[key].toUpperCase();
      if (!sortQuery[key]) {
        delete sortQuery[key];
      }
    }

    const $and: any[] = [filter_query ? this.getFilterQuery(filter_query) : {}];

    const where: { $and: any } = { $and };

    const results = await this.purchaseOrderModel
      .find(where)
      .skip(skip)
      .limit(take)
      .sort(sortQuery);
    return {
      docs: results || [],
      length: await this.purchaseOrderModel.count(where),
      offset: skip,
    };
  }

  getFilterQuery(query) {
    const keys = Object.keys(query);
    keys.forEach(key => {
      if (query[key]) {
        if (key === 'status' && query[key] === 'All') {
          delete query[key];
        } else {
          if (typeof query[key] === 'string') {
            query[key] = { $regex: PARSE_REGEX(query[key]), $options: 'i' };
          } else {
            delete query[key];
          }
        }
      } else {
        delete query[key];
      }
    });
    return query;
  }

  async deleteOne(query, options?): Promise<any> {
    return await this.purchaseOrderModel.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.purchaseOrderModel.updateOne(query, options);
  }

  async insertMany(query, options?) {
    return await this.purchaseOrderModel.insertMany(query, options);
  }
}
