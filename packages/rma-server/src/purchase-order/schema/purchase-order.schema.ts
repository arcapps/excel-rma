import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { PurchaseOrderItem } from '../entity/purchase-order/purchase-order-item.interface';
import { PaymentSchedule } from '../entity/purchase-order/po-payment-schedule.interface';
import { v4 as uuidv4 } from 'uuid';

export type PurchaseOrderDocument = HydratedDocument<PurchaseOrder>;

@Schema({ collection: 'purchase_order' })
export class PurchaseOrder {
  @Prop({ index: true })
  uuid: string;

  @Prop({ index: true })
  name: string;
  @Prop()
  owner: string;
  @Prop()
  creation: string;
  @Prop()
  modified: string;
  @Prop()
  modified_by: string;
  @Prop()
  idx: number;
  @Prop()
  docstatus: number;
  @Prop()
  title: string;
  @Prop()
  naming_series: string;
  @Prop()
  supplier: string;
  @Prop()
  supplier_name: string;
  @Prop()
  company: string;
  @Prop()
  transaction_date: string;
  @Prop()
  schedule_date: string;
  @Prop()
  supplier_address: string;
  @Prop()
  address_display: string;
  @Prop()
  currency: string;
  @Prop()
  conversion_rate: number;
  @Prop()
  buying_price_list: string;
  @Prop()
  price_list_currency: string;
  @Prop()
  plc_conversion_rate: number;
  @Prop()
  ignore_pricing_rule: number;
  @Prop()
  is_subcontracted: string;
  @Prop()
  total_qty: number;
  @Prop()
  base_total: number;
  @Prop()
  base_net_total: number;
  @Prop()
  total: number;
  @Prop()
  net_total: number;
  @Prop()
  total_net_weight: number;
  @Prop()
  base_taxes_and_charges_added: number;
  @Prop()
  base_taxes_and_charges_deducted: number;
  @Prop()
  base_total_taxes_and_charges: number;
  @Prop()
  taxes_and_charges_added: number;
  @Prop()
  taxes_and_charges_deducted: number;
  @Prop()
  total_taxes_and_charges: number;
  @Prop()
  apply_discount_on: string;
  @Prop()
  base_discount_amount: number;
  @Prop()
  additional_discount_percentage: number;
  @Prop()
  discount_amount: number;
  @Prop()
  base_grand_total: number;
  @Prop()
  base_rounding_adjustment: number;
  @Prop()
  base_in_words: string;
  @Prop()
  base_rounded_total: number;
  @Prop()
  grand_total: number;
  @Prop()
  rounding_adjustment: number;
  @Prop()
  rounded_total: number;
  @Prop()
  disable_rounded_total: number;
  @Prop()
  in_words: string;
  @Prop()
  advance_paid: number;
  @Prop()
  terms: string;
  @Prop()
  status: string;
  @Prop()
  party_account_currency: string;
  @Prop()
  per_received: number;
  @Prop()
  per_billed: number;
  @Prop()
  group_same_items: number;
  @Prop()
  language: string;
  @Prop()
  doctype: string;
  @Prop()
  purchase_invoice_name: string;
  @Prop()
  items: PurchaseOrderItem[];
  @Prop()
  pricing_rules: any[];
  @Prop()
  supplied_items: any[];
  @Prop()
  taxes: any[];
  @Prop()
  payment_schedule: PaymentSchedule[];
  @Prop()
  isSynced: boolean;
  @Prop()
  inQueue: boolean;
  @Prop()
  submitted: boolean;
  @Prop()
  created_on: Date;
  @Prop()
  created_by: string;
  @Prop()
  update_stock: boolean;
  constructor() {
    this.uuid = uuidv4();
  }
}

export class PurchaseReceiptMetaData {
  purchase_document_type: string;
  purchase_document_no: string;
  purchase_invoice_name: string;
  amount: number;
  cost_center: string;
  expense_account: string;
  item_code: string;
  item_name: string;
  name: string;
  qty: number;
  rate: number;
  serial_no: string[];
  warehouse: string;
  deliveredBy: string;
  deliveredByEmail: string;
}

export class PurchaseInvoicePaymentSchedule {
  name: string;
  due_date: string;
  invoice_portion: number;
  payment_amount: number;
}

export class PurchaseInvoiceAdvances {
  name: string;
  parenttype: string;
  reference_type: string;
  reference_name: string;
  reference_row: string;
  advance_amount: number;
  allocated_amount: number;
}

export class PurchaseInvoiceItem {
  name: string;
  item_code: string;
  item_name: string;
  description: string;
  item_group: string;
  image: string;
  warehouse: string;
  serial_no: string;
  has_serial_no: number;
  expense_account: string;
  cost_center: string;
  received_qty: number;
  qty: number;
  rejected_qty: number;
  rate: number;
  amount: number;
}

export const PurchaseOrderSchema = SchemaFactory.createForClass(PurchaseOrder);
