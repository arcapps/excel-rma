import { HttpService } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import { DeliveryNoteService } from '../../entity/delivery-note-service/delivery-note.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { DNHookAggServ } from './delivery-note-webhook-aggregate.service';

describe('DeliveryNoteWebhookAggregateService', () => {
  let service: DNHookAggServ;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DNHookAggServ,
        {
          provide: DeliveryNoteService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: ClientTokenManagerService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<DNHookAggServ>(DNHookAggServ);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
