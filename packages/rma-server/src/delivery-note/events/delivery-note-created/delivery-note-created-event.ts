import { IEvent } from '@nestjs/cqrs';
import { DeliveryNote } from '../../schema/delivery-note.schema';

export class DeliveryNoteCreatedEvent implements IEvent {
  constructor(public payload: DeliveryNote) {}
}
