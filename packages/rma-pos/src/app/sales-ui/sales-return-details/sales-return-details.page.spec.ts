import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { MaterialModule } from '../../material/material.module';
import { SalesReturnService } from '../view-sales-invoice/sales-return/sales-return.service';
import { SalesReturnDetailsPage } from './sales-return-details.page';

@Pipe({ name: 'curFormat' })
class MockPipe implements PipeTransform {
  transform(value: string) {
    return value;
  }
}

describe('SalesReturnDetailsPage', () => {
  let component: SalesReturnDetailsPage;
  let fixture: ComponentFixture<SalesReturnDetailsPage>;
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [SalesReturnDetailsPage, MockPipe],
        imports: [
          RouterTestingModule.withRoutes([]),
          MaterialModule,
          BrowserAnimationsModule,
        ],
        providers: [
          {
            provide: SalesReturnService,
            useValue: {
              getSalesReturn: (...args) => of({ items: [] }),
              getStore: () => ({
                getItem: (...args) => Promise.resolve('Item'),
                getItems: (...args) => Promise.resolve({}),
              }),
            },
          },
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();

      fixture = TestBed.createComponent(SalesReturnDetailsPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
