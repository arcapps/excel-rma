import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  startWith,
  switchMap,
} from 'rxjs/operators';
import { ValidateInputSelected } from '../../common/pipes/validators';
import { MY_FORMATS } from '../../constants/date-format';
import { PurchaseService } from '../services/purchase.service';
import { PurchaseInvoiceDataSource } from './purchase-invoice-datasource';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.page.html',
  styleUrls: ['./purchase.page.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class PurchasePage implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: PurchaseInvoiceDataSource;
  displayedColumns = [
    'sr_no',
    'purchase_invoice_number',
    'status',
    'delivered_percent',
    'date',
    'supplier',
    'total',
    'created_by',
    'delivered_by',
  ];
  invoiceStatus: string[] = ['Completed', 'Canceled', 'Submitted', 'All'];
  status: string = 'All';
  total: number = 0;
  sortQuery: any = {};
  purchaseForm: UntypedFormGroup = new UntypedFormGroup({
    invoice_number: new UntypedFormControl(),
    supplier: new UntypedFormControl(),
    status: new UntypedFormControl(),
    start_date: new UntypedFormControl(),
    end_date: new UntypedFormControl(),
  });
  validateInput: any = ValidateInputSelected;
  filteredSupplierList: any[];

  //  get controls
  get f() {
    return this.purchaseForm.controls;
  }

  constructor(
    private readonly location: Location,
    private readonly purchaseService: PurchaseService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(() => {
      this.paginator.firstPage();
    });
    this.dataSource = new PurchaseInvoiceDataSource(this.purchaseService);
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map((event: any) => {
          if (event.url === '/purchase') {
            this.dataSource.loadItems(undefined, undefined, undefined, {});
          }
          return event;
        }),
      )
      .subscribe({
        next: () => {
          this.getTotal();
        },
        error: () => {},
      });

    this.purchaseForm
      .get('supplier')
      .valueChanges.pipe(
        startWith(''),
        distinctUntilChanged(),
        debounceTime(500),
        switchMap(value => {
          return this.purchaseService.getSupplierList(value);
        }),
      )
      .subscribe(res => (this.filteredSupplierList = res));
  }

  getUpdate(event: any) {
    const query: any = {};
    if (this.f.supplier.value) query.supplier = this.f.supplier.value;
    if (this.f.status.value) query.status = this.f.status.value;
    if (this.f.invoice_number.value) query.name = this.f.invoice_number.value;
    if (this.f.start_date.value && this.f.end_date.value) {
      query.fromDate = new Date(this.f.start_date.value).setHours(0, 0, 0, 0);
      query.toDate = new Date(this.f.end_date.value).setHours(23, 59, 59, 59);
    }

    this.paginator.pageIndex = event?.pageIndex || 0;
    this.paginator.pageSize = event?.pageSize || 30;

    this.dataSource.loadItems(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      query,
      this.sortQuery,
    );
  }

  getTotal() {
    this.dataSource.total.subscribe({
      next: total => {
        this.total = total;
      },
    });
  }

  setFilter(event?: any) {
    const query: any = {};
    if (this.f.supplier.value) query.supplier = this.f.supplier.value;
    if (this.f.status.value) query.status = this.f.status.value;
    if (this.f.invoice_number.value) query.name = this.f.invoice_number.value;
    if (this.f.start_date.value && this.f.end_date.value) {
      query.fromDate = new Date(this.f.start_date.value).setHours(0, 0, 0, 0);
      query.toDate = new Date(this.f.end_date.value).setHours(23, 59, 59, 59);
    }

    if (event) {
      for (const key of Object.keys(event)) {
        if (key === 'active' && event.direction !== '') {
          this.sortQuery[event[key]] = event.direction;
        }
      }
    }

    this.sortQuery =
      Object.keys(this.sortQuery).length === 0
        ? { created_on: 'DESC' }
        : this.sortQuery;

    this.paginator.pageIndex = event?.pageIndex || 0;
    this.paginator.pageSize = event?.pageSize || 30;

    this.dataSource.loadItems(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      query,
      this.sortQuery,
    );
  }

  clearFilters() {
    this.purchaseForm.reset();
    this.status = 'All';
    this.f.status.setValue('All');
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 30;

    this.dataSource.loadItems(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      undefined,
      this.sortQuery,
    );
  }

  navigateBack() {
    this.location.back();
  }
  isVisible: boolean = false;
  purchaseInvoiceFilter() {
    const filterShowHideID = document.getElementById('purchase_invoice_filter');
    const PurchaseFilter = document.getElementById('PurchaseFilter');
    const PurchaseClose = document.getElementById('PurchaseClose');
    const purchaseTable = document.getElementById('purchaseTable');

    if (filterShowHideID.classList.contains('active')) {
      filterShowHideID.classList.remove('active');
      this.isVisible = false;
      PurchaseClose.style.display = 'none';
      PurchaseFilter.style.display = 'block';
      purchaseTable.style.height = '76vh';
    } else {
      filterShowHideID.classList.add('active');
      this.isVisible = true;
      PurchaseClose.style.display = 'block';
      PurchaseFilter.style.display = 'none';
      purchaseTable.style.height = '69vh';
    }
  }

  getTime(timeString: string) {
    const time = timeString.split(':');
    const hours = Number(time[0]);
    const minutes = Number(time[1]);
    const seconds = Number(time[2].split('.')[0]); // Get the seconds without milliseconds

    // Determine if it's AM or PM
    const ampm = hours >= 12 ? 'PM' : 'AM';

    // Convert to 12-hour format
    const tm = hours % 12 === 0 ? 12 : hours % 12;

    // Pad single-digit hours, minutes, and seconds with leading zeros
    const formattedTime = `${tm
      .toString()
      .padStart(2, '0')}:${minutes
      .toString()
      .padStart(2, '0')}:${seconds.toString().padStart(2, '0')} ${ampm}`;

    return formattedTime;
  }
}
