import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../../../material/material.module';
import { EditPurchaseTableComponent } from './edit-purchase-table.component';

describe('EditPurchaseTableComponent', () => {
  let component: EditPurchaseTableComponent;
  let fixture: ComponentFixture<EditPurchaseTableComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [EditPurchaseTableComponent],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        imports: [
          IonicModule.forRoot(),
          MaterialModule,
          HttpClientTestingModule,
          FormsModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(EditPurchaseTableComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
