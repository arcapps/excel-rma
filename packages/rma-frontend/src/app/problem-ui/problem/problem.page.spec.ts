import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, PopoverController } from '@ionic/angular';

import { Location } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { MaterialModule } from '../../material/material.module';
import { ProblemService } from '../services/problem/problem.service';
import { ProblemPage } from './problem.page';

describe('ProblemPage', () => {
  let component: ProblemPage;
  let fixture: ComponentFixture<ProblemPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ProblemPage],
        imports: [
          IonicModule.forRoot(),
          MaterialModule,
          ReactiveFormsModule,
          FormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [
          {
            provide: ProblemService,
            useValue: {
              getProblemList: (...args) =>
                of({ docs: [], length: 0, offset: 0 }),
            },
          },
          { provide: Location, useValue: {} },
          { provide: PopoverController, useValue: {} },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(ProblemPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
