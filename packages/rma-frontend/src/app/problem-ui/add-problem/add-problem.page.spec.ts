import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, PopoverController } from '@ionic/angular';

import { of } from 'rxjs';
import { ProblemService } from '../services/problem/problem.service';
import { AddProblemPage } from './add-problem.page';

describe('AddProblemPage', () => {
  let component: AddProblemPage;
  let fixture: ComponentFixture<AddProblemPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AddProblemPage],
        imports: [IonicModule.forRoot()],
        providers: [
          {
            provide: ProblemService,
            useValue: {
              getProblem: (...args) => of({ problem_name: '' }),
            },
          },
          { provide: PopoverController, useValue: {} },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(AddProblemPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
