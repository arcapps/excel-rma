import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, map, startWith, switchMap } from 'rxjs/operators';
import { ValidateInputSelected } from '../../common/pipes/validators';
import {
  STOCK_ENTRY_TYPE,
  STOCK_TRANSFER_STATUS,
  WAREHOUSES,
} from '../../constants/app-string';
import { MY_FORMATS } from '../../constants/date-format';
import { PERMISSION_STATE } from '../../constants/permission-roles';
import { SalesService } from '../../sales-ui/services/sales.service';
import { StockEntryService } from '../services/stock-entry/stock-entry.service';
import {
  StockEntryListData,
  StockEntryListDataSource,
} from './stock-entry-list-datasource';

@Component({
  selector: 'app-stock-entry-list',
  templateUrl: './stock-entry-list.page.html',
  styleUrls: ['./stock-entry-list.page.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class StockEntryListPage implements OnInit {
  salesInvoiceList: Array<StockEntryListData>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  dataSource: StockEntryListDataSource;
  displayedColumns = [
    'sr_no',
    'name',
    's_warehouse',
    't_warehouse',
    'status',
    'createdBy',
    'territory',
    'posting_date',
    'remarks',
    // 'stockEntryType',
    // 'posting_time',
  ];
  filterState: any = {};
  filteredFromWarehouseList: any[];
  filteredToWarehouseList: any[];
  validateInput: any = ValidateInputSelected;
  permissionState = PERMISSION_STATE;
  invoiceStatus: string[] = Object.keys(STOCK_TRANSFER_STATUS).map(
    key => STOCK_TRANSFER_STATUS[key],
  );
  search: string = '';
  sortQuery: any = {};
  stockEntryForm: UntypedFormGroup = new UntypedFormGroup({
    start_date: new UntypedFormControl(),
    end_date: new UntypedFormControl(),
    status: new UntypedFormControl(),
    stockEntryType: new UntypedFormControl(),
    from_warehouse: new UntypedFormControl(),
    to_warehouse: new UntypedFormControl(),
    names: new UntypedFormControl(),
  });
  stockEntryType: string[] = Object.values(STOCK_ENTRY_TYPE);

  get f() {
    return this.stockEntryForm.controls;
  }
  constructor(
    private readonly location: Location,
    private readonly stockEntryService: StockEntryService,
    private readonly salesService: SalesService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.setAutoComplete();
    this.route.params.subscribe(() => {
      this.paginator.firstPage();
    });
    this.dataSource = new StockEntryListDataSource(this.stockEntryService);
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(event => {
          this.dataSource.loadItems(undefined, undefined, undefined, {});
          return event;
        }),
      )
      .subscribe();
  }

  setAutoComplete() {
    this.stockEntryForm
      .get('from_warehouse')
      .valueChanges.pipe(
        startWith(''),
        switchMap(value => {
          return this.salesService.getStore().getItemAsync(WAREHOUSES, value);
        }),
      )
      .subscribe(res => (this.filteredFromWarehouseList = res));

    this.stockEntryForm
      .get('to_warehouse')
      .valueChanges.pipe(
        startWith(''),
        switchMap(value => {
          return this.salesService.getStore().getItemAsync(WAREHOUSES, value);
        }),
      )
      .subscribe(res => (this.filteredToWarehouseList = res));
  }

  getStockIdName(row) {
    if (row?.stock_id?.includes('BTRIN')) {
      return row.trin_stock_id;
    }
    return row.stock_id;
  }

  statusChange(status) {
    if (status === 'All') {
      delete this.filterState.status;
      this.dataSource.loadItems();
    } else {
      this.filterState.status = status;
      this.setFilter();
    }
  }

  setStockEntryType(type) {
    this.filterState.stock_entry_type = type;
    this.setFilter();
  }

  getUpdate(event: any) {
    const query: any = this.filterState;
    if (this.search) query.search = this.search;
    if (this.f.start_date.value && this.f.end_date.value) {
      query.fromDate = new Date(this.f.start_date.value).setHours(0, 0, 0, 0);
      query.toDate = new Date(this.f.end_date.value).setHours(23, 59, 59, 59);
    }

    this.paginator.pageIndex = event?.pageIndex || 0;
    this.paginator.pageSize = event?.pageSize || 30;

    this.dataSource.loadItems(
      event.pageIndex,
      event.pageSize,
      query,
      this.sortQuery,
    );
  }

  fromWarehouseChange(value: string) {
    this.filterState.s_warehouse = value;
    this.setFilter();
  }

  toWarehouseChange(value: string) {
    this.filterState.t_warehouse = value;
    this.setFilter();
  }

  clearFilters() {
    this.filterState = {};
    this.stockEntryForm.reset();

    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 30;

    this.dataSource.loadItems(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      undefined,
      this.sortQuery,
    );
  }

  // setFilter(event?: any) {
  //   const query: any = this.filterState;

  //   if (this.f.names.value) {
  //     if (
  //       this.f.names.value.includes('PAQ') ||
  //       this.f.names.value.includes('TROUT') ||
  //       this.f.names.value.includes('PCM') ||
  //       this.f.names.value.includes('RND')
  //     ) {
  //       query.stock_id = this.f.names.value;
  //     } else {
  //       query.names = this.f.names.value;
  //     }
  //   }
  //   if (this.f.start_date.value && this.f.end_date.value) {
  //     query.fromDate = new Date(this.f.start_date.value).setHours(0, 0, 0, 0);
  //     query.toDate = new Date(this.f.end_date.value).setHours(23, 59, 59, 59);
  //   }

  //   if (event) {
  //     for (const key of Object.keys(event)) {
  //       if (key === 'active' && event.direction !== '') {
  //         this.sortQuery[event[key]] = event.direction;
  //       }
  //     }
  //   }
  //   this.sortQuery =
  //     Object.keys(this.sortQuery).length === 0
  //       ? { createdOn: 'DESC' }
  //       : this.sortQuery;

  //   this.paginator.pageIndex = 0;
  //   this.paginator.pageSize = 30;

  //   this.dataSource.loadItems(
  //     this.paginator.pageIndex,
  //     this.paginator.pageSize,
  //     query,
  //     this.sortQuery,
  //   );
  // }
  setFilter(): void {
    const query: any = this.filterState;

    if (this.f.names.value) {
      if (
        this.f.names.value.includes('PAQ') ||
        this.f.names.value.includes('TROUT') ||
        this.f.names.value.includes('PCM') ||
        this.f.names.value.includes('RND')
      ) {
        query.stock_id = this.f.names.value;
      } else {
        query.names = this.f.names.value;
      }
    }
    if (this.f.start_date.value && this.f.end_date.value) {
      query.fromDate = new Date(this.f.start_date.value).setHours(0, 0, 0, 0);
      query.toDate = new Date(this.f.end_date.value).setHours(23, 59, 59, 59);
    }

    // Assuming you have a property like 'direction' to determine sorting direction
    if (this.sort.direction !== '') {
      this.sortQuery.active = 'createdOn'; // Replace with your actual sort property
      this.sortQuery.direction = this.sort.direction;
    }

    this.sortQuery =
      Object.keys(this.sortQuery).length === 0
        ? { createdOn: 'DESC' } // Replace with your default sort property
        : this.sortQuery;

    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 30;

    this.dataSource.loadItems(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      query,
      this.sortQuery,
    );
  }

  navigateBack() {
    this.location.back();
  }
  isVissible: boolean = false;
  stockEntryList() {
    const filterShowHideID = document.getElementById('stock_entry_list');
    const MaterialFilter = document.getElementById('MaterialFilter');
    const MaterialClose = document.getElementById('MaterialClose');
    const MaterialTable = document.getElementById('MaterialTable');

    if (filterShowHideID.classList.contains('active')) {
      filterShowHideID.classList.remove('active');
      this.isVissible = false;
      MaterialClose.style.display = 'none';
      MaterialFilter.style.display = 'block';
      MaterialTable.style.height = '76vh';
    } else {
      filterShowHideID.classList.add('active');
      this.isVissible = true;
      MaterialClose.style.display = 'block';
      MaterialFilter.style.display = 'none';
      MaterialTable.style.height = '61vh';
    }
  }

  timeFunc(timeString: string) {
    const time = timeString.split(':');
    const hh = time[0] === '0' ? '1' : time[0];
    const mm = time[1];
    const ampm = Number(time[0]) > 12 ? 'PM' : 'AM';
    const fullTime =
      (Number(hh) > 12 ? Number(hh) - 12 : Number(hh)) + ':' + mm + ':' + ampm;

    return fullTime;
  }
  convertToMediamTime(timeString: string): string {
    if (!timeString) {
      return '';
    }

    const timeParts = timeString.split(':');
    let hours = parseInt(timeParts[0], 10);
    const minutes = parseInt(timeParts[1], 10);
    const seconds = parseInt(timeParts[2], 10);

    let meridian = 'AM';
    if (hours >= 12) {
      meridian = 'PM';
      if (hours > 12) {
        hours -= 12;
      }
    }

    return `${hours}:${minutes
      .toString()
      .padStart(2, '0')}:${seconds.toString().padStart(2, '0')} ${meridian}`;
  }
}
