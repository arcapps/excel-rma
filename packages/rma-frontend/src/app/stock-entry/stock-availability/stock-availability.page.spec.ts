import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StockAvailabilityPage } from './stock-availability.page';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { SalesService } from '../../../app/sales-ui/services/sales.service';
import { CsvJsonService } from '../../api/csv-json/csv-json.service';
import { MaterialModule } from '../../material/material.module';
import { StockLedgerService } from '../services/stock-ledger/stock-ledger.service';

describe('StockAvailabilityPage', () => {
  let component: StockAvailabilityPage;
  let fixture: ComponentFixture<StockAvailabilityPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [StockAvailabilityPage],
        imports: [
          IonicModule.forRoot(),
          MaterialModule,
          FormsModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          RouterTestingModule,
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [
          {
            provide: CsvJsonService,
            useValue: {},
          },
          {
            provide: StockLedgerService,
            useValue: {
              listStockLedger: (...args) => of([]),
            },
          },
          {
            provide: SalesService,
            useValue: {
              getItemList: (...args) => of([]),
              getStore: () => ({
                getItemAsync: (...args) => Promise.resolve({}),
              }),
              getItemGroupList: (...args) => of([]),
              getItemBrandList: (...args) => of([]),
            },
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(StockAvailabilityPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
