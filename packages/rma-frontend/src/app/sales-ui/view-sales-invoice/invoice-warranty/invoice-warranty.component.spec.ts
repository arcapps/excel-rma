import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../../../material/material.module';
import { InvoiceWarrantyComponent } from './invoice-warranty.component';
import { ActivatedRoute } from '@angular/router';
import { SalesService } from '../../services/sales.service';
import { of } from 'rxjs';

describe('InvoiceWarrantyComponent', () => {
  let component: InvoiceWarrantyComponent;
  let fixture: ComponentFixture<InvoiceWarrantyComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [InvoiceWarrantyComponent],
        imports: [MaterialModule, HttpClientModule],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {},
          },
          {
            provide: SalesService,
            useValue: {
              getWarrantyClaimsBySINV: () => of([]), // Provide a mock observable response
            },
          },
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceWarrantyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
