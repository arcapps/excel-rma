import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {
  LoadingController,
  NavParams,
  PopoverController,
} from '@ionic/angular';

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { of } from 'rxjs';
import { StorageService } from '../../../api/storage/storage.service';
import { SalesService } from '../../services/sales.service';
import { PrintComponent } from './print.component';

describe('PrintComponent', () => {
  let component: PrintComponent;
  let fixture: ComponentFixture<PrintComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        declarations: [PrintComponent],
        providers: [
          {
            provide: SalesService,
            useValue: {
              getDeliveryNoteNames: (...args) => of([]),
              getStore: () => ({
                getItem: (...args) => Promise.resolve('Item'),
                getItems: (...args) => Promise.resolve({}),
              }),
            },
          },
          {
            provide: LoadingController,
            useValue: {},
          },
          {
            provide: MatSnackBar,
            useValue: {},
          },
          {
            provide: NavParams,
            useValue: { data: {} },
          },
          {
            provide: StorageService,
            useValue: {
              getItem: (...args) => Promise.resolve('ITEM'),
            },
          },
          { provide: PopoverController, useValue: {} },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(PrintComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
