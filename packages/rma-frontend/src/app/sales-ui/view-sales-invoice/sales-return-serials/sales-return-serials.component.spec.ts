import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { MaterialModule } from '../../../material/material.module';
import { SalesReturnSerialsComponent } from './sales-return-serials.component';
import { SalesReturnSerialsService } from './sales-return-serials.service';

describe('SalesReturnSerialsComponent', () => {
  let component: SalesReturnSerialsComponent;
  let fixture: ComponentFixture<SalesReturnSerialsComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [SalesReturnSerialsComponent],
        imports: [
          MaterialModule,
          FormsModule,
          BrowserAnimationsModule,
          RouterTestingModule,
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [
          {
            provide: SalesReturnSerialsService,
            useValue: {
              getSalesReturnSerialList: (...args) => of([{}]),
            },
          },
        ],
      }).compileComponents();
    }),
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesReturnSerialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
