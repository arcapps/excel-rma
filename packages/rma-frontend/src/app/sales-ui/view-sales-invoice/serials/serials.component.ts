import {
  Component,
  ElementRef,
  Inject,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import { SalesService } from '../../services/sales.service';

import { MomentDateAdapter } from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { Subject, firstValueFrom, from, of } from 'rxjs';
import { mergeMap, startWith, switchMap, toArray } from 'rxjs/operators';
import { TimeService } from '../../../api/time/time.service';
import { DeliveredSerialsState } from '../../../common/components/delivered-serials/delivered-serials.component';
import { SerialAssign } from '../../../common/interfaces/sales.interface';
import { ValidateInputSelected } from '../../../common/pipes/validators';
import {
  ASSIGN_SERIAL_DIALOG_QTY,
  CLOSE,
  DELIVERED_SERIALS_BY,
  DELIVERY_NOTE,
  WAREHOUSES,
} from '../../../constants/app-string';
import { MY_FORMATS } from '../../../constants/date-format';
import {
  ERROR_FETCHING_SALES_INVOICE,
  SERIAL_ASSIGNED,
} from '../../../constants/messages';
import { PERMISSION_STATE } from '../../../constants/permission-roles';
import {
  BACKDATE_PERMISSION,
  BACKDATE_PERMISSION_FOR_DAYS,
  DELIVERED_SERIALS_DISPLAYED_COLUMNS,
} from '../../../constants/storage';
import { SalesInvoiceDetails } from '../details/details.component';
import { ViewSalesInvoicePage } from '../view-sales-invoice.page';
import {
  DeliveryNoteItemInterface,
  ItemDataSource,
  SerialDataSource,
} from './serials-datasource';
import * as moment from 'moment';
import { StorageService } from '../../../api/storage/storage.service';

@Component({
  selector: 'sales-invoice-serials',
  templateUrl: './serials.component.html',
  styleUrls: ['./serials.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class SerialsComponent implements OnInit {
  @ViewChild('csvFileInput', { static: false })
  csvFileInput: ElementRef;

  xlsxData: any;
  showSerial: string = 'no';
  date = new UntypedFormControl(new Date());
  allowBackDatedInvoices: boolean;
  allowBackDatedInvoicesForDays: number;
  minPostingDate: Date = new Date();
  claimsReceivedDate: string;
  permissionState = PERMISSION_STATE;
  warehouseFormControl = new UntypedFormControl('', [Validators.required]);
  costCenterFormControl = new UntypedFormControl('', [Validators.required]);
  filteredWarehouseList: any[];
  getOptionText = '';
  salesInvoiceDetails: SalesInvoiceDetails;
  submit: boolean = false;
  state = {
    component: DELIVERY_NOTE,
    warehouse: '',
    itemData: [],
    costCenter: '',
  };
  rangePickerState = {
    prefix: '',
    fromRange: '',
    toRange: '',
    serials: [],
  };

  DEFAULT_SERIAL_RANGE = { start: 0, end: 0, prefix: '', serialPadding: 0 };
  filteredItemList = [];
  fromRangeUpdate = new Subject<string>();
  toRangeUpdate = new Subject<string>();
  validateInput: any = ValidateInputSelected;
  itemDisplayedColumns = [
    'sr_no',
    'item_name',
    'qty',
    'assigned',
    'remaining',
    'has_serial_no',
    'salesWarrantyMonths',
    'add_serial',
  ];
  itemDataSource: ItemDataSource;
  serialDisplayedColumns = [
    'sr_no',
    'item_name',
    'qty',
    'warranty_date',
    'serial_no',
    'delete',
  ];
  materialTransferDisplayedColumns = [
    's_warehouse',
    't_warehouse',
    'item_code',
    'item_name',
    'qty',
    'amount',
    'serial_no',
  ];
  serialDataSource: SerialDataSource;

  deliveredSerialsDisplayedColumns = [
    'sr_no',
    'item_name',
    'warehouse',
    'sales_warranty_period',
    'sales_warranty_expiry',
    'serial_no',
  ];
  deliveredSerialsState: DeliveredSerialsState = {
    deliveredSerialsDisplayedColumns:
      DELIVERED_SERIALS_DISPLAYED_COLUMNS[
        DELIVERED_SERIALS_BY.sales_invoice_name
      ],
    type: DELIVERED_SERIALS_BY.sales_invoice_name,
  };
  disableDeliveredSerialsCard: boolean = false;
  remaining: number = 0;
  validSerials: boolean = true;
  salesInvoiceItemsTemp: any[];

  constructor(
    private readonly salesService: SalesService,
    private readonly snackBar: MatSnackBar,
    private readonly route: ActivatedRoute,
    private readonly dialog: MatDialog,
    private readonly timeService: TimeService,
    private readonly renderer: Renderer2,
    private readonly viewSalesInvoicePage: ViewSalesInvoicePage,
    private readonly loadingController: LoadingController,
    private readonly storageService: StorageService,
  ) {}

  ngOnInit() {
    this.serialDataSource = new SerialDataSource();
    this.itemDataSource = new ItemDataSource();
    this.getSalesInvoice(this.route.snapshot.params.invoiceUuid);
    this.checkBackDatedInvoices();

    this.warehouseFormControl.valueChanges
      .pipe(
        startWith(''),
        switchMap(value => {
          return this.salesService.getStore().getItemAsync(WAREHOUSES, value);
        }),
      )
      .subscribe(res => (this.filteredWarehouseList = res));
  }
  checkBackDatedInvoices() {
    this.storageService.getItem(BACKDATE_PERMISSION).then(res => {
      this.allowBackDatedInvoices = res === 'true' ? true : false;
    });
    this.storageService.getItem(BACKDATE_PERMISSION_FOR_DAYS).then(days => {
      if (days) {
        this.allowBackDatedInvoicesForDays = Number(days);
        this.minPostingDate = new Date(
          moment()
            .subtract(this.allowBackDatedInvoicesForDays, 'days')
            .format('L'),
        );
      }
    });
  }

  getFilteredItems(salesInvoice: SalesInvoiceDetails) {
    const filteredItemList = [];
    let remaining = 0;
    salesInvoice.items.forEach(item => {
      item.assigned = 0;
      item.remaining = item.qty;
      if (
        salesInvoice.delivered_items_map &&
        salesInvoice.delivered_items_map[btoa(item.item_code)]
      ) {
        item.assigned =
          salesInvoice.delivered_items_map[btoa(item.item_code)] || 0;
        item.remaining =
          item.qty - salesInvoice.delivered_items_map[btoa(item.item_code)];
      }
      remaining += item.remaining;
      filteredItemList.push(item);
    });
    this.remaining = remaining;

    return this.sortSecondArrayByFirstOrder(
      this.salesInvoiceItemsTemp,
      filteredItemList,
    );
  }

  getItemsWarranty() {
    from(this.itemDataSource.data())
      .pipe(
        mergeMap(item => {
          return this.salesService.getItemFromRMAServer(item.item_code).pipe(
            switchMap(warrantyItem => {
              item.salesWarrantyMonths = warrantyItem.salesWarrantyMonths;
              return of(item);
            }),
          );
        }),
        toArray(),
      )
      .subscribe({
        next: success => {
          this.itemDataSource.loadItems(
            this.sortSecondArrayByFirstOrder(
              this.salesInvoiceItemsTemp,
              success,
            ),
          );
        },
        error: () => {},
      });
  }

  getSalesInvoice(uuid: string) {
    return this.salesService
      .getSalesInvoice(uuid)
      .pipe(
        switchMap((sales_invoice: SalesInvoiceDetails) => {
          this.deliveredSerialsState.uuid = sales_invoice.name;
          if (sales_invoice.has_bundle_item) {
            const item_codes = {};
            sales_invoice.items.forEach(item => {
              item_codes[item.item_code] = item.qty;
            });
            return this.salesService.getBundleItem(item_codes).pipe(
              switchMap((data: any) => {
                sales_invoice.items = data;
                this.salesInvoiceItemsTemp = data;

                return of(sales_invoice);
              }),
            );
          } else {
            this.salesInvoiceItemsTemp = sales_invoice.items;
          }
          return of(sales_invoice);
        }),
      )
      .subscribe({
        next: (sales_invoice: SalesInvoiceDetails) => {
          this.salesInvoiceDetails = sales_invoice as SalesInvoiceDetails;

          this.disableDeliveredSerialsCard =
            this.salesInvoiceDetails.delivery_note_names &&
            this.salesInvoiceDetails.delivery_note_names.length === 0
              ? true
              : false;
          this.filteredItemList = this.getFilteredItems(sales_invoice);
          this.itemDataSource.loadItems(this.filteredItemList);
          this.warehouseFormControl.setValue(sales_invoice.delivery_warehouse);
          this.date.setValue(new Date());
          this.getItemsWarranty();
          this.state.itemData = this.itemDataSource.data();
          this.state.warehouse = this.warehouseFormControl.value;
          this.salesService.relaySalesInvoice(sales_invoice.name).subscribe({
            next: success => {
              this.costCenterFormControl.setValue(success.cost_center);
            },
            error: error => {
              this.presentSnackBar(error);
            },
          });
        },
        error: err => {
          this.presentSnackBar(
            err.error.message
              ? err.error.message
              : `${ERROR_FETCHING_SALES_INVOICE}${err.error.error}`,
          );
        },
      });
  }

  async assignSingularSerials(row: Item, serialArr: string[]) {
    // eslint-disable-next-line
    const dialogRef =
      row.remaining >= ASSIGN_SERIAL_DIALOG_QTY
        ? this.dialog.open(AssignSerialsDialog, {
            width: '250px',
            data: { serials: row.remaining || 0 },
          })
        : null;

    const serials =
      row.remaining >= ASSIGN_SERIAL_DIALOG_QTY
        ? await firstValueFrom<number>(dialogRef.afterClosed())
        : row.remaining;
    if (serials && serials <= row.remaining) {
      this.addSingularSerials(row, serials, serialArr);
      // this.resetRangeState();
      this.updateProductState(row, serials);
      return;
    }
    this.presentSnackBar('Please select a valid number of rows.');
  }

  async assignRangeSerial(row: Item, serials: string[]) {
    const data = this.serialDataSource.data();
    data.push({
      item_code: row.item_code,
      item_name: row.item_name,
      qty: serials.length,
      rate: row.rate,
      brand: row.brand,
      has_serial_no: row.has_serial_no,
      warranty_date: await this.getWarrantyDate(row.salesWarrantyMonths),
      amount: row.amount,
      serial_no: serials,
    });
    this.updateProductState(row.item_code, serials.length);
    this.serialDataSource.update(data);
    // eslint-disable-next-line
    this.resetRangeState();
  }

  assignSerial(itemRow: Item) {
    if (!itemRow.has_serial_no) {
      this.showSerial = 'yes';
      this.addNonSerialItem(itemRow);
      return;
    }
    if (
      !this.rangePickerState.serials.length ||
      this.rangePickerState.serials.length === 1
    ) {
      this.assignSingularSerials(itemRow, this.rangePickerState.serials);
      this.showSerial = 'yes';
      return;
    }
    if (itemRow.remaining < this.rangePickerState.serials.length) {
      this.presentSnackBar(
        `Only ${itemRow.remaining} serials could be assigned to ${itemRow.item_code}`,
      );
      return;
    }
    this.validateSerial(
      { item_code: itemRow.item_code, serials: this.rangePickerState.serials },
      itemRow,
    );
  }

  async addNonSerialItem(row: Item) {
    const dialogRef = this.dialog.open(AssignNonSerialsItemDialog, {
      width: '250px',
      data: { qty: row.remaining || 0, remaining: row.remaining },
    });
    const assignValue = await firstValueFrom(dialogRef.afterClosed());
    if (assignValue && assignValue <= row.remaining) {
      const serials = this.serialDataSource.data();
      serials.push({
        item_code: row.item_code,
        item_name: row.item_name,
        qty: assignValue,
        warranty_date: await this.getWarrantyDate(row.salesWarrantyMonths),
        rate: row.rate,
        brand: row.brand,
        amount: row.amount,
        has_serial_no: row.has_serial_no,
        serial_no: ['Non Serial Item'],
      });
      this.serialDataSource.update(serials);
      // eslint-disable-next-line
      this.updateProductState(row.item_code, assignValue);
      return;
    }
    this.presentSnackBar('Please select a valid number of rows.');
  }

  validateSerial(
    item: { item_code: string; serials: string[]; warehouse?: string },
    row: Item,
  ) {
    if (!this.warehouseFormControl.value) {
      this.getMessage('Please select a warehouse to validate serials.');
      return;
    }
    item.warehouse = this.warehouseFormControl.value;
    this.salesService.validateSerials(item).subscribe({
      next: (success: { notFoundSerials: string[] }) => {
        if (success.notFoundSerials && success.notFoundSerials.length) {
          this.presentSnackBar(
            `Found ${success.notFoundSerials.length} Invalid Serials for
              item: ${item.item_code} at
              warehouse: ${item.warehouse},
              ${success.notFoundSerials.splice(0, 50).join(', ')}...`,
          );
          return;
        }
        this.assignRangeSerial(row, this.rangePickerState.serials);
      },
      error: () => {},
    });
  }

  addSingularSerials(row, serialCount, serialArr) {
    this.updateProductState(row.item_code, serialCount);
    const serials = this.serialDataSource.data();
    let serialTemp = [];
    if (serialArr && serialArr.length > 0) {
      serialTemp = serialArr;
    }
    Array.from({ length: serialCount }, async (x, i) => {
      serials.push({
        item_code: row.item_code,
        item_name: row.item_name,
        qty: 1,
        has_serial_no: row.has_serial_no,
        warranty_date: await this.getWarrantyDate(row.salesWarrantyMonths),
        rate: row.rate,
        amount: row.amount,
        brand: row.brand,
        serial_no: [...serialTemp, ...serialTemp],
      });
      this.serialDataSource.update(serials);
      // eslint-disable-next-line
    });
  }

  async getWarrantyDate(salesWarrantyMonths: number) {
    let date = new Date(this.date.value);
    if (salesWarrantyMonths) {
      try {
        date = new Date(date.setMonth(date.getMonth() + salesWarrantyMonths));
        return await (await this.timeService.getDateAndTime(date)).date;
      } catch (err) {
        this.getMessage(`Error occurred while settings warranty date: ${err}`);
      }
    }
    return;
  }

  updateProductState(item_code, assigned) {
    const itemState = this.itemDataSource.data();
    itemState.filter(product => {
      if (product.item_code === item_code) {
        product.assigned = product.assigned + assigned;
        product.remaining = product.qty - product.assigned;
      }
      return product;
    });
    this.itemDataSource.update(itemState);
  }
  showSerialData() {}
  deleteRow(row, i) {
    let serialData = this.serialDataSource.data();
    serialData.length === 1 ? (serialData = []) : serialData.splice(i, 1);

    this.serialDataSource.update(serialData);
    const itemData = this.itemDataSource.data();

    itemData.filter(item => {
      if (item.item_code === row.item_code) {
        item.assigned = item.assigned - row.qty;
        item.remaining = item.remaining + row.qty;
      }
      return item;
    });

    this.itemDataSource.update(itemData);
    // eslint-disable-next-line
  }

  onSerialKepUp(i) {
    let element;
    try {
      element = this.renderer.selectRootElement(`#serials${i + 1}`);
    } catch {
      return;
    }

    element?.focus();
  }

  getSerialsInputValue(row) {
    // eslint-disable-next-line
    if (!row.serial_no.length) return;

    return row.serial_no.length === 1
      ? row.serial_no[0]
      : `${row.serial_no[0]} - ${row.serial_no[row.serial_no.length - 1]}`;
  }
  // // Define a variable to store a timeout reference
  // serialInputTimeout: any;

  // onSerialInputInput(event: any, index: number, row: any) {
  //   console.log("Inpur getting changed")
  //   // clearTimeout(this.serialInputTimeout);

  //   this.onSerialInputChange(event.target.value, index, row);
  //   // this.serialInputTimeout = setTimeout(() => {
  //   // }, 10);
  // }

  // onSerialInputChange(newSerial: string, index: number, row: any) {
  //   const tempData = JSON.parse(JSON.stringify(this.serialDataSource.data()));

  //   if (Array.isArray(tempData[index].serial_no)) {
  //     tempData[index].serial_no[0] = newSerial.toUpperCase();
  //     row.updatedSerial = newSerial.toUpperCase();
  //     this.serialDataSource.update(tempData);
  //     // setTimeout(() => {
  //     //   this.onSerialKepUp(index - 1);
  //     // });
  //   }
  // }

  validateState() {
    const data = this.serialDataSource.data();
    let isValid = true;
    let index = 0;
    if (!this.warehouseFormControl.value) {
      this.presentSnackBar('Please select a warehouse.');
      return false;
    }
    if (!this.costCenterFormControl.value) {
      this.presentSnackBar('Please select a Cost Center.');
      return false;
    }

    for (const item of data) {
      index++;
      if (
        !item.serial_no ||
        !item.serial_no.length ||
        item.serial_no[0] === ''
      ) {
        isValid = false;
        this.getMessage(
          `Serial No empty for ${item.item_name} at position ${index}, please add a Serial No`,
        );
        break;
      }
    }
    return isValid;
  }

  generatePayload() {
    const assignSerial = {} as SerialAssign;

    assignSerial.company = this.salesInvoiceDetails.company;
    assignSerial.customer = this.salesInvoiceDetails.customer;
    assignSerial.posting_date = this.getParsedDate(this.date.value);
    assignSerial.posting_time = this.getFrappeTime();
    assignSerial.sales_invoice_name = this.salesInvoiceDetails.name;
    assignSerial.set_warehouse = this.warehouseFormControl.value;
    assignSerial.total = 0;
    assignSerial.total_qty = 0;
    assignSerial.items = [];

    const item_hash: { [key: string]: DeliveryNoteItemInterface } = {};

    this.salesInvoiceDetails.items.forEach(item => {
      if (!item_hash[item.item_code]) {
        item_hash[item.item_code] = { serial_no: [] };
      }
      item_hash[item.item_code].item_code = item.item_code;
      item_hash[item.item_code].item_name = item.item_name;
      item_hash[item.item_code].rate = item.rate || 0;
      item_hash[item.item_code].qty = 0;
      item_hash[item.item_code].amount = 0;
      item_hash[item.item_code].cost_center = this.costCenterFormControl.value;
    });

    this.serialDataSource.data().forEach(serial => {
      const existing_qty = item_hash[serial.item_code].qty;
      const existing_rate = item_hash[serial.item_code].rate;
      const existing_serials = item_hash[serial.item_code].serial_no;
      assignSerial.total_qty += serial.qty;
      assignSerial.total += serial.qty * existing_rate;

      Object.assign(item_hash[serial.item_code], serial);

      item_hash[serial.item_code].qty += existing_qty;
      item_hash[serial.item_code].rate = existing_rate;
      item_hash[serial.item_code].amount =
        item_hash[serial.item_code].qty * item_hash[serial.item_code].rate;
      item_hash[serial.item_code].serial_no.push(...existing_serials);
      item_hash[
        serial.item_code
      ].against_sales_invoice = this.salesInvoiceDetails.name;
    });

    Object.keys(item_hash).forEach(key => {
      if (item_hash[key].qty) {
        assignSerial.items.push(item_hash[key]);
      }
    });
    return assignSerial;
  }

  async submitDeliveryNote() {
    if (!this.validateState()) return;

    this.submit = true;
    this.mergeDuplicateItems();
    const loading = await this.loadingController.create({
      message: 'Creating Delivery Note..',
    });
    await loading.present();
    this.salesService.assignSerials(this.generatePayload()).subscribe({
      next: () => {
        this.validSerials = true;
        this.submit = false;
        loading.dismiss();
        this.presentSnackBar(SERIAL_ASSIGNED);
        this.viewSalesInvoicePage.selectedSegment = 0;
      },
      error: err => {
        this.validSerials = false;
        loading.dismiss();
        this.submit = false;
        if (err.status === 406) {
          const errMessage = err.error.message.split('\\n');
          this.presentSnackBar(errMessage[errMessage.length - 2].split(':')[1]);
          return;
        }
        this.presentSnackBar(err.error.message);
      },
    });
  }

  mergeDuplicateItems() {
    const map = {};
    this.serialDataSource.data().forEach(item => {
      if (map[item.item_code]) {
        map[item.item_code].qty += item.qty;
        map[item.item_code].serial_no.push(...item.serial_no);
      } else {
        map[item.item_code] = item;
      }
    });
    this.serialDataSource.update(Object.values(map));
  }

  resetRangeState() {
    this.rangePickerState = {
      prefix: '',
      fromRange: '',
      toRange: '',
      serials: [],
    };
  }

  getFrappeTime() {
    const date = new Date();
    return [date.getHours(), date.getMinutes(), date.getSeconds()].join(':');
  }

  addSerialsFromCsvJson(csvJsonObj: any) {
    const data = this.itemDataSource.data();
    data.some(element => {
      if (csvJsonObj[element.item_code]) {
        if (!element.has_serial_no) {
          this.presentSnackBar(`${element.item_name} is a non-serial item.`);
          return true;
        }
        this.assignRangeSerial(
          element,
          csvJsonObj[element.item_code].serial_no,
        );
        return false;
      }
    });
  }

  getMessage(notFoundMessage, expected?, found?) {
    return this.presentSnackBar(
      expected && found
        ? `${notFoundMessage}, expected ${expected} found ${found}`
        : `${notFoundMessage}`,
    );
  }

  getParsedDate(value) {
    const date = new Date(value);
    return [
      date.getFullYear(),
      date.getMonth() + 1,
      // +1 as index of months start's from 0
      date.getDate(),
    ].join('-');
  }

  assignPickerState(rangePickerState) {
    this.rangePickerState = rangePickerState;
  }

  warehouseOptionChanged(warehouse) {
    this.state.warehouse = warehouse;
  }

  costCenterOptionChanged(costCenter) {
    this.state.costCenter = costCenter.name;
  }

  presentSnackBar(message: string) {
    this.snackBar.open(message, CLOSE);
  }

  sortSecondArrayByFirstOrder(firstArray, secondArray) {
    // Create a map to store the index of each item_code in the firstArray
    const indexMap = {};
    firstArray.forEach((item, index) => {
      indexMap[item.item_code] = index;
    });

    // Sort the secondArray based on the order of item_code in the firstArray
    secondArray.sort((a, b) => {
      const indexA = indexMap[a.item_code];
      const indexB = indexMap[b.item_code];
      return indexA - indexB;
    });

    return secondArray;
  }
}

export interface CsvJsonObj {
  [key: string]: {
    serial_no: string[];
  };
}
export interface SerialItem {
  item_code: string;
  item_name: string;
  qty: number;
  has_serial_no: number;
  warranty_date?: any;
  rate: number;
  amount: number;
  serial_no: string[];
  against_sales_invoice?: string;
  cost_center?: string;
  brand?: string;
}

export interface Item {
  item_name: string;
  item_code: string;
  qty: number;
  assigned: number;
  has_serial_no: number;
  remaining: number;
  rate?: number;
  amount?: number;
  salesWarrantyMonths?: number;
  purchaseWarrantyMonths?: number;
  brand: string;
}

@Component({
  selector: 'assign-serials-dialog',
  templateUrl: 'assign-serials-dialog.html',
})
export class AssignSerialsDialog {
  constructor(
    public dialogRef: MatDialogRef<AssignSerialsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}
  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'assign-non-serials-item-dialog',
  templateUrl: 'assign-non-serials-item-dialog.html',
})
export class AssignNonSerialsItemDialog {
  constructor(
    public dialogRef: MatDialogRef<AssignNonSerialsItemDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}
  onNoClick(): void {
    this.dialogRef.close();
  }
}
