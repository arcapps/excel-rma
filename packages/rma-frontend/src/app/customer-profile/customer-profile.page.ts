import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  UntypedFormControl,
  UntypedFormGroup,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { DateTime } from 'luxon';
import { forkJoin, from, of, throwError } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  retry,
  startWith,
  switchMap,
} from 'rxjs/operators';
import { TimeService } from '../api/time/time.service';
import { ValidateInputSelected } from '../common/pipes/validators';
import { CLOSE } from '../constants/app-string';
import {
  ACCESS_TOKEN,
  AUTHORIZATION,
  AUTH_SERVER_URL,
  BEARER_TOKEN_PREFIX,
  DEFAULT_COMPANY,
} from '../constants/storage';
import { ItemPriceService } from '../sales-ui/services/item-price.service';
import { SalesService } from '../sales-ui/services/sales.service';
import { CustomerDataSource } from './customer-datasource';
import { BrandLimitService } from './services/brand-limit.service';

@Component({
  selector: 'app-customer-profile',
  templateUrl: './customer-profile.page.html',
  styleUrls: ['./customer-profile.page.scss'],
})
export class CustomerProfilePage implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  dataSource: CustomerDataSource;
  defaultCompany: string;
  customerName: string;
  creditLimit: string;
  displayedColumns = [
    'sr_no',
    'customer',
    'territory',

    'credit_limit',
    'remaining_balance',
    'remaining_credit',
    'brand_wise_limit',
  ];

  brandLimitForm: FormGroup;
  filters: any = [];
  countFilter: any = [];
  customerProfileForm: UntypedFormGroup = new UntypedFormGroup({
    customer: new UntypedFormControl(),
    territory: new UntypedFormControl(),
  });
  filteredCustomerList: any[];
  filteredItemBrandList: any[];
  territories: any[];
  validateInput: any = ValidateInputSelected;
  isFirstModalOpen = false;
  brandsCreditLimits: any[];
  totalRemainingBalance: number;
  allocatedCreditLimit: number;
  othersCreditLimit: number;
  firstColumn: number;
  inputColumn: number;
  btnColumn: number;

  get f() {
    return this.customerProfileForm.controls;
  }

  constructor(
    private readonly location: Location,
    private readonly salesService: SalesService,
    private readonly itemService: ItemPriceService,
    private readonly time: TimeService,
    private readonly snackBar: MatSnackBar,
    public dialog: MatDialog,
    private readonly route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private brandLimitService: BrandLimitService,
  ) {}

  ngOnInit() {
    this.autoComplete();
    this.route.params.subscribe(() => {
      this.paginator.firstPage();
    });
    this.setDefaultCompany();
    this.dataSource = new CustomerDataSource(this.salesService);
    this.dataSource.loadItems(0, 30, undefined, this.countFilter);
    this.brandLimitForm = this.formBuilder.group({
      brand_name: '',
      limit_amount: '',
    });
    this.loadResize();
  }
  onResize(event) {
    // eslint-disable-next-line
    if (event.target.innerWidth >= 1344) {
      this.firstColumn = 2;
      this.inputColumn = 5;
      this.btnColumn = 3;
      return;
    }
    if (event.target.innerWidth >= 1024 && event.target.innerWidth < 1343) {
      this.firstColumn = 2;
      this.inputColumn = 4;
      this.btnColumn = 4;
      return;
    }
    if (event.target.innerWidth <= 1024 && event.target.innerWidth > 768) {
      this.firstColumn = 7;
      this.inputColumn = 7;
      this.btnColumn = 7;
      return;
    }
    if (event.target.innerWidth < 768) {
      this.firstColumn = 14;
      this.inputColumn = 14;
      this.btnColumn = 14;
      return;
    }
  }
  loadResize() {
    if (window.innerWidth >= 1344) {
      this.firstColumn = 2;
      this.inputColumn = 5;
      this.btnColumn = 3;
      return;
    }
    if (window.innerWidth >= 1024 && window.innerWidth < 1343) {
      this.firstColumn = 2;
      this.inputColumn = 4;
      this.btnColumn = 4;
      return;
    }
    if (window.innerWidth <= 1024 && window.innerWidth > 768) {
      this.firstColumn = 7;
      this.inputColumn = 7;
      this.btnColumn = 7;
      return;
    }
    if (window.innerWidth < 768) {
      this.firstColumn = 14;
      this.inputColumn = 14;
      this.btnColumn = 14;
      return;
    }
  }
  autoComplete() {
    // new
    this.customerProfileForm
      .get('territory')
      .valueChanges.pipe(
        startWith(''),
        switchMap(value => {
          return this.salesService
            .getTerritoryList(value)
            .pipe(map(res => res.docs));
        }),
      )
      .subscribe(res => (this.territories = res));
    // old
    this.customerProfileForm
      .get('customer')
      .valueChanges.pipe(
        startWith(''),
        distinctUntilChanged(),
        debounceTime(500),
        switchMap(value => {
          return this.salesService
            .getCustomerList(value)
            .pipe(map(res => res.docs));
        }),
      )
      .subscribe(res => (this.filteredCustomerList = res));
  }

  clearFilters() {
    this.customerProfileForm.reset();

    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 30;

    this.dataSource.loadItems(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      undefined,
      this.countFilter,
    );
  }

  // getUpdate(event: any) {
  //   this.filters = [];
  //   this.countFilter = {};

  //   if (this.f.customer.value) {
  //     this.filters.push([
  //       'customer_name',
  //       'like',
  //       `%${this.f.customer.value}%`,
  //     ]);
  //     this.countFilter.push([
  //       'Customer',
  //       'customer_name',
  //       'like',
  //       `%${this.f.customer.value}%`,
  //     ]);
  //   }

  //   this.paginator.pageIndex = event?.pageIndex || 0;
  //   this.paginator.pageSize = event?.pageSize || 30;

  //   this.dataSource.loadItems(
  //     this.paginator.pageIndex,
  //     this.paginator.pageSize,
  //     this.filters,
  //     this.countFilter,
  //   );
  // }

  getUpdate(event: any) {
    this.filters = [];
    this.countFilter = [];
    if (this.f.customer.value) {
      this.filters.push([
        'customer_name',
        'like',
        `%${this.f.customer.value}%`,
      ]);
      this.countFilter.push([
        'Customer',
        'customer_name',
        'like',
        `%${this.f.customer.value}%`,
      ]);
    }
    if (this.f.territory.value) {
      this.filters.push(['territory', 'like', `%${this.f.territory.value}%`]);
      this.countFilter.push([
        'Customer',
        'territory',
        'like',
        `%${this.f.territory.value}%`,
      ]);
    }
    this.paginator.pageIndex = event?.pageIndex || 0;
    this.paginator.pageSize = event?.pageSize || 30;
    this.dataSource.loadItems(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.filters,
      this.countFilter,
    );
  }
  // setFilter() {
  //   this.filters = [];
  //   this.countFilter = [];

  //   if (this.f.customer.value) {
  //     this.filters.push([
  //       'customer_name',
  //       'like',
  //       `%${this.f.customer.value}%`,
  //     ]);
  //     this.countFilter.push([
  //       'Customer',
  //       'customer_name',
  //       'like',
  //       `%${this.f.customer.value}%`,
  //     ]);
  //   }

  //   this.paginator.pageIndex = 0;
  //   this.paginator.pageSize = 30;

  //   this.dataSource.loadItems(
  //     this.paginator.pageIndex,
  //     this.paginator.pageSize,
  //     this.filters,
  //     this.countFilter,
  //   );
  // }
  openBrandModal(row: any, i: any) {
    this.customerName = row.customer_name;
    this.creditLimit = row.credit_limit;
    this.isFirstModalOpen = true;
    this.brandLimitService.getBrandLimit(row.name).subscribe((res: any) => {
      const customer = res?.data;

      this.allocatedCreditLimit = 0;

      if (customer?.custom_brand_wise_allocations?.length) {
        customer.custom_brand_wise_allocations.forEach(allocation => {
          this.allocatedCreditLimit += allocation.limit;
        });
      }

      this.brandsCreditLimits = customer.custom_brand_wise_allocations.map(
        x => ({
          brand: x.brand,
          limit: x.limit,
        }),
      );

      this.totalRemainingBalance = customer.excel_remaining_balance;
      this.othersCreditLimit = customer.custom_other_brands_limit;
    });
  }

  closeBrandModal() {
    this.isFirstModalOpen = false;
  }

  createLimit() {
    const data = this.brandLimitForm.value;
    this.brandLimitService.createBrandLimit(data);
    this.brandLimitForm.reset();
  }

  setFilter() {
    this.filters = [];
    this.countFilter = [];

    if (this.f.customer.value) {
      this.filters.push([
        'customer_name',
        'like',
        `%${this.f.customer.value}%`,
      ]);
      this.countFilter.push([
        'Customer',
        'customer_name',
        'like',
        `%${this.f.customer.value}%`,
      ]);
    }

    if (this.f.territory.value) {
      this.filters.push(['territory', 'like', `%${this.f.territory.value}%`]);
      this.countFilter.push([
        'Customer',
        'territory',
        'like',
        `%${this.f.territory.value}%`,
      ]);
    }

    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 30;

    this.dataSource.loadItems(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.filters,
      this.countFilter,
    );
  }

  loadPrice(row: any, index: number) {
    const data = this.dataSource.getData();
    this.dataSource.loadingSubject.next(true);
    if (data && data.length) {
      data[index] = { ...row };
      of({})
        .pipe(
          switchMap(() => {
            return forkJoin({
              time: from(this.time.getDateTime(new Date())),
              token: from(this.salesService.getStore().getItem(ACCESS_TOKEN)),
              debtorAccount: this.salesService
                .getApiInfo()
                .pipe(map(res => res.debtorAccount)),
              customer: this.salesService.relayCustomer(data[index].name),
            });
          }),
          switchMap(({ token, time, debtorAccount, customer }) => {
            if (!debtorAccount) {
              return throwError({
                message: 'Please select Debtor Account in settings',
              });
            }

            if (
              customer &&
              customer.credit_limits &&
              customer.credit_limits.length
            ) {
              customer.credit_limits.forEach(limit => {
                if (limit.company === this.defaultCompany) {
                  data[index].credit_limit = limit.credit_limit;
                }
              });
            } else {
              data[index].credit_limit = '0.00';
            }
            const headers = {
              [AUTHORIZATION]: BEARER_TOKEN_PREFIX + token,
            };
            return this.itemService.getRemainingBalance(
              debtorAccount,
              time,
              'Customer',
              data[index].name,
              this.defaultCompany,
              headers,
            );
          }),
          retry(3),
        )
        .subscribe({
          next: balance => {
            this.dataSource.loadingSubject.next(false);
            data[index].remaining_balance = balance || '0.00';
          },
          error: err => {
            this.dataSource.loadingSubject.next(false);
            this.snackBar.open(
              'Error Occurred in fetching customer balance',
              CLOSE,
              { duration: 3500 },
            );
          },
        });
    } else {
      this.dataSource.loadingSubject.next(false);
    }
    this.dataSource.update(data);
  }

  getRemainingCredit(row) {
    return (row && row.excel_remaining_balance) || 0;
  }

  openVoucher(row: any) {
    this.salesService
      .getStore()
      .getItem(AUTH_SERVER_URL)
      .then(auth_url => {
        window.open(
          `${auth_url}/desk#Form/Excel%20Script%20Runner?customer=${
            row.name
          }&fromDate=${DateTime.local().plus({ months: 1 }).toISODate()}
           &toDate=${DateTime.local().toISODate()}`,
        );
      });
  }

  setDefaultCompany() {
    this.salesService
      .getStore()
      .getItems([DEFAULT_COMPANY])
      .then(items => {
        if (items[DEFAULT_COMPANY]) {
          this.defaultCompany = items[DEFAULT_COMPANY];
        } else {
          this.salesService.getApiInfo().subscribe({
            next: res => {
              this.defaultCompany = res.defaultCompany;
            },
            error: () => {
              this.snackBar.open('Error fetching default company', CLOSE, {
                duration: 3500,
              });
            },
          });
        }
      });
  }

  navigateBack() {
    this.location.back();
  }
  isVissible: boolean = false;
  customarList() {
    const SalseInvoiceSidebarID = document.getElementById('customarList');
    const CustomersFilter = document.getElementById('CustomersFilter');
    const CustomersClose = document.getElementById('CustomersClose');
    const CustomersTable = document.getElementById('CustomersTable');

    if (SalseInvoiceSidebarID.classList.contains('active')) {
      SalseInvoiceSidebarID.classList.remove('active');
      this.isVissible = false;
      CustomersFilter.style.display = 'block';
      CustomersClose.style.display = 'none';
      CustomersTable.style.height = '75.5vh';
    } else {
      SalseInvoiceSidebarID.classList.add('active');
      this.isVissible = true;
      CustomersFilter.style.display = 'none';
      CustomersClose.style.display = 'block';
      CustomersTable.style.height = '67vh';
    }
  }
}
