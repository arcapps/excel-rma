import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrandWiseCreditLimitComponent } from './brand-wise-credit-limit.component';

const routes: Routes = [
  {
    path: '',
    component: BrandWiseCreditLimitComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BrandWiseCreditLimitRoutingModule {}
