import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';

import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  startWith,
  switchMap,
} from 'rxjs';
import { CsvJsonService } from '../../../app/api/csv-json/csv-json.service';
import { ValidateInputSelected } from '../../../app/common/pipes/validators';
import {
  CREDIT_LIMIT_CSV_FILE,
  CREDIT_LIMIT_DOWNLOAD_HEADERS,
} from '../../../app/constants/app-string';
import { SalesService } from '../../../app/sales-ui/services/sales.service';
import { BrandWiseLedgerDataSource } from '../brand-wise-ledger-datasource';
import { CreditLimitLedgerService } from '../services/credit-limit-ledger.service';

export interface Data {
  sr_no: number;
  created_at: string;
  brand_id: string;
  actual_qty: string;
  balance_qty: string;
  doc_type: string;
  doc_id: string;
  modified_by: string;
}

@Component({
  selector: 'app-brand-wise-credit-limit',
  templateUrl: './brand-wise-credit-limit.component.html',
  styleUrls: ['./brand-wise-credit-limit.component.scss'],
})
export class BrandWiseCreditLimitComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  dataSource: BrandWiseLedgerDataSource;
  displayedColumns = [
    'sr_no',
    'created_at',
    'brand_id',
    'actual_qty',
    'balance_qty',
    'doc_type',
    'doc_id',
  ];
  brandWiseCreditLimitForm: UntypedFormGroup = new UntypedFormGroup({
    customer: new UntypedFormControl(''),
    brand: new UntypedFormControl(''),
    doc_id: new UntypedFormControl(''),
    start_date: new UntypedFormControl(),
    end_date: new UntypedFormControl(),
  });
  filters: any[] = [];
  filteredCustomerList: any[];
  filteredBrandList: any[];
  firstColumn: number;
  inputColumn: number;
  btnColumn: number;
  validateInput: any = ValidateInputSelected;
  get f() {
    return this.brandWiseCreditLimitForm.controls;
  }

  constructor(
    private readonly csvService: CsvJsonService,
    private readonly ledgerSvc: CreditLimitLedgerService,
    private readonly location: Location,
    private readonly route: ActivatedRoute,
    private readonly salesService: SalesService,
  ) {}

  ngOnInit() {
    this.autoComplete();
    this.dataSource = new BrandWiseLedgerDataSource(this.ledgerSvc);

    this.route.params.subscribe(() => {
      this.paginator.firstPage();
      this.clearFilters();
    });
    this.loadResize();
  }

  onResize(event) {
    // eslint-disable-next-line
    if (event.target.innerWidth >= 1543) {
      this.firstColumn = 2;
      this.inputColumn = 4;
      this.btnColumn = 7;
      return;
    }
    if (event.target.innerWidth >= 1024 && event.target.innerWidth < 1543) {
      this.firstColumn = 2;
      this.inputColumn = 3;
      this.btnColumn = 10;
      return;
    }
    if (event.target.innerWidth <= 1024 && event.target.innerWidth > 768) {
      this.firstColumn = 5;
      this.inputColumn = 10;
      this.btnColumn = 25;
      return;
    }
    if (event.target.innerWidth < 768) {
      this.firstColumn = 25;
      this.inputColumn = 25;
      this.btnColumn = 25;
      return;
    }
  }
  loadResize() {
    if (window.innerWidth >= 1543) {
      this.firstColumn = 2;
      this.inputColumn = 4;
      this.btnColumn = 7;
      return;
    }
    if (window.innerWidth >= 1024 && window.innerWidth < 1543) {
      this.firstColumn = 2;
      this.inputColumn = 3;
      this.btnColumn = 10;
      return;
    }
    if (window.innerWidth <= 1024 && window.innerWidth > 768) {
      this.firstColumn = 5;
      this.inputColumn = 10;
      this.btnColumn = 25;
      return;
    }
    if (window.innerWidth < 768) {
      this.firstColumn = 25;
      this.inputColumn = 25;
      this.btnColumn = 25;
      return;
    }
  }

  autoComplete() {
    this.brandWiseCreditLimitForm
      .get('customer')
      .valueChanges.pipe(
        startWith(''),
        distinctUntilChanged(),
        debounceTime(500),
        switchMap(value => {
          return this.salesService
            .getCustomerList(value || '')
            .pipe(map(res => res.docs));
        }),
      )
      .subscribe(res => {
        this.filteredCustomerList = res;
      });

    this.brandWiseCreditLimitForm
      .get('brand')
      .valueChanges.pipe(
        startWith(''),
        distinctUntilChanged(),
        debounceTime(500),
        switchMap(value => {
          return this.salesService
            .getItemBrandList(value || '')
            .pipe(map(res => res));
        }),
      )
      .subscribe(res => {
        this.filteredBrandList = res;
      });
  }

  clearFilters() {
    this.brandWiseCreditLimitForm.reset();

    this.filters = [];

    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 30;

    this.dataSource.loadItems(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.filters,
    );
  }

  downloadCreditLimits() {
    this.csvService.downloadAsCSV(
      this.dataSource.data.map(x => ({
        brand: x.brand_id,
        transaction: x.actual_qty,
        remaining_limit: x.balance_qty,
        doc_type: x.doc_type,
        doc_id: x.doc_id,
        created_at: x.created_at,
      })),
      CREDIT_LIMIT_DOWNLOAD_HEADERS,
      `${CREDIT_LIMIT_CSV_FILE}`,
    );
  }

  setFilter() {
    this.updateFilters();

    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 30;

    this.dataSource.loadItems(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.filters,
    );
  }

  getUpdate(event: any) {
    this.updateFilters();

    this.paginator.pageIndex = event?.pageIndex || 0;
    this.paginator.pageSize = event?.pageSize || 30;
    this.dataSource.loadItems(
      this.paginator.pageIndex,
      this.paginator.pageSize,
      this.filters,
    );
  }

  navigateBack() {
    this.location.back();
  }

  private updateFilters() {
    this.filters = [];

    if (this.f.customer.value) {
      const selectedCustomer = this.filteredCustomerList.find(
        customer => customer.customer_name === this.f.customer.value,
      );

      this.filters.push({ customer_id: selectedCustomer.name });
    }

    if (this.f.brand.value) {
      this.filters.push({ brand_id: this.f.brand.value });
    }

    if (this.f.doc_id.value) {
      this.filters.push({ doc_id: this.f.doc_id.value });
    }

    if (this.f.start_date.value && this.f.end_date.value) {
      this.filters.push({
        start: new Date(this.f.start_date.value).setHours(0, 0, 0, 0),
      });

      this.filters.push({
        end: new Date(this.f.end_date.value).setHours(23, 59, 59, 59),
      });
    }
  }
}
