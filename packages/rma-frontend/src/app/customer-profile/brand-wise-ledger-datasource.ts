import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, finalize, map } from 'rxjs/operators';
import { CreditLimitLedgerService } from './services/credit-limit-ledger.service';

export interface ListingData {
  uuid: string;
  customer_id: string;
  brand_id: string;
  actual_qty: number;
  balance_qty: number;
  doc_type: string;
  doc_id: string;
  created_at: string;
}

export interface ListResponse {
  docs: ListingData[];
  length: number;
  offset: number;
}

export class BrandWiseLedgerDataSource extends DataSource<ListingData> {
  data: ListingData[];
  length: number;
  offset: number;

  itemSubject = new BehaviorSubject<ListingData[]>([]);
  loadingSubject = new BehaviorSubject<boolean>(false);

  loading$ = this.loadingSubject.asObservable();

  constructor(private readonly ledgerSvc: CreditLimitLedgerService) {
    super();
  }

  connect(collectionViewer: CollectionViewer): Observable<ListingData[]> {
    return this.itemSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.itemSubject.complete();
    this.loadingSubject.complete();
  }

  loadItems(pageIndex = 0, pageSize = 30, filters: any[]) {
    this.loadingSubject.next(true);
    this.ledgerSvc
      .getBrandWiseCreditLimitLedger(
        JSON.stringify(filters) || '[]',
        'ASC',
        pageIndex,
        pageSize,
      )
      .pipe(
        map((res: ListResponse) => {
          this.data = res.docs;
          this.offset = res.offset;
          this.length = res.length;
          return res.docs;
        }),
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false)),
      )
      .subscribe(items => this.itemSubject.next(items));
  }

  getData() {
    return this.itemSubject.value;
  }

  update(data) {
    this.itemSubject.next(data);
  }
}
