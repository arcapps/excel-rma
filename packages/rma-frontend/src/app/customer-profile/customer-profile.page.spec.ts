import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { TimeService } from '../api/time/time.service';
import { MaterialModule } from '../material/material.module';
import { ItemPriceService } from '../sales-ui/services/item-price.service';
import { SalesService } from '../sales-ui/services/sales.service';
import { CustomerProfilePage } from './customer-profile.page';

@Pipe({ name: 'curFormat' })
class MockPipe implements PipeTransform {
  transform(value: string) {
    return value;
  }
}

describe('CustomerProfilePage', () => {
  let component: CustomerProfilePage;
  let fixture: ComponentFixture<CustomerProfilePage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [CustomerProfilePage, MockPipe],
        imports: [
          IonicModule.forRoot(),
          MaterialModule,
          FormsModule,
          ReactiveFormsModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          RouterTestingModule,
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        providers: [
          {
            provide: SalesService,
            useValue: {
              relayCustomerList: (...args) => of([{}]),
              customerList: (...args) => of([{}]),
              getCustomerList: (...args) => of([{}]),
              getTerritoryList: (...args) => of([{}]),
              getDoctypeCount: (...args) => of(0),
              getStore: () => ({
                getItem: (...args) => Promise.resolve('Item'),
                getItems: (...args) => Promise.resolve({}),
              }),
              getApiInfo: (...args) => of(),
            },
          },
          {
            provide: ItemPriceService,
            useValue: {},
          },
          {
            provide: TimeService,
            useValue: {},
          },
          {
            provide: MatSnackBar,
            useValue: {},
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(CustomerProfilePage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
